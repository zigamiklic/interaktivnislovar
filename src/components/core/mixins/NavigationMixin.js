import NavigationComponent from '@/components/shared/NavigationComponent.vue'

export default {
  components: {
    'is-navigation': NavigationComponent
  },

  created () {
    this.$store.dispatch('setNavBackText', '')
    this.$store.dispatch('setNavForwardText', '')
    this.$store.dispatch('setShowNavBack', false)
    this.$store.dispatch('setShowNavForward', false)
    this.$store.dispatch('setPulseNavBack', false)
    this.$store.dispatch('setPulseNavForward', false)
  }
}
