import { Observable } from 'rxjs'
import { Howl } from 'howler'

export default {
  data () {
    return {
      sound: null
    }
  },

  methods: {
    onSoundEnd (observer) {
      setTimeout(() => {
        observer.next()
        observer.complete()
      }, 1000)
    },

    playSound$ (asset = null, delay = 0) {
      if (asset === null) {
        return Observable.create(this.onSoundEnd)
      }

      return Observable.create(o => {
        this.sound = new Howl({
          src: [asset],
          onend: () => {
            this.onSoundEnd(o)
          }
        })

        setTimeout(() => {
          this.sound.play()
        }, delay)
      })
    },

    stopSound () {
      if (this.sound) {
        this.sound.unload()
      }
    }
  },

  destroyed () {
    this.stopSound()
  }
}
