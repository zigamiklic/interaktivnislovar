export default {
  components: {},

  data () {
    return {
      originalX: 0,
      originalY: 0,
      currentX: 0,
      currentY: 0
    }
  },

  props: {
    posX: Number,
    posY: Number,
    posDynamic: Boolean
  },

  computed: {
    transformStyle () {
      return {transform: `translate(${this.currentX}px, ${this.currentY}px)`}
    }
  },

  methods: {
    setPosition (x, y) {
      this.originalX = this.currentX = x
      this.originalY = this.currentY = y
    }
  },

  updated () {
    if (this.posDynamic) {
      if (this.originalX !== this.posX && this.posX !== undefined) {
        this.originalX = this.currentX = this.posX
      }

      if (this.originalY !== this.posY && this.posY !== undefined) {
        this.originalY = this.currentY = this.posY
      }
    }
  },

  mounted () {
    this.originalX = this.currentX = this.posX
    this.originalY = this.currentY = this.posY
  }
}
