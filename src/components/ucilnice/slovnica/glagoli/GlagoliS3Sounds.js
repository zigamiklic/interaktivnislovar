import weWrite from '@/assets/sounds/5.2/s3/weWrite.mp3'
import you from '@/assets/sounds/5.2/s3/you.mp3'
import they from '@/assets/sounds/5.2/s3/they.mp3'

export default {
  weWrite,
  you,
  they
}
