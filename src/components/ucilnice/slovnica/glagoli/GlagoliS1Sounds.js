import meWrite from '@/assets/sounds/5.2/s1/meWrite.mp3'
import you from '@/assets/sounds/5.2/s1/you.mp3'
import he from '@/assets/sounds/5.2/s1/he.mp3'
import she from '@/assets/sounds/5.2/s1/she.mp3'

export default {
  meWrite,
  you,
  he,
  she
}
