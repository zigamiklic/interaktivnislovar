export let linesData = new Map([
  ['Glagoli-1', {
    write: {
      meWrite: 'Jaz piše<span style="color: red;">m</span>.',
      you: 'Ti piše<span style="color: red;">š</span>.',
      he: 'On piše.',
      she: 'Ona piše.'
    }
  }],
  ['Glagoli-2', {
    write: {
      weWrite: 'Midva piše<span style="color: red;">va</span>.',
      you: 'Vidva piše<span style="color: red;">ta</span>.',
      they: 'Onadva piše<span style="color: red;">ta</span>.'
    }
  }],
  ['Glagoli-3', {
    write: {
      weWrite: 'Mi piše<span style="color: red;">mo</span>.',
      you: 'Vi piše<span style="color: red;">te</span>.',
      they: 'Oni piše<span style="color: red;">jo</span>.'
    }
  }]
])
