export let labelsData = new Map([
  ['TablePovezovanje', {
    racunam: {matchId: 'jaz', text: 'računam'},
    racunamo: {matchId: 'mi', text: 'računamo'},
    racunata1: {matchId: 'vidva onadva', text: 'računata'},
    racuna1: {matchId: 'on ona', text: 'računa'},
    racunata2: {matchId: 'vidva onadva', text: 'računata'},
    racunas: {matchId: 'ti', text: 'računaš'},
    racuna2: {matchId: 'on ona', text: 'računa'},
    racunajo: {matchId: 'oni', text: 'računajo'},
    racunate: {matchId: 'vi', text: 'računate'},
    racunava: {matchId: 'midva', text: 'računava'}
  }],
  ['TablePovezovanje-2', {
    pisem: {matchId: 'jaz', text: 'pišem'},
    beremo: {matchId: 'mi', text: 'beremo'},
    govori: {matchId: 'on', text: 'govori'},
    poslusata: {matchId: 'vidva', text: 'poslušata'},
    racunas: {matchId: 'ti', text: 'računaš'},
    brisetaTablo: {matchId: 'onadva', text: 'brišeta tablo'},
    seUcijo: {matchId: 'oni', text: 'se učijo'},

    seIgra: {matchId: 'ona', text: 'se igra'},
    pojeva: {matchId: 'midva', text: 'pojeva'},
    sedite: {matchId: 'vi', text: 'sedite'},
    risem: {matchId: 'jaz2', text: 'rišem'},
    slika: {matchId: 'on2', text: 'slika'},
    siUmivasRoke: {matchId: 'ti2', text: 'si umivaš roke'},
    klepetamo: {matchId: 'mi2', text: 'klepetamo'}
  }]
])

let m1 = 16 // margin between items
let h1 = 48 // height of item
let cl1 = 240 // left column x
let cr1 = 540 // right column x
let ct1 = 160 // columns top/y

let m2 = 16
let h2 = 48
let cl2 = 254
let cr2 = 540
let ct2 = 105
export let dropsData = new Map([
  ['TablePovezovanje', {
    jaz: {matchId: 'jaz', text: 'jaz', pos: {x: cl1, y: ct1}},
    mi: {matchId: 'mi', text: 'mi', pos: {x: cl1, y: ct1 + (h1 + m1)}},
    on: {matchId: 'on', text: 'on', pos: {x: cl1, y: ct1 + (h1 + m1) * 2}},
    vidva: {matchId: 'vidva', text: 'vidva', pos: {x: cl1, y: ct1 + (h1 + m1) * 3}},
    ti: {matchId: 'ti', text: 'ti', pos: {x: cl1, y: ct1 + (h1 + m1) * 4}},

    onadva: {matchId: 'onadva', text: 'onadva', pos: {x: cr1, y: ct1}},
    oni: {matchId: 'oni', text: 'oni', pos: {x: cr1, y: ct1 + (h1 + m1)}},
    ona: {matchId: 'ona', text: 'ona', pos: {x: cr1, y: ct1 + (h1 + m1) * 2}},
    vi: {matchId: 'vi', text: 'vi', pos: {x: cr1, y: ct1 + (h1 + m1) * 3}},
    midva: {matchId: 'midva', text: 'midva', pos: {x: cr1, y: ct1 + (h1 + m1) * 4}}
  }],
  ['TablePovezovanje-2', {
    jaz: {glyph: 'pisem', matchId: 'jaz', text: 'jaz', pos: {x: cl2, y: ct2}},
    mi: {glyph: 'beremo', matchId: 'mi', text: 'mi', pos: {x: cl2, y: ct2 + (h2 + m2)}},
    on: {glyph: 'govori', matchId: 'on', text: 'on', pos: {x: cl2, y: ct2 + (h2 + m2) * 2}},
    vidva: {glyph: 'poslusata', matchId: 'vidva', text: 'vidva', pos: {x: cl2, y: ct2 + (h2 + m2) * 3}},
    ti: {glyph: 'racunas', matchId: 'ti', text: 'ti', pos: {x: cl2, y: ct2 + (h2 + m2) * 4}},
    onadva: {glyph: 'brisetaTablo', matchId: 'onadva', text: 'onadva', pos: {x: cl2, y: ct2 + (h2 + m2) * 5}},
    oni: {glyph: 'seUcijo', matchId: 'oni', text: 'oni', pos: {x: cl2, y: ct2 + (h2 + m2) * 6}},

    ona: {glyph: 'seIgra', matchId: 'ona', text: 'ona', pos: {x: cr2, y: ct2}},
    midva: {glyph: 'pojeva', matchId: 'midva', text: 'midva', pos: {x: cr2, y: ct2 + (h2 + m2)}},
    vi: {glyph: 'sedite', matchId: 'vi', text: 'vi', pos: {x: cr2, y: ct2 + (h2 + m2) * 2}},
    jaz2: {glyph: 'risem', matchId: 'jaz2', text: 'jaz', pos: {x: cr2, y: ct2 + (h2 + m2) * 3}},
    on2: {glyph: 'slika', matchId: 'on2', text: 'on', pos: {x: cr2, y: ct2 + (h2 + m2) * 4}},
    ti2: {glyph: 'siUmivasRoke', matchId: 'ti2', text: 'ti', pos: {x: cr2, y: ct2 + (h2 + m2) * 5}},
    mi2: {glyph: 'klepetamo', matchId: 'mi2', text: 'mi', pos: {x: cr2, y: ct2 + (h2 + m2) * 6}}
  }]
])

export let lines = new Map([
  ['TablePovezovanje', {
    poskusi: 'Zdaj pa poskusi še ti.'
  }],
  ['TablePovezovanje-2', {
    poskusi: 'Zdaj pa poskusi še ti.'
  }]
])
