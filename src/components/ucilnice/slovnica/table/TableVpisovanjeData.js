let m1 = 16
let h1 = 48
let cl1 = 340
let cr1 = 770
let ct1 = 120
export let inputsData = new Map([
  ['TableVpisovanje', {
    jaz: {glyph: 'pisem', solution: 'pišem', text: 'jaz', pos: {x: cl1, y: ct1}},
    mi: {glyph: 'beremo', solution: 'beremo', text: 'mi', pos: {x: cl1, y: ct1 + (h1 + m1)}},
    on: {glyph: 'govori', solution: 'govori', text: 'on', pos: {x: cl1, y: ct1 + (h1 + m1) * 2}},
    vidva: {glyph: 'poslusata', solution: 'poslušata', text: 'vidva', pos: {x: cl1, y: ct1 + (h1 + m1) * 3}},
    ti: {glyph: 'racunas', solution: 'računaš', text: 'ti', pos: {x: cl1, y: ct1 + (h1 + m1) * 4}},
    onadva: {glyph: 'brisetaTablo', solution: 'brišeta tablo', text: 'onadva', pos: {x: cl1, y: ct1 + (h1 + m1) * 5}},
    oni: {glyph: 'seUcijo', solution: 'se učijo', text: 'oni', pos: {x: cl1, y: ct1 + (h1 + m1) * 6}},

    ona: {glyph: 'seIgra', solution: 'se igra', text: 'ona', pos: {x: cr1, y: ct1}},
    midva: {glyph: 'pojeva', solution: 'pojeva', text: 'midva', pos: {x: cr1, y: ct1 + (h1 + m1)}},
    vi: {glyph: 'sedite', solution: 'sedite', text: 'vi', pos: {x: cr1, y: ct1 + (h1 + m1) * 2}},
    jaz2: {glyph: 'risem', solution: 'rišem', text: 'jaz', pos: {x: cr1, y: ct1 + (h1 + m1) * 3}},
    on2: {glyph: 'slika', solution: 'slika', text: 'on', pos: {x: cr1, y: ct1 + (h1 + m1) * 4}},
    ti2: {glyph: 'siUmivasRoke', solution: 'si umivaš roke', text: 'ti', pos: {x: cr1, y: ct1 + (h1 + m1) * 5}},
    mi2: {glyph: 'klepetamo', solution: 'klepetamo', text: 'mi', pos: {x: cr1, y: ct1 + (h1 + m1) * 6}}
  }]
])

export let lines = new Map([
  ['TableVpisovanje', {
    poskusi: 'Zdaj pa poskusi še ti.'
  }]
])
