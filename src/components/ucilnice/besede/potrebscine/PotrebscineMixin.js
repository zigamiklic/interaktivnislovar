import { TweenLite } from 'gsap'
import { Observable, Subject } from 'rxjs'

import GameBasicMixin from '@/components/shared/mixins/GameBasicMixin'
import GameUtilsMixin from '@/components/shared/mixins/GameUtilsMixin'
import StoreLabelsMixin from '@/components/shared/mixins/StoreLabelsMixin'
import StoreTargetsMixin from '@/components/shared/mixins/StoreTargetsMixin'
import AnimationsLabelsMixin from '@/components/shared/mixins/AnimationsLabelsMixin'

import { targetsData, labelsData } from './PotrebscineData'

export default {
  mixins: [
    GameBasicMixin,
    StoreLabelsMixin,
    StoreTargetsMixin,
    GameUtilsMixin,
    AnimationsLabelsMixin
  ],
  data () {
    return {
      targetsData: {},
      labelsData: {},
    }
  },

  methods: {
    onDragStopped (dragEl, dragElPos) {
      let target = this.getTargetIfMatch(this.targets, dragEl.matchId, dragElPos)

      if (target) {
        target.setSolved()
        dragEl.setSolved()
        if (this.isGameSolved()) {
          this.startEnd()
        }
      } else {
        dragEl.moveToStartPosition()
      }
    },

    isGameSolved () {
      let solved = this.labels.filter((label) => label.isSolved)
      return solved.length === this.labels.length
    },

    resetScene () {
      this.$refs.endModal.hide()
      this.targets.forEach((target) => target.reset())
      this.labels.forEach((label) => label.reset())
      this.nowYouTry.reset()
    },

    startIntroAnimation () {
      this.resetScene()
      this.setStateToIntro()

      this.introAnim$ = this.animateLabelsToTargetsWithSound$(this.labels, this.$refs, this.sounds)
        .subscribe(null, null, this.startYouTryAnimation)
    },

    stopIntroAnimation () {
      if (this.introAnim$) {
        this.introAnim$.unsubscribe()
      }
    },

    startYouTryAnimation () {
      if (!this.nowYouTry.isAnimating) {
        this.nowYouTry.animate()
          .subscribe(null, null, this.startInteraction)
      }
    },

    startInteraction () {
      this.resetScene()
      this.setStateToInteractive()
    },

    startEnd () {
      this.setStateToEnd()

      const dialog = this.endDialogs.get(this.$route.name)
      this.$refs.endModal.show(dialog)
    },

    getLabelDestination (label) {
      let destination = this.$refs[label.animateTo][0].center()
      let labelBounds = label.bounds()

      return {
        currentX: destination.x - labelBounds.width / 2,
        currentY: destination.y - labelBounds.height / 2
      }
    },
  },

  created () {
    this.targetsData = targetsData.get(this.$route.name)
    this.labelsData = labelsData.get(this.$route.name)

    this.$store.dispatch('setShowNavBack', true)
    this.$store.dispatch('setShowNavForward', true)
  },

  mounted () {
    this.labels.forEach((label) => {
      let b = label.$el.getBoundingClientRect()
      label.setPosition(
        (b.left - this.screenPosition.x) / this.screenScale,
        (b.top - this.screenPosition.y) / this.screenScale
      )
    })

    this.$refs.positioning.classList.remove('positioning')

    this.nowYouTry = this.$refs.nowYouTry

    this.startIntroAnimation()
  },

  destroyed () {
    this.stopIntroAnimation()

    if (this.introTimeline) {
      this.introTimeline.kill()
    }

    if (this.intro) {
      this.intro.kill()
    }
  }
}
