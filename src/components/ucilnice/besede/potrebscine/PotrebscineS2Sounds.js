import berilo from '@/assets/sounds/4.3/berilo.mp3'
import delovniZvezek from '@/assets/sounds/4.3/delovniZvezek.mp3'
import knjiga from '@/assets/sounds/4.3/knjiga.mp3'
import list from '@/assets/sounds/4.3/list.mp3'
import mapa from '@/assets/sounds/4.3/mapa.mp3'
import torba from '@/assets/sounds/4.3/torba.mp3'
import ucbenik from '@/assets/sounds/4.3/ucbenik.mp3'
import ura from '@/assets/sounds/4.3/ura.mp3'
import urnik from '@/assets/sounds/4.3/urnik.mp3'
import vodenke from '@/assets/sounds/4.3/vodenke.mp3'
import zvezek from '@/assets/sounds/4.3/zvezek.mp3'
import copic from '@/assets/sounds/4.3/copic.mp3'

export default { berilo, delovniZvezek, knjiga, list, mapa, torba, ucbenik, ura, urnik, vodenke, zvezek, copic }
