import berilo from '@/assets/ucilnice/besede/potrebscine/screen-2/berilo.svg'
import delovniZvezek from '@/assets/ucilnice/besede/potrebscine/screen-2/delovni_zvezek.svg'
import knjiga from '@/assets/ucilnice/besede/potrebscine/screen-2/knjiga.svg'
import list from '@/assets/ucilnice/besede/potrebscine/screen-2/list.svg'
import mapa from '@/assets/ucilnice/besede/potrebscine/screen-2/mapa.svg'
import torba from '@/assets/ucilnice/besede/potrebscine/screen-2/torba.svg'
import ucbenik from '@/assets/ucilnice/besede/potrebscine/screen-2/ucbenik.svg'
import ura from '@/assets/ucilnice/besede/potrebscine/screen-2/ura.svg'
import urnik from '@/assets/ucilnice/besede/potrebscine/screen-2/urnik.svg'
import vodenke from '@/assets/ucilnice/besede/potrebscine/screen-2/vodenke.svg'
import zvezek from '@/assets/ucilnice/besede/potrebscine/screen-2/zvezek.svg'
import copic from '@/assets/ucilnice/besede/potrebscine/screen-2/copic.svg'

export default { berilo, delovniZvezek, knjiga, list, mapa, torba, ucbenik, ura, urnik, vodenke, zvezek, copic }
