import milo from '@/assets/sounds/4.2/milo.mp3'
import papirnataBrisaca from '@/assets/sounds/4.2/papirnataBrisaca.mp3'
import kosi from '@/assets/sounds/4.2/kosi.mp3'
import klop from '@/assets/sounds/4.2/klop.mp3'
import umivalnik from '@/assets/sounds/4.2/umivalnik.mp3'
import plakat from '@/assets/sounds/4.2/plakat.mp3'
import loncnica from '@/assets/sounds/4.2/loncnica.mp3'
import miza from '@/assets/sounds/4.2/miza.mp3'
import racunalnik from '@/assets/sounds/4.2/racunalnik.mp3'
import stol from '@/assets/sounds/4.2/stol.mp3'
import tabla from '@/assets/sounds/4.2/tabla.mp3'
import vrata from '@/assets/sounds/4.2/vrata.mp3'
import okno from '@/assets/sounds/4.2/okno.mp3'
import omara from '@/assets/sounds/4.2/omara.mp3'
import projektor from '@/assets/sounds/4.2/projektor.mp3'
import ura from '@/assets/sounds/4.2/ura.mp3'
import uvod from '@/assets/sounds/4.2/uvod.mp3'
import zakjucek from '@/assets/sounds/poizkusi-se-ti.mp3'

export default {
  milo,
  papirnataBrisaca,
  kosi,
  umivalnik,
  plakat,
  loncnica,
  tabla,
  vrata,
  okno,
  omara,
  projektor,
  miza,
  racunalnik,
  stol,
  klop,
  ura,
  uvod,
  zakjucek
}
