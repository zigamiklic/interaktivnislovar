export let targetsData = {
  milo: {matchId: 'milo', pos: {x: 543, y: 241}},
  papirnataBrisaca: {matchId: 'papirnataBrisaca', pos: {x: 485, y: 222}},
  kosi: {matchId: 'kosi', pos: {x: 438, y: 303}},
  umivalnik: {matchId: 'umivalnik', pos: {x: 550, y: 246}},
  plakat: {matchId: 'plakat', pos: {x: 851, y: 130}},
  loncnica: {matchId: 'loncnica', pos: {x: 1131, y: 240}},
  miza: {matchId: 'miza', pos: {x: 933, y: 576}},
  racunalnik: {matchId: 'racunalnik', pos: {x: 1078, y: 561}},
  stol: {matchId: 'stol', pos: {x: 984, y: 588}},
  ura: {matchId: 'ura', pos: {x: 437, y: 88}},
  tabla: {matchId: 'tabla', pos: {x: 26, y: 372}},
  vrata: {matchId: 'vrata', pos: {x: 131, y: 183}},
  okno: {matchId: 'okno', pos: {x: 1219, y: 159}},
  omara: {matchId: 'omara', pos: {x: 990, y: 143}},
  projektor: {matchId: 'projektor', pos: {x: 570, y: 14}},
  klop4: {matchId: 'klop', pos: {x: 277, y: 325}},
  klop5: {matchId: 'klop', pos: {x: 578, y: 324}},
  klop6: {matchId: 'klop', pos: {x: 862, y: 325}},
  klop3: {matchId: 'klop', pos: {x: 893, y: 424}},
  klop2: {matchId: 'klop', pos: {x: 567, y: 423}},
  klop1: {matchId: 'klop', pos: {x: 230, y: 424}}
}

export let labelsData = new Map([
  ['KajJeTo-1', {
    omara: {animateTo: 'omara', matchId: 'omara', text: 'To je omara.', pos: {x: 82, y: 547}},
    miza: {animateTo: 'miza', matchId: 'miza', text: 'To je miza.', pos: {x: 350, y: 547}},
    umivalnik: {animateTo: 'umivalnik', matchId: 'umivalnik', text: 'To je umivalnik.', pos: {x: 380, y: 616}},
    projektor: {animateTo: 'projektor', matchId: 'projektor', text: 'To je projektor.', pos: {x: 38, y: 616}},
    tabla: {animateTo: 'tabla', matchId: 'tabla', text: 'To je tabla.', pos: {x: 577, y: 547}},
    plakat: {animateTo: 'plakat', matchId: 'plakat', text: 'To je plakat.', pos: {x: 700, y: 616}},
    papirnataBrisaca: {animateTo: 'papirnataBrisaca', matchId: 'papirnataBrisaca', text: 'To je papirnata brisačka.', pos: {x: 38, y: 682}},
    okno: {animateTo: 'okno', matchId: 'okno', text: 'To je okno.', pos: {x: 516, y: 682}}
  }],
  ['KajJeTo-2', {
    milo: {animateTo: 'milo', matchId: 'milo', text: 'To je milo.', pos: {x: 82, y: 547}},
    ura: {animateTo: 'ura', matchId: 'ura', text: 'To je ura.', pos: {x: 310, y: 547}},
    klop: {animateTo: 'klop2', matchId: 'klop', text: 'To je klop.', pos: {x: 530, y: 547}},
    vrata: {animateTo: 'vrata', matchId: 'vrata', text: 'To so vrata.', pos: {x: 38, y: 616}},
    loncnica: {animateTo: 'loncnica', matchId: 'loncnica', text: 'To je lončnica.', pos: {x: 296, y: 616}},
    kosi: {animateTo: 'kosi', matchId: 'kosi', text: 'To je koš.', pos: {x: 612, y: 616}},
    stol: {animateTo: 'stol', matchId: 'stol', text: 'To je stol.', pos: {x: 38, y: 682}},
    racunalnik: {animateTo: 'racunalnik', matchId: 'racunalnik', text: 'To je računalnik.', pos: {x: 267, y: 682}}
  }]
])

export let inputsData = new Map([
  ['KajJeToVpisovanje', {
    milo: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'milo', pos: {x: 522, y: 170}},
    papirnataBrisaca: {openDirection: 'left', arrowDirection: 'right', preText: 'To je', solution: 'papirnata brisačka', pos: {x: 412, y: 210}},
    kosi: {openDirection: 'right', arrowDirection: 'top', preText: 'To je', solution: 'koš', pos: {x: 480, y: 360}},
    umivalnik: {openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'umivalnik', pos: {x: 634, y: 262}},
    ura: {openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'ura', pos: {x: 522, y: 96}},
    plakat: {openDirection: 'left', arrowDirection: 'bottom', preText: 'To je', solution: 'plakat', pos: {x: 874, y: 70}},
    omara: {openDirection: 'left', arrowDirection: 'bottom', preText: 'To je', solution: 'omara', pos: {x: 1020, y: 80}},
    loncnica: {openDirection: 'left', arrowDirection: 'bottom', preText: 'To je', solution: 'lončnica', pos: {x: 1120, y: 170}},
    miza: {openDirection: 'left', arrowDirection: 'right', preText: 'To je', solution: 'miza', pos: {x: 870, y: 576}},
    racunalnik: {openDirection: 'left', arrowDirection: 'bottom', preText: 'To je', solution: 'računalnik', pos: {x: 1080, y: 506}},
    stol: {openDirection: 'left', arrowDirection: 'right', preText: 'To je', solution: 'stol', pos: {x: 918, y: 648}},
    tabla: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'tabla', pos: {x: 30, y: 362}},
    vrata: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To so', solution: 'vrata', pos: {x: 140, y: 132}},
    okno: {openDirection: 'left', arrowDirection: 'bottom', preText: 'To je', solution: 'okno', pos: {x: 1230, y: 160}},
    projektor: {openDirection: 'left', arrowDirection: 'right', preText: 'To je', solution: 'projektor', pos: {x: 550, y: 14}},
    klop: {openDirection: 'right', arrowDirection: 'bottom', preText: 'To je', solution: 'klop', pos: {x: 640, y: 414}}
  }]])

export let linesData = new Map([
  ['KajJeTo-1', {
    kaj: 'Kaj je to?',
    poskusi: 'Zdaj pa poskusi še ti!'
  }],
  ['KajJeTo-2', {
    kaj: 'Kaj je to?',
    poskusi: 'Zdaj pa poskusi še ti!'
  }],
  ['KajJeToVpisovanje', {
    kaj: 'Kaj je to?',
    poskusi: 'Zdaj pa poskusi še ti!'
  }]
])
