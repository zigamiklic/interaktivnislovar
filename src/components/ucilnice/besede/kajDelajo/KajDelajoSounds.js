import alesRise from '@/assets/sounds/4.4/alesRise.mp3'
import anjaSedi from '@/assets/sounds/4.4/anjaSedi.mp3'
import anzePoslusa from '@/assets/sounds/4.4/anzePoslusa.mp3'
import jasnaBere from '@/assets/sounds/4.4/jasnaBere.mp3'
import jureBriseTablo from '@/assets/sounds/4.4/jureBriseTablo.mp3'
import katarinaRacuna from '@/assets/sounds/4.4/katarinaRacuna.mp3'
import maticPoje from '@/assets/sounds/4.4/maticPoje.mp3'
import mojcaSlika from '@/assets/sounds/4.4/mojcaSlika.mp3'
import nezaSiUmivaRoke from '@/assets/sounds/4.4/nezaSiUmivaRoke.mp3'
import peterKlepeta from '@/assets/sounds/4.4/peterKlepeta.mp3'
import tinaSeIgra from '@/assets/sounds/4.4/tinaSeIgra.mp3'
import uciteljicaGovori from '@/assets/sounds/4.4/uciteljicaGovori.mp3'
import uciteljPise from '@/assets/sounds/4.4/uciteljPise.mp3'
import vidSeUci from '@/assets/sounds/4.4/vidSeUci.mp3'
import poskusi from '@/assets/sounds/poizkusi-se-ti.mp3'
import kajDelajo from '@/assets/sounds/4.4/kajDelajo.mp3'

export default {
  alesRise,
  anjaSedi,
  anzePoslusa,
  jasnaBere,
  jureBriseTablo,
  katarinaRacuna,
  maticPoje,
  mojcaSlika,
  nezaSiUmivaRoke,
  peterKlepeta,
  tinaSeIgra,
  uciteljicaGovori,
  uciteljPise,
  vidSeUci,
  poskusi,
  kajDelajo
}
