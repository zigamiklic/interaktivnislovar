export let lines = {
  jakob1: 'To je moj razred.',
  jakob2: 'To je moja učiteljica.',
  jakob3: 'To je moj učitelj.',
  uciteljica1: 'Jakob je moj učenec.',
  uciteljica2: 'Ana je moja učenka.',
  ana1: 'To je moja sošolka Maja.',
  ana2: 'To je moj sošolec Alex.',
  jakob4: 'Alex ne govori Slovensko.',
  ana3: 'Igrajmo se igro &#xbb;Kaj je to?&#xab;!'
}
