export let targetsData = new Map([
  ['KdoJeTo', {
    ivanaKobilca: {matchId: 'ivanaKobilca', pos: {x: 125, y: 60}},
    primozTrubar: {matchId: 'primozTrubar', pos: {x: 310, y: 60}},
    francePreseren: {matchId: 'francePreseren', pos: {x: 495, y: 60}},
    jozePlecnik: {matchId: 'jozePlecnik', pos: {x: 690, y: 60}},
    jurijVega: {matchId: 'jurijVega', pos: {x: 865, y: 60}},
    ivanCankar: {matchId: 'ivanCankar', pos: {x: 210, y: 290}},
    jakobPetelin: {matchId: 'jakobPetelin', pos: {x: 395, y: 290}},
    janezValvazor: {matchId: 'janezValvazor', pos: {x: 580, y: 290}},
    rihardJakopic: {matchId: 'rihardJakopic', pos: {x: 765, y: 290}}
  }]
])

export let labelsData = new Map([
  ['KdoJeTo', {
    ivanaKobilca: { animateTo: 'ivanaKobilca', matchId: 'ivanaKobilca', text: 'To je Ivana Kobilca, ki je slikarka.', char: 'vid' },
    primozTrubar: { animateTo: 'primozTrubar', matchId: 'primozTrubar', text: 'To je Primož Trubar, ki je pisatelj.', char: 'vid' },
    francePreseren: { animateTo: 'francePreseren', matchId: 'francePreseren', text: 'To je France Prešeren, ki je pesnik.', char: 'vid' },
    jozePlecnik: { animateTo: 'jozePlecnik', matchId: 'jozePlecnik', text: 'To je Jože Plečnik, ki je arhitekt.', char: 'vid' },
    jurijVega: { animateTo: 'jurijVega', matchId: 'jurijVega', text: 'To je Jurij Vega, ki je matematik.', char: 'vid' },
    ivanCankar: { animateTo: 'ivanCankar', matchId: 'ivanCankar', text: 'To je Ivan Cankar, ki je pisatelj.', char: 'katarina' },
    jakobPetelin: { animateTo: 'jakobPetelin', matchId: 'jakobPetelin', text: 'To je Jakob Petelin Gallus, ki je skladatelj.', char: 'katarina' },
    janezValvazor: { animateTo: 'janezValvazor', matchId: 'janezValvazor', text: 'To je Janez Vajkard Valvazor, ki je zgodovinar.', char: 'katarina' },
    rihardJakopic: { animateTo: 'rihardJakopic', matchId: 'rihardJakopic', text: 'To je Rihard Jakopič, ki je slikar.', char: 'katarina' }
  }]
])

export let linesData = new Map([
  ['KdoJeTo', {
    poskusi: 'Zdaj pa poskusi še ti!'
  }]
])
