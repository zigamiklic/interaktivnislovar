export let itemsData = new Map([
  ['KajDelajoVpisovanje', {
    ivanaKobilca: {pos: {x: 125, y: 60}},
    primozTrubar: {pos: {x: 310, y: 60}},
    francePreseren: {pos: {x: 495, y: 60}},
    jozePlecnik: {pos: {x: 690, y: 60}},
    jurijVega: {pos: {x: 865, y: 60}},
    ivanCankar: {pos: {x: 210, y: 290}},
    jakobPetelin: {pos: {x: 395, y: 290}},
    janezValvazor: {pos: {x: 580, y: 290}},
    rihardJakopic: {pos: {x: 765, y: 290}}
  }]
])

export let inputsData = new Map([
  ['KajDelajoVpisovanje', {
    rihardJakopic: { arrowDirection: 'top', preText: 'Rihard Jakopič', solution: 'slika', pos: { x: 800, y: 460 }, char: 'katarina' },
    janezValvazor: { arrowDirection: 'top', preText: 'Janez Vajkard Valvazor', solution: 'piše', pos: { x: 615, y: 460 }, char: 'katarina' },
    jakobPetelin: { arrowDirection: 'top', preText: 'Jakob Petelin Gallus', solution: 'sklada', pos: { x: 430, y: 460 }, char: 'katarina' },
    ivanCankar: { arrowDirection: 'top', preText: 'Ivan Cankar', solution: 'piše', pos: { x: 245, y: 460 }, char: 'katarina' },
    jurijVega: { arrowDirection: 'top', preText: 'Jurij Vega', solution: 'računa', pos: { x: 900, y: 230 }, char: 'vid' },
    jozePlecnik: { arrowDirection: 'top', preText: 'Jože Plečnik', solution: 'riše', pos: { x: 715, y: 230 }, char: 'vid' },
    francePreseren: { arrowDirection: 'top', preText: 'France Prešeren', solution: 'piše', pos: { x: 530, y: 230 }, char: 'vid' },
    primozTrubar: { arrowDirection: 'top', preText: 'Primož Trubar', solution: 'piše', pos: { x: 345, y: 230 }, char: 'vid' },
    ivanaKobilca: { arrowDirection: 'top', preText: 'Ivana Kobilca', solution: 'slika', pos: { x: 160, y: 230 }, char: 'vid' }
  }]
])

export let linesData = new Map([
  ['KajDelajoVpisovanje', {
    kaj: 'Kaj dela?',
    poskusi: 'Zdaj pa poskusi še ti!'
  }]
])
