import francePreseren from '@/assets/sounds/6/kdo-je-to/francePreseren.mp3'
import ivanaKobilca from '@/assets/sounds/6/kdo-je-to/ivanaKobilca.mp3'
import ivanCankar from '@/assets/sounds/6/kdo-je-to/ivanCankar.mp3'
import jakobPetelin from '@/assets/sounds/6/kdo-je-to/jakobPetelin.mp3'
import janezValvazor from '@/assets/sounds/6/kdo-je-to/janezValvazor.mp3'
import jozePlecnik from '@/assets/sounds/6/kdo-je-to/jozePlecnik.mp3'
import jurijVega from '@/assets/sounds/6/kdo-je-to/jurijVega.mp3'
import primozTrubar from '@/assets/sounds/6/kdo-je-to/primozTrubar.mp3'
import rihardJakopic from '@/assets/sounds/6/kdo-je-to/rihardJakopic.mp3'
import kdoJeTo from '@/assets/sounds/6/kdo-je-to.mp3'
import poskusi from '@/assets/sounds/6/poskusi.mp3'

export default {
  francePreseren,
  ivanaKobilca,
  ivanCankar,
  jakobPetelin,
  janezValvazor,
  jozePlecnik,
  jurijVega,
  primozTrubar,
  rihardJakopic,
  kdoJeTo,
  poskusi
}
