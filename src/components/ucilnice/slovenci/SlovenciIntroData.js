export let itemsData = new Map([
  ['SlovenciUvod', {
    ivanaKobilca: { text: 'To je Ivana Kobilca, ki je slikarka.', pos: { x: 125, y: 60 }, char: 'vid' },
    primozTrubar: { text: 'To je Primož Trubar, ki je pisatelj.', pos: { x: 310, y: 60 }, char: 'vid' },
    francePreseren: { text: 'To je France Prešeren, ki je pesnik.', pos: { x: 495, y: 60 }, char: 'vid' },
    jozePlecnik: { text: 'To je Jože Plečnik, ki je arhitekt.', pos: { x: 690, y: 60 }, char: 'vid' },
    jurijVega: { text: 'To je Jurij Vega, ki je matematik.', pos: { x: 865, y: 60 }, char: 'katarina' },
    ivanCankar: { text: 'To je Ivan Cankar, ki je pisatelj.', pos: { x: 210, y: 290 }, char: 'katarina' },
    jakobPetelin: { text: 'To je Jakob Petelin Gallus, ki je skladatelj.', pos: { x: 395, y: 290 }, char: 'katarina' },
    janezValvazor: { text: 'To je Janez Vajkard Valvazor, ki je zgodovinar.', pos: { x: 580, y: 290 }, char: 'katarina' },
    rihardJakopic: { text: 'To je Rihard Jakopič, ki je slikar.', pos: { x: 765, y: 290 }, char: 'katarina' }
  }]
])

export let linesData = new Map([
  ['SlovenciUvod', {
    kdo: 'Kdo je to?'
  }]
])
