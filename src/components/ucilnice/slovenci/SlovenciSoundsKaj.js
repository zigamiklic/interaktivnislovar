import francePreseren from '@/assets/sounds/6/kaj-dela/francePreseren.mp3'
import ivanaKobilca from '@/assets/sounds/6/kaj-dela/ivanaKobilca.mp3'
import ivanCankar from '@/assets/sounds/6/kaj-dela/ivanCankar.mp3'
import jakobPetelin from '@/assets/sounds/6/kaj-dela/jakobPetelin.mp3'
import janezValvazor from '@/assets/sounds/6/kaj-dela/janezValvazor.mp3'
import jozePlecnik from '@/assets/sounds/6/kaj-dela/jozePlecnik.mp3'
import jurijVega from '@/assets/sounds/6/kaj-dela/jurijVega.mp3'
import primozTrubar from '@/assets/sounds/6/kaj-dela/primozTrubar.mp3'
import rihardJakopic from '@/assets/sounds/6/kaj-dela/rihardJakopic.mp3'
import kajDela from '@/assets/sounds/6/kaj-dela.mp3'
import poskusi from '@/assets/sounds/6/poskusi.mp3'

export default {
  francePreseren,
  ivanaKobilca,
  ivanCankar,
  jakobPetelin,
  janezValvazor,
  jozePlecnik,
  jurijVega,
  primozTrubar,
  rihardJakopic,
  kajDela,
  poskusi
}
