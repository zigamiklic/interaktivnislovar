import { Observable } from 'rxjs'
import { TimelineLite } from 'gsap'
import GameSoundMixin from '@/components/core/mixins/GameSoundMixin'

export default {
  mixins: [GameSoundMixin],
  data () {
    return {
      animData: {}
    }
  },
  methods: {
    animateLabelsToTargets$ (labels, refs) {
      return Observable.create((observer) => {
        let introTimeline = new TimelineLite({
          onComplete: () => {
            observer.next()
            observer.complete()
          },
          delay: 0.6,
          paused: true})

        labels.forEach((label) => {
          let target = refs[label.animateTo]
          if (target[0] !== undefined) {
            target = target[0]
          }
          let destination = target.center()
          let labelBounds = label.$el.getBoundingClientRect()

          introTimeline
            .set(label.$el, {'z-index': 100})
            .to(label, 2, {
              currentX: destination.x - (labelBounds.width / this.screenScale) / 2,
              currentY: destination.y - (labelBounds.height / this.screenScale) / 2
            })
            .set(label, {isSolved: true})
        })

        introTimeline.play()

        return () => {
          introTimeline.kill()
        }
      })
    },

    animateLabelsToTargetsWithSound$ (labels, refs, sounds) {
      return Observable.create((observer) => {
        this.introTimeline = new TimelineLite({
          delay: 0.6,
          paused: true,
          onComplete: () => {
            this.isAnimDone = true
            this.nextStep()
          }
        })

        this.animData = {
          labels: [...labels],
          refs,
          sounds,
          observer,
          isSoundDone: false,
          isAnimDone: false
        }

        this.selectNext()

        return () => {
          this.stopAnimateLabelsToTargets()
        }
      })
    },

    nextStep () {
      if (this.isAnimDone && this.isSoundDone) {
        this.isAnimDone = false
        this.isSoundDone = false

        this.introTimeline.set(this.label, { isSolved: true })

        if (this.animData.labels.length) {
          this.selectNext()
        } else {
          this.animData.observer.next()
          this.animData.observer.complete()
        }
      }
    },

    selectNext () {
      this.label = this.animData.labels.splice(0, 1)[0]

      let target = this.animData.refs[this.label.animateTo]
      if (target[0] !== undefined) {
        target = target[0]
      }
      let destination = target.center()
      let labelBounds = this.label.$el.getBoundingClientRect()

      this.introTimeline
        .set(this.label.$el, { 'z-index': 100 })
        .to(this.label, 2, {
          currentX: destination.x - (labelBounds.width / this.screenScale) / 2,
          currentY: destination.y - (labelBounds.height / this.screenScale) / 2
        })

      this.playSound$(this.animData.sounds[this.label.$attrs.id.replace('$l-', '')]).subscribe(() => {
        this.isSoundDone = true
        this.nextStep()
      })

      this.introTimeline.play()
    },

    stopAnimateLabelsToTargets () {
      if (this.introTimeline) {
        this.introTimeline.kill()
      }

      if (typeof this.stopSound === 'function') {
        this.stopSound()
      }
    }
  },

  destroyed () {
    this.stopAnimateLabelsToTargets()
  }
}
