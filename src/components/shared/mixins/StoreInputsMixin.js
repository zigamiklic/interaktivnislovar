export default {
  data () {
    return {
      inputs: []
    }
  },
  mounted () {
    this.inputs = Object.keys(this.$refs)
      .filter(val => val.includes('$i'))
      .map(key => this.$refs[key][0] || this.$refs[key])
  }
}
