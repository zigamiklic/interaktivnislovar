import GameBasicMixin from '@/components/shared/mixins/GameBasicMixin'
import GameSoundMixin from '@/components/core/mixins/GameSoundMixin'
import { Observable } from 'rxjs'

export default {
  mixins: [GameBasicMixin, GameSoundMixin],

  data () {
    return {
      animationsCurrent: 0,
      characters: {},
      charactersCurrent: 0,
      isEnd: false,
      frames: [
        { from: 0, to: 101 }, // me
        { from: 101, to: 165 }, // you
        { from: 165, to: 223 }, // he / they
        { from: 223, to: 250 } // she
      ]
    }
  },

  computed: {
    currentAnimationName () {
      return this.animationsOrder[this.animationsCurrent]
    },
    currentAnimation () {
      return this.animations[this.currentAnimationName]
    },
    currentCharacter () {
      return this.characterOrder[this.charactersCurrent]
    },
    characterLines () {
      return this.linesData.get(this.$route.name)[this.currentAnimationName]
    }
  },

  methods: {
    navigate (routes) {
      const route = routes.get(this.$route.name)
      this.$router.push({ name: route })
    },

    navigateBack () {
      this.navigate(this.$store.state.sceneRoutes.backwardRoutes)
    },

    navigateForward () {
      this.navigate(this.$store.state.sceneRoutes.forwardRoutes)
    },

    showBubble () {
      return this.$refs.bubble.show$(this.characterLines[this.currentCharacter])
    },

    animateMainCharacter (from = this.frames[this.charactersCurrent].from, to = this.frames[this.charactersCurrent].to) {
      if (from !== null && to !== null && this.characters.me !== undefined) {
        return this.characters.me.playSegment$(from, to, true)
      }

      return Observable.of({})
    },

    animateCharacter () {
      if (this.currentCharacter !== 'me') {
        return this.characters[this.currentCharacter].play$(false, true)
      }

      return Observable.of({})
    },

    playSound () {
      let sound

      if (this.animationsOrder.length > 1) {
        sound = this.sounds[this.currentAnimationName][this.currentCharacter]
      } else {
        sound = this.sounds[this.currentCharacter]
      }

      return this.playSound$(sound, this.frames[this.charactersCurrent].soundDelay)
    },

    nextAnimation () {
      this.setCharacters()

      this.animations$ = Observable.zip(
        this.showBubble(),
        this.animateMainCharacter(),
        this.animateCharacter(),
        this.playSound()
      ).subscribe(() => {
        if (this.charactersCurrent === this.characterOrder.length - 1) {
          // Reset character, next animation
          this.charactersCurrent = 0

          if (this.animationsCurrent < this.animationsOrder.length - 1) {
            this.animationsCurrent += 1
          } else {
            return this.endAnimations()
          }
        } else {
          // Next character
          this.charactersCurrent += 1
        }

        return this.$nextTick(this.nextAnimation)
      })
    },

    endAnimations () {
      this.$refs.bubble.hide()
      const to = this.frames[this.frames.length - 1].to
      this.animateMainCharacter(to, 999).subscribe()
      this.isEnd = true
    },

    setCharacters () {
      this.characterOrder.forEach(character => {
        this.characters[character] = this.$refs[character]
      })

      if (this.characters.me === undefined) {
        this.characters.me = this.$refs['me']
      }
    }
  },

  mounted () {
    setTimeout(this.nextAnimation, 1)
  },

  created () {
    this.$store.dispatch('setShowNavBack', true)
    this.$store.dispatch('setShowNavForward', true)
    this.$store.dispatch('setNavForwardText', this.forwardText)
  },

  destroyed () {
    if (this.animations$) {
      this.animations$.unsubscribe()
    }
  }
}
