export const shelfs = [
  {
    top: 59,
    paddingSize: 'lg',
    hangers: [
      { name: 'one', value: 1, label: 'ena' },
      { name: 'two', value: 2, label: 'dve' },
      { name: 'three', value: 3, label: 'tri' },
      { name: 'four', value: 4, label: 'štiri' },
      { name: 'five', value: 5, label: 'pet' },
      { name: 'six', value: 6, label: 'šest' }
    ]
  },
  {
    top: 215,
    hangers: [
      { name: 'seven', value: 7, label: 'sedem' },
      { name: 'eight', value: 8, label: 'osem' },
      { name: 'nine', value: 9, label: 'devet' },
      { name: 'ten', value: 10, label: 'deset' },
      { name: 'eleven', value: 11, label: 'enajst' },
      { name: 'twelve', value: 12, label: 'dvanajst' }
    ]
  },
  {
    top: 374,
    hangers: [
      { name: 'thirteen', value: 13, label: 'trinajst' },
      { name: 'fourteen', value: 14, label: 'štirinajst' },
      { name: 'fifteen', value: 15, label: 'petnajst' },
      { name: 'sixteen', value: 16, label: 'šestnajst' },
      { name: 'seventeen', value: 17, label: 'sedemnajst' },
      { name: 'eighteen', value: 18, label: 'osemnajst' },
      { name: 'nineteen', value: 19, label: 'devetnajst' }
    ]
  },
  {
    top: 534,
    hangers: [
      { name: 'twenty', value: 20, label: 'dvajset' },
      { name: 'twentyone', value: 21, label: 'enaindvajset' },
      { name: 'twentytwo', value: 22, label: 'dvaindvajset' },
      { name: 'twentythree', value: 23, label: 'triindvajset' },
      { name: 'twentyfour', value: 24, label: 'štiriindvajset' },
      { name: 'twentyfive', value: 25, label: 'petindvajset' }
    ]
  }
]

export const inputsData = {
  one: {
    solution: 'ena',
    openDirection: 'right',
    pos: {
      x: 139,
      y: 161
    }
  },
  two: {
    solution: 'dve',
    openDirection: 'right',
    pos: {
      x: 338,
      y: 161
    }
  },
  three: {
    solution: 'tri',
    openDirection: 'right',
    pos: {
      x: 537,
      y: 161
    }
  },
  four: {
    solution: 'štiri',
    openDirection: 'right',
    pos: {
      x: 736,
      y: 161
    }
  },
  five: {
    solution: 'pet',
    openDirection: 'right',
    pos: {
      x: 935,
      y: 161
    }
  },
  six: {
    solution: 'šest',
    openDirection: 'left',
    pos: {
      x: 1134,
      y: 161
    }
  },

  seven: {
    solution: 'sedem',
    openDirection: 'right',
    pos: {
      x: 89,
      y: 317
    }
  },
  eight: {
    solution: 'osem',
    openDirection: 'right',
    pos: {
      x: 308,
      y: 317
    }
  },
  nine: {
    solution: 'devet',
    openDirection: 'right',
    pos: {
      x: 527,
      y: 317
    }
  },
  ten: {
    solution: 'deset',
    openDirection: 'right',
    pos: {
      x: 746,
      y: 317
    }
  },
  eleven: {
    solution: 'enajst',
    openDirection: 'right',
    pos: {
      x: 965,
      y: 317
    }
  },
  twelve: {
    solution: 'dvanajst',
    openDirection: 'left',
    pos: {
      x: 1184,
      y: 317
    }
  },

  thirteen: {
    solution: 'trinajst',
    openDirection: 'right',
    pos: {
      x: 90,
      y: 476
    }
  },
  fourteen: {
    solution: 'štirinajst',
    openDirection: 'right',
    pos: {
      x: 271,
      y: 476
    }
  },
  fifteen: {
    solution: 'petnajst',
    openDirection: 'right',
    pos: {
      x: 452,
      y: 476
    }
  },
  sixteen: {
    solution: 'šestnajst',
    openDirection: 'right',
    pos: {
      x: 635,
      y: 476
    }
  },
  seventeen: {
    solution: 'sedemnajst',
    openDirection: 'right',
    pos: {
      x: 819,
      y: 476
    }
  },
  eighteen: {
    solution: 'osemnajst',
    openDirection: 'left',
    pos: {
      x: 1000,
      y: 476
    }
  },
  nineteen: {
    solution: 'devetnajst',
    openDirection: 'left',
    pos: {
      x: 1184,
      y: 476
    }
  },

  twenty: {
    solution: 'dvajset',
    openDirection: 'right',
    pos: {
      x: 90,
      y: 636
    }
  },
  twentyone: {
    solution: 'enaindvajset',
    openDirection: 'right',
    pos: {
      x: 309,
      y: 636
    }
  },
  twentytwo: {
    solution: 'dvaindvajset',
    openDirection: 'right',
    pos: {
      x: 527,
      y: 636
    }
  },
  twentythree: {
    solution: 'triindvajset',
    openDirection: 'right',
    pos: {
      x: 747,
      y: 636
    }
  },
  twentyfour: {
    solution: 'štiriindvajset',
    openDirection: 'right',
    pos: {
      x: 965,
      y: 636
    }
  },
  twentyfive: {
    solution: 'petindvajset',
    openDirection: 'left',
    pos: {
      x: 1185,
      y: 636
    }
  }
}
