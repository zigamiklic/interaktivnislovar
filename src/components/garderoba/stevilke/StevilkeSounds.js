import one from '@/assets/sounds/garderoba/1.1/1.mp3'
import two from '@/assets/sounds/garderoba/1.1/2.mp3'
import three from '@/assets/sounds/garderoba/1.1/3.mp3'
import four from '@/assets/sounds/garderoba/1.1/4.mp3'
import five from '@/assets/sounds/garderoba/1.1/5.mp3'
import six from '@/assets/sounds/garderoba/1.1/6.mp3'
import seven from '@/assets/sounds/garderoba/1.1/7.mp3'
import eight from '@/assets/sounds/garderoba/1.1/8.mp3'
import nine from '@/assets/sounds/garderoba/1.1/9.mp3'
import ten from '@/assets/sounds/garderoba/1.1/10.mp3'
import eleven from '@/assets/sounds/garderoba/1.1/11.mp3'
import twelve from '@/assets/sounds/garderoba/1.1/12.mp3'
import thirteen from '@/assets/sounds/garderoba/1.1/13.mp3'
import fourteen from '@/assets/sounds/garderoba/1.1/14.mp3'
import fifteen from '@/assets/sounds/garderoba/1.1/15.mp3'
import sixteen from '@/assets/sounds/garderoba/1.1/16.mp3'
import seventeen from '@/assets/sounds/garderoba/1.1/17.mp3'
import eighteen from '@/assets/sounds/garderoba/1.1/18.mp3'
import nineteen from '@/assets/sounds/garderoba/1.1/19.mp3'
import twenty from '@/assets/sounds/garderoba/1.1/20.mp3'
import twentyone from '@/assets/sounds/garderoba/1.1/21.mp3'
import twentytwo from '@/assets/sounds/garderoba/1.1/22.mp3'
import twentythree from '@/assets/sounds/garderoba/1.1/23.mp3'
import twentyfour from '@/assets/sounds/garderoba/1.1/24.mp3'
import twentyfive from '@/assets/sounds/garderoba/1.1/25.mp3'

export default {
  one,
  two,
  three,
  four,
  five,
  six,
  seven,
  eight,
  nine,
  ten,
  eleven,
  twelve,
  thirteen,
  fourteen,
  fifteen,
  sixteen,
  seventeen,
  eighteen,
  nineteen,
  twenty,
  twentyone,
  twentytwo,
  twentythree,
  twentyfour,
  twentyfive
}
