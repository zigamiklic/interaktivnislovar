export default [
  { name: 'srajcica', label: 'To je srajčica.' },
  { name: 'ovcka', label: 'To je ovčka.' },
  { name: 'pajek', label: 'To je pajek.' },
  { name: 'rak', label: 'To je rak.' },
  { name: 'ptica', label: 'To je ptica.' },
  { name: 'volna', label: 'To je volna.' },
  { name: 'zalosten', label: 'Videk je žalosten.' },
  { name: 'vesel', label: 'Videk je srečen.' }
]
