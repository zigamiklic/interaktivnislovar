import srajcica from '@/assets/garderoba/table-slike/srajcica.svg'
import ovcka from '@/assets/garderoba/table-slike/ovcka.svg'
import pajek from '@/assets/garderoba/table-slike/pajek.svg'
import rak from '@/assets/garderoba/table-slike/rak.svg'
import ptica from '@/assets/garderoba/table-slike/ptica.svg'
import volna from '@/assets/garderoba/table-slike/volna.svg'
import zalosten from '@/assets/garderoba/table-slike/zalosten.svg'
import vesel from '@/assets/garderoba/table-slike/vesel.svg'

export default {
  srajcica,
  ovcka,
  pajek,
  rak,
  ptica,
  volna,
  zalosten,
  vesel
}
