import videkZalosten from '@/assets/garderoba/pesmica/videk-zalosten.svg'
import videkVesel from '@/assets/garderoba/pesmica/videk-vesel.svg'
import ptica from '@/assets/garderoba/pesmica/ptica.svg'
import rak from '@/assets/garderoba/pesmica/rak.svg'
import pajek from '@/assets/garderoba/pesmica/pajek.svg'
import ovca from '@/assets/garderoba/pesmica/ovca.svg'

export default {
  videkZalosten,
  videkVesel,
  ptica,
  rak,
  pajek,
  ovca
}
