export default {
  staro: { text: 'staro' },
  zalosten: { text: 'žalosten' },
  volno: { text: 'volno' },
  pajek: { text: 'pajek' },
  rak: { text: 'rak' },
  ptica: { text: 'ptica' },
  novo: { text: 'novo' },
  vesel: { text: 'srečen' }
}
