import alex from '@/assets/garderoba/kako-so-obleceni/alex.svg'
import alexCopati from '@/assets/garderoba/kako-so-obleceni/alex-copati.svg'
import alexMajica from '@/assets/garderoba/kako-so-obleceni/alex-majica.svg'
import alexTrenirka from '@/assets/garderoba/kako-so-obleceni/alex-trenirka.svg'

import ana from '@/assets/garderoba/kako-so-obleceni/ana.svg'
import anaCevlji from '@/assets/garderoba/kako-so-obleceni/ana-cevlji.svg'
import anaHlace from '@/assets/garderoba/kako-so-obleceni/ana-hlace.svg'
import anaJopica from '@/assets/garderoba/kako-so-obleceni/ana-jopica.svg'
import anaKapa from '@/assets/garderoba/kako-so-obleceni/ana-kapa.svg'
import anaSal from '@/assets/garderoba/kako-so-obleceni/ana-sal.svg'
import anaRokavice from '@/assets/garderoba/kako-so-obleceni/ana-rokavice.svg'

import jakob from '@/assets/garderoba/kako-so-obleceni/jakob.svg'
import jakobSportniCopati from '@/assets/garderoba/kako-so-obleceni/jakob-sportni-copati.svg'
import jakobKavbojke from '@/assets/garderoba/kako-so-obleceni/jakob-kavbojke.svg'
import jakobSrajca from '@/assets/garderoba/kako-so-obleceni/jakob-srajca.svg'
import jakobJakna from '@/assets/garderoba/kako-so-obleceni/jakob-jakna.svg'

import maja from '@/assets/garderoba/kako-so-obleceni/maja.svg'
import majaKrilo from '@/assets/garderoba/kako-so-obleceni/maja-krilo.svg'
import majaPulover from '@/assets/garderoba/kako-so-obleceni/maja-pulover.svg'
import majaCevlji from '@/assets/garderoba/kako-so-obleceni/maja-cevlji.svg'

export default {
  alex,
  alexCopati,
  alexMajica,
  alexTrenirka,

  ana,
  anaCevlji,
  anaHlace,
  anaJopica,
  anaKapa,
  anaRokavice,
  anaSal,

  jakob,
  jakobJakna,
  jakobKavbojke,
  jakobSportniCopati,
  jakobSrajca,

  maja,
  majaKrilo,
  majaPulover,
  majaCevlji
}
