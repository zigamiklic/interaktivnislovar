import anaCevlji from '@/assets/sounds/garderoba/1.5.1/cevlji.mp3'
import majaCevlji from '@/assets/sounds/garderoba/1.5.1/cevlji.mp3'
import alexCopati from '@/assets/sounds/garderoba/1.5.1/copati.mp3'
import anaHlace from '@/assets/sounds/garderoba/1.5.1/hlace.mp3'
import jakobJakna from '@/assets/sounds/garderoba/1.5.1/jakna.mp3'
import anaJopica from '@/assets/sounds/garderoba/1.5.1/jopica.mp3'
import anaKapa from '@/assets/sounds/garderoba/1.5.1/kapa.mp3'
import jakobKavbojke from '@/assets/sounds/garderoba/1.5.1/kavbojke.mp3'
import majaKrilo from '@/assets/sounds/garderoba/1.5.1/krilo.mp3'
import alexMajica from '@/assets/sounds/garderoba/1.5.1/majica.mp3'
import majaPulover from '@/assets/sounds/garderoba/1.5.1/pulover.mp3'
import anaRokavice from '@/assets/sounds/garderoba/1.5.1/rokavice.mp3'
import anaSal from '@/assets/sounds/garderoba/1.5.1/sal.mp3'
import jakobSportniCopati from '@/assets/sounds/garderoba/1.5.1/sportni-copati.mp3'
import jakobSrajca from '@/assets/sounds/garderoba/1.5.1/srajca.mp3'
import alexTrenirka from '@/assets/sounds/garderoba/1.5.1/trenirka.mp3'

export default {
  anaCevlji,
  majaCevlji,
  alexCopati,
  anaHlace,
  jakobJakna,
  anaJopica,
  anaKapa,
  jakobKavbojke,
  majaKrilo,
  alexMajica,
  majaPulover,
  anaRokavice,
  anaSal,
  jakobSportniCopati,
  jakobSrajca,
  alexTrenirka
}
