export const targetsData = {
  alexCopati: { glyph: 'alexCopati', matchId: 'alexCopati', pos: { x: 975, y: 533 } },
  alexMajica: { glyph: 'alexMajica', matchId: 'alexMajica', pos: { x: 984, y: 210 } },
  alexTrenirka: { glyph: 'alexTrenirka', matchId: 'alexTrenirka', pos: { x: 1009, y: 342 } },

  anaCevlji: { glyph: 'anaCevlji', matchId: 'anaCevlji', pos: { x: 717, y: 529 } },
  anaHlace: { glyph: 'anaHlace', matchId: 'anaHlace', pos: { x: 723, y: 327 } },
  anaKapa: { glyph: 'anaKapa', matchId: 'anaKapa', pos: { x: 695, y: 7 } },
  anaJopica: { glyph: 'anaJopica', matchId: 'anaJopica', pos: { x: 698, y: 212 } },
  anaSal: { glyph: 'anaSal', matchId: 'anaSal', pos: { x: 740, y: 204 } },
  anaRokavice: { glyph: 'anaRokavice', matchId: 'anaRokavice', pos: { x: 679, y: 336 } },

  jakobKavbojke: { glyph: 'jakobKavbojke', matchId: 'jakobKavbojke', pos: { x: 477, y: 346 } },
  jakobSrajca: { glyph: 'jakobSrajca', matchId: 'jakobSrajca', pos: { x: 477, y: 205 } },
  jakobJakna: { glyph: 'jakobJakna', matchId: 'jakobJakna', pos: { x: 428, y: 209 } },
  jakobSportniCopati: { glyph: 'jakobSportniCopati', matchId: 'jakobSportniCopati', pos: { x: 476, y: 532 } },

  majaKrilo: { glyph: 'majaKrilo', matchId: 'majaKrilo', pos: { x: 178, y: 307 } },
  majaPulover: { glyph: 'majaPulover', matchId: 'majaPulover', pos: { x: 177, y: 204 } },
  majaCevlji: { glyph: 'majaCevlji', matchId: 'majaCevlji', pos: { x: 220, y: 534 } }
}

export const labelsData = {
  majaKrilo: { animateTo: 'majaKrilo', matchId: 'majaKrilo', text: 'To je krilo.' },
  anaCevlji: { animateTo: 'anaCevlji', matchId: ['anaCevlji', 'majaCevlji'], text: 'To so čevlji.' },
  anaKapa: { animateTo: 'anaKapa', matchId: 'anaKapa', text: 'To je kapa.' },
  alexTrenirka: { animateTo: 'alexTrenirka', matchId: 'alexTrenirka', text: 'To je trenirka.' },
  anaHlace: { animateTo: 'anaHlace', matchId: 'anaHlace', text: 'To so hlače.' },
  alexMajica: { animateTo: 'alexMajica', matchId: 'alexMajica', text: 'To je majica.' },
  anaJopica: { animateTo: 'anaJopica', matchId: 'anaJopica', text: 'To je jopica.' },
  jakobJakna: { animateTo: 'jakobJakna', matchId: 'jakobJakna', text: 'To je jakna.' },
  jakobKavbojke: { animateTo: 'jakobKavbojke', matchId: 'jakobKavbojke', text: 'To so kavbojke.' },
  jakobSportniCopati: { animateTo: 'jakobSportniCopati', matchId: 'jakobSportniCopati', text: 'To so športni copati.' },
  anaSal: { animateTo: 'anaSal', matchId: 'anaSal', text: 'To je šal.' },
  jakobSrajca: { animateTo: 'jakobSrajca', matchId: 'jakobSrajca', text: 'To je srajca.' },
  anaRokavice: { animateTo: 'anaRokavice', matchId: 'anaRokavice', text: 'To so rokavice.' },
  majaPulover: { animateTo: 'majaPulover', matchId: 'majaPulover', text: 'To je pulover.' },
  alexCopati: { animateTo: 'alexCopati', matchId: 'alexCopati', text: 'To so copati.' },
  majaCevlji: { animateTo: 'majaCevlji', matchId: ['majaCevlji', 'anaCevlji'], text: 'To so čevlji.' }
}
