const types = {
  is: {
    label: 'to je',
    question: 'Kaj je to?'
  },
  are: {
    label: 'to so',
    question: 'Kaj so to?'
  },
  wear: {
    label: 'nosi',
    question: 'Kaj nosi?'
  }
}

const words = [
  {
    name: 'pulover',
    solution: 'pulover',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'brezrokavnik',
    solution: 'brezrokavnik',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'sal',
    solution: 'šal',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'jopica',
    text: 'jopica',
    solution: 'jopico',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'jakna',
    text: 'jakna',
    solution: 'jakno',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'majica',
    text: 'majica',
    solution: 'majico',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'srajca',
    text: 'srajca',
    solution: 'srajco',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'trenirka',
    text: 'trenirka',
    solution: 'trenirko',
    type: 'wear',
    quantity: 'singular'
  },
  {
    name: 'kapa',
    text: 'kapa',
    solution: 'kapo',
    type: 'wear',
    quantity: 'singular'
  },

  {
    name: 'copati',
    text: 'copati',
    solution: 'copate',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'cevlji',
    text: 'čevlji',
    solution: 'čevlje',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'sportniCopati',
    text: 'športni copati',
    solution: 'športne copate',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'hlace',
    solution: 'hlače',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'kavbojke',
    solution: 'kavbojke',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'rokavice',
    solution: 'rokavice',
    type: 'wear',
    quantity: 'plural'
  },
  {
    name: 'nogavice',
    solution: 'nogavice',
    type: 'wear',
    quantity: 'plural'
  }
]

export default {
  types,
  words
}
