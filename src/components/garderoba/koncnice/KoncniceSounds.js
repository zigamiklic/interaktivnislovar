import brezrokavnik from '@/assets/sounds/garderoba/2.3.1/brezrokavnik.mp3'
import bunda from '@/assets/sounds/garderoba/2.3.1/bunda.mp3'
import cevlji from '@/assets/sounds/garderoba/2.3.1/cevlji.mp3'
import copati from '@/assets/sounds/garderoba/2.3.1/copati.mp3'
import hlace from '@/assets/sounds/garderoba/2.3.1/hlace.mp3'
import jakna from '@/assets/sounds/garderoba/2.3.1/jakna.mp3'
import jopica from '@/assets/sounds/garderoba/2.3.1/jopica.mp3'
import kapa from '@/assets/sounds/garderoba/2.3.1/kapa.mp3'
import kavbojke from '@/assets/sounds/garderoba/2.3.1/kavbojke.mp3'
import majica from '@/assets/sounds/garderoba/2.3.1/majica.mp3'
import nogavice from '@/assets/sounds/garderoba/2.3.1/nogavice.mp3'
import pulover from '@/assets/sounds/garderoba/2.3.1/pulover.mp3'
import rokavice from '@/assets/sounds/garderoba/2.3.1/rokavice.mp3'
import sal from '@/assets/sounds/garderoba/2.3.1/sal.mp3'
import sportniCopati from '@/assets/sounds/garderoba/2.3.1/sportni-copati.mp3'
import srajca from '@/assets/sounds/garderoba/2.3.1/srajca.mp3'
import trenirka from '@/assets/sounds/garderoba/2.3.1/trenirka.mp3'

export default {
  brezrokavnik,
  bunda,
  cevlji,
  copati,
  hlace,
  jakna,
  jopica,
  kapa,
  kavbojke,
  majica,
  nogavice,
  pulover,
  rokavice,
  sal,
  sportniCopati,
  srajca,
  trenirka
}
