import pulover from '@/assets/garderoba/shared/pulover.svg'
import puloverWear from '@/assets/garderoba/koncnice/pulover.svg'

import brezrokavnik from '@/assets/garderoba/shared/brezrokavnik.svg'
import brezrokavnikWear from '@/assets/garderoba/koncnice/brezrokavnik.svg'

import sal from '@/assets/garderoba/shared/sal.svg'
import salWear from '@/assets/garderoba/koncnice/sal.svg'

import jopica from '@/assets/garderoba/shared/jopica.svg'
import jopicaWear from '@/assets/garderoba/koncnice/jopica.svg'

import jakna from '@/assets/garderoba/shared/jakna.svg'
import jaknaWear from '@/assets/garderoba/koncnice/jakna.svg'

import majica from '@/assets/garderoba/shared/majica.svg'
import majicaWear from '@/assets/garderoba/koncnice/majica.svg'

import srajca from '@/assets/garderoba/shared/srajca.svg'
import srajcaWear from '@/assets/garderoba/koncnice/srajca.svg'

import trenirka from '@/assets/garderoba/shared/trenirka.svg'
import trenirkaWear from '@/assets/garderoba/koncnice/trenirka.svg'

import kapa from '@/assets/garderoba/shared/kapa.svg'
import kapaWear from '@/assets/garderoba/koncnice/kapa.svg'

import copati from '@/assets/garderoba/shared/copati.svg'
import copatiWear from '@/assets/garderoba/koncnice/copati.svg'

import cevlji from '@/assets/garderoba/shared/cevlji.svg'
import cevljiWear from '@/assets/garderoba/koncnice/cevlji.svg'

import sportniCopati from '@/assets/garderoba/shared/sportni-copati.svg'
import sportniCopatiWear from '@/assets/garderoba/koncnice/sportni-copati.svg'

import hlace from '@/assets/garderoba/shared/hlace.svg'
import hlaceWear from '@/assets/garderoba/koncnice/hlace.svg'

import kavbojke from '@/assets/garderoba/shared/kavbojke.svg'
import kavbojkeWear from '@/assets/garderoba/koncnice/kavbojke.svg'

import rokavice from '@/assets/garderoba/shared/rokavice.svg'
import rokaviceWear from '@/assets/garderoba/koncnice/rokavice.svg'

import nogavice from '@/assets/garderoba/shared/nogavice.svg'
import nogaviceWear from '@/assets/garderoba/koncnice/nogavice.svg'

export const assets = {
  pulover: {
    default: pulover,
    wear: puloverWear
  },
  brezrokavnik: {
    default: brezrokavnik,
    wear: brezrokavnikWear
  },
  sal: {
    default: sal,
    wear: salWear
  },
  jopica: {
    default: jopica,
    wear: jopicaWear
  },
  jakna: {
    default: jakna,
    wear: jaknaWear
  },
  majica: {
    default: majica,
    wear: majicaWear
  },
  srajca: {
    default: srajca,
    wear: srajcaWear
  },
  kapa: {
    default: kapa,
    wear: kapaWear
  },
  trenirka: {
    default: trenirka,
    wear: trenirkaWear
  },
  copati: {
    default: copati,
    wear: copatiWear
  },
  cevlji: {
    default: cevlji,
    wear: cevljiWear
  },
  sportniCopati: {
    default: sportniCopati,
    wear: sportniCopatiWear
  },
  hlace: {
    default: hlace,
    wear: hlaceWear
  },
  kavbojke: {
    default: kavbojke,
    wear: kavbojkeWear
  },
  rokavice: {
    default: rokavice,
    wear: rokaviceWear
  },
  nogavice: {
    default: nogavice,
    wear: nogaviceWear
  }
}
