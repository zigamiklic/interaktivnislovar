import modra from '@/assets/garderoba/shared/kavbojke.svg'
import rumena from '@/assets/garderoba/shared/hlace.svg'
import oranzna from '@/assets/garderoba/shared/jakna.svg'
import vijolicna from '@/assets/garderoba/shared/sal.svg'
import roza from '@/assets/garderoba/shared/jopica.svg'
import crna from '@/assets/garderoba/shared/trenirka.svg'
import zelena from '@/assets/garderoba/shared/majica.svg'
import rdeca from '@/assets/garderoba/shared/pulover.svg'
import bela from '@/assets/garderoba/shared/srajca.svg'
import rjava from '@/assets/garderoba/shared/cevlji.svg'

export default {
  modra,
  rumena,
  oranzna,
  vijolicna,
  roza,
  crna,
  zelena,
  rdeca,
  bela,
  rjava
}
