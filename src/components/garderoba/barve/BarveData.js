export let targetsData = {
  modra: { glyph: 'modra', matchId: 'modra', pos: { x: 134, y: 42 } },
  rumena: { glyph: 'rumena', matchId: 'rumena', pos: { x: 313, y: 42 } },
  oranzna: { glyph: 'oranzna', matchId: 'oranzna', pos: { x: 465, y: 42 } },
  vijolicna: { glyph: 'vijolicna', matchId: 'vijolicna', pos: { x: 690, y: 42 } },
  roza: { glyph: 'roza', matchId: 'roza', pos: { x: 858, y: 42 } },
  crna: { glyph: 'crna', matchId: 'crna', pos: { x: 1046, y: 42 } },
  zelena: { glyph: 'zelena', matchId: 'zelena', pos: { x: 329, y: 338 } },
  rdeca: { glyph: 'rdeca', matchId: 'rdeca', pos: { x: 534, y: 338 } },
  bela: { glyph: 'bela', matchId: 'bela', pos: { x: 791, y: 338 } },
  rjava: { glyph: 'rjava', matchId: 'rjava', pos: { x: 1019, y: 505 } }
}

export let labelsData = {
  roza: { animateTo: 'roza', matchId: 'roza', text: 'To je roza.' },
  modra: { animateTo: 'modra', matchId: 'modra', text: 'To je modra.' },
  rdeca: { animateTo: 'rdeca', matchId: 'rdeca', text: 'To je rdeča.' },
  rumena: { animateTo: 'rumena', matchId: 'rumena', text: 'To je rumena.' },
  rjava: { animateTo: 'rjava', matchId: 'rjava', text: 'To je rjava.' },
  zelena: { animateTo: 'zelena', matchId: 'zelena', text: 'To je zelena.' },
  vijolicna: { animateTo: 'vijolicna', matchId: 'vijolicna', text: 'To je vijolična.' },
  bela: { animateTo: 'bela', matchId: 'bela', text: 'To je bela.' },
  crna: { animateTo: 'crna', matchId: 'crna', text: 'To je črna.' },
  oranzna: { animateTo: 'oranzna', matchId: 'oranzna', text: 'To je oranžna.' }
}
