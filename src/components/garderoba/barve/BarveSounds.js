import bela from '@/assets/sounds/garderoba/1.3/bela.mp3'
import crna from '@/assets/sounds/garderoba/1.3/crna.mp3'
import modra from '@/assets/sounds/garderoba/1.3/modra.mp3'
import oranzna from '@/assets/sounds/garderoba/1.3/oranzna.mp3'
import rdeca from '@/assets/sounds/garderoba/1.3/rdeca.mp3'
import rjava from '@/assets/sounds/garderoba/1.3/rjava.mp3'
import roza from '@/assets/sounds/garderoba/1.3/roza.mp3'
import rumena from '@/assets/sounds/garderoba/1.3/rumena.mp3'
import vijolicna from '@/assets/sounds/garderoba/1.3/vijolicna.mp3'
import zelena from '@/assets/sounds/garderoba/1.3/zelena.mp3'

export default {
  bela,
  crna,
  modra,
  oranzna,
  rdeca,
  rjava,
  roza,
  rumena,
  vijolicna,
  zelena
}
