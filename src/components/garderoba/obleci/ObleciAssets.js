// Items
import kapa from '@/assets/garderoba/obleci/kapa.svg'
import kapaOverlay from '@/assets/garderoba/obleci/kapa-overlay.svg'
import sal from '@/assets/garderoba/obleci/sal.svg'
import rokavice from '@/assets/garderoba/obleci/rokavice.svg'
import rokaviceOverlay from '@/assets/garderoba/obleci/rokavice-overlay.svg'
import sportniCopati from '@/assets/garderoba/obleci/sportni-copati.svg'
import sportniCopatiOverlay from '@/assets/garderoba/obleci/sportni-copati-overlay.svg'
import copati from '@/assets/garderoba/obleci/copati.svg'
import copatiOverlay from '@/assets/garderoba/obleci/copati-overlay.svg'
import cevlji from '@/assets/garderoba/obleci/cevlji.svg'
import cevljiOverlay from '@/assets/garderoba/obleci/cevlji-overlay.svg'
import krilo from '@/assets/garderoba/obleci/krilo.svg'
import trenirka from '@/assets/garderoba/obleci/trenirka.svg'
import trenirkaOverlay from '@/assets/garderoba/obleci/trenirka-overlay.svg'
import trenirkaOverlayMaja from '@/assets/garderoba/obleci/trenirka-overlay-maja.svg'
import kavbojke from '@/assets/garderoba/obleci/kavbojke.svg'
import kavbojkeOverlay from '@/assets/garderoba/obleci/kavbojke-overlay.svg'
import kavbojkeOverlayMaja from '@/assets/garderoba/obleci/kavbojke-overlay-maja.svg'
import hlace from '@/assets/garderoba/obleci/hlace.svg'
import hlaceOverlay from '@/assets/garderoba/obleci/hlace-overlay.svg'
import hlaceOverlayMaja from '@/assets/garderoba/obleci/hlace-overlay-maja.svg'
import jopica from '@/assets/garderoba/obleci/jopica.svg'
import jopicaOverlay from '@/assets/garderoba/obleci/jopica-overlay.svg'
import jopicaOverlayMaja from '@/assets/garderoba/obleci/jopica-overlay-maja.svg'
import majica from '@/assets/garderoba/obleci/majica.svg'
import majicaOverlay from '@/assets/garderoba/obleci/majica-overlay.svg'
import majicaOverlayMaja from '@/assets/garderoba/obleci/majica-overlay-maja.svg'
import pulover from '@/assets/garderoba/obleci/pulover.svg'
import puloverOverlay from '@/assets/garderoba/obleci/pulover-overlay.svg'
import puloverOverlayMaja from '@/assets/garderoba/obleci/pulover-overlay-maja.svg'
import brezrokavnik from '@/assets/garderoba/obleci/brezrokavnik.svg'
import brezrokavnikOverlay from '@/assets/garderoba/obleci/brezrokavnik-overlay.svg'
import brezrokavnikOverlayMaja from '@/assets/garderoba/obleci/brezrokavnik-overlay-maja.svg'
import bunda from '@/assets/garderoba/obleci/bunda.svg'
import bundaOverlay from '@/assets/garderoba/obleci/bunda-overlay.svg'
import bundaOverlayMaja from '@/assets/garderoba/obleci/bunda-overlay-maja.svg'
import srajca from '@/assets/garderoba/obleci/srajca.svg'
import srajcaOverlay from '@/assets/garderoba/obleci/srajca-overlay.svg'
import srajcaOverlayMaja from '@/assets/garderoba/obleci/srajca-overlay-maja.svg'
import jakna from '@/assets/garderoba/obleci/jakna.svg'
import jaknaOverlay from '@/assets/garderoba/obleci/jakna-overlay.svg'
import jaknaOverlayMaja from '@/assets/garderoba/obleci/jakna-overlay-maja.svg'

// Characters
import maja from '@/assets/garderoba/obleci/maja.svg'
import jakob from '@/assets/garderoba/obleci/jakob.svg'

export default {
  kapa,
  kapaOverlay,

  sal,

  rokavice,
  rokaviceOverlay,

  sportniCopati,
  sportniCopatiOverlay,

  copati,
  copatiOverlay,

  cevlji,
  cevljiOverlay,

  krilo,

  trenirka,
  trenirkaOverlay,
  trenirkaOverlayMaja,

  kavbojke,
  kavbojkeOverlay,
  kavbojkeOverlayMaja,

  hlace,
  hlaceOverlay,
  hlaceOverlayMaja,

  jopica,
  jopicaOverlay,
  jopicaOverlayMaja,

  majica,
  majicaOverlay,
  majicaOverlayMaja,

  pulover,
  puloverOverlay,
  puloverOverlayMaja,

  brezrokavnik,
  brezrokavnikOverlay,
  brezrokavnikOverlayMaja,

  bunda,
  bundaOverlay,
  bundaOverlayMaja,

  srajca,
  srajcaOverlay,
  srajcaOverlayMaja,

  jakna,
  jaknaOverlay,
  jaknaOverlayMaja,

  maja,
  jakob
}
