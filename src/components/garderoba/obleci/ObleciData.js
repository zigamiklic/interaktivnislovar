export let targetsData = {
  jakob: { glyph: 'jakob', pos: { x: 492, y: 252 } },
  maja: { glyph: 'maja', pos: { x: 716, y: 257 } }
}

export let itemsData = {
  kapa: {
    glyph: 'kapa',
    overlayGlyph: 'kapaOverlay',
    part: 'head',
    pos: { x: 987, y: 611 },
    posMaja: { x: 722, y: 199 },
    posJakob: { x: 502, y: 199 }
  },
  sal: {
    glyph: 'sal',
    part: 'neck',
    pos: { x: 604, y: 48 },
    posMaja: { x: 753, y: 386 },
    posJakob: { x: 532, y: 389 },
    zIndex: 102
  },
  rokavice: {
    glyph: 'rokavice',
    part: 'hands',
    overlayGlyph: 'rokaviceOverlay',
    hide: true,
    pos: { x: 95, y: 486 },
    posMaja: { x: 706, y: 495 },
    posJakob: { x: 485, y: 501 }
  },
  sportniCopati: {
    glyph: 'sportniCopati',
    overlayGlyph: 'sportniCopatiOverlay',
    part: 'feet',
    hide: true,
    pos: { x: 273, y: 537 },
    posMaja: { x: 759, y: 675 },
    posJakob: { x: 540, y: 676 },
    zIndex: 101
  },
  copati: {
    glyph: 'copati',
    overlayGlyph: 'copatiOverlay',
    part: 'feet',
    hide: true,
    pos: { x: 980, y: 521 },
    posMaja: { x: 729, y: 679 },
    posJakob: { x: 509, y: 678 }
  },
  cevlji: {
    glyph: 'cevlji',
    overlayGlyph: 'cevljiOverlay',
    part: 'feet',
    hide: true,
    pos: { x: 51, y: 622 },
    posMaja: { x: 757, y: 679 },
    posJakob: { x: 537, y: 678 }
  },
  krilo: {
    glyph: 'krilo',
    part: 'legs',
    pos: { x: 960, y: 97 },
    posMaja: { x: 709, y: 489 },
    posJakob: { x: 488, y: 478 }
  },
  trenirka: {
    glyph: 'trenirka',
    overlayGlyph: 'trenirkaOverlay',
    overlayMajaGlyph: 'trenirkaOverlayMaja',
    part: 'legs',
    hide: true,
    pos: { x: 1166, y: 104 },
    posMaja: { x: 762, y: 514 },
    posJakob: { x: 538, y: 508 },
    zIndex: 100
  },
  kavbojke: {
    glyph: 'kavbojke',
    overlayGlyph: 'kavbojkeOverlay',
    overlayMajaGlyph: 'kavbojkeOverlayMaja',
    part: 'legs',
    hide: true,
    pos: { x: 130, y: 105 },
    posMaja: { x: 759, y: 516 },
    posJakob: { x: 536, y: 515 },
    zIndex: 100
  },
  hlace: {
    glyph: 'hlace',
    overlayGlyph: 'hlaceOverlay',
    overlayMajaGlyph: 'hlaceOverlayMaja',
    part: 'legs',
    hide: true,
    pos: { x: 277, y: 105 },
    posMaja: { x: 758, y: 508 },
    posJakob: { x: 533, y: 501 },
    zIndex: 100
  },
  jopica: {
    glyph: 'jopica',
    overlayGlyph: 'jopicaOverlay',
    overlayMajaGlyph: 'jopicaOverlayMaja',
    part: 'body-over',
    pos: { x: 797, y: 66 },
    posMaja: { x: 723, y: 397 },
    posJakob: { x: 501, y: 396 },
    zIndex: 101
  },
  majica: {
    glyph: 'majica',
    overlayGlyph: 'majicaOverlay',
    overlayMajaGlyph: 'majicaOverlayMaja',
    part: 'body',
    pos: { x: 353, y: 296 },
    posMaja: { x: 742, y: 398 },
    posJakob: { x: 513, y: 400 },
    zIndex: 101
  },
  pulover: {
    glyph: 'pulover',
    overlayMajaGlyph: 'puloverOverlayMaja',
    part: 'body',
    pos: { x: 34, y: 288 },
    posMaja: { x: 725, y: 395 },
    posJakob: { x: 504, y: 396 },
    zIndex: 101
  },
  brezrokavnik: {
    glyph: 'brezrokavnik',
    overlayMajaGlyph: 'brezrokavnikOverlayMaja',
    part: 'body-over',
    pos: { x: 1172, y: 535 },
    posMaja: { x: 760, y: 384 },
    posJakob: { x: 529, y: 387 },
    zIndex: 101
  },
  bunda: {
    glyph: 'bunda',
    overlayMajaGlyph: 'bundaOverlayMaja',
    part: 'body-over',
    overlayGlyph: 'bundaOverlay',
    pos: { x: 935, y: 285 },
    posMaja: { x: 724, y: 354 },
    posJakob: { x: 500, y: 357 },
    zIndex: 101
  },
  srajca: {
    glyph: 'srajca',
    overlayGlyph: 'srajcaOverlay',
    overlayMajaGlyph: 'srajcaOverlayMaja',
    part: 'body',
    pos: { x: 1146, y: 286 },
    posMaja: { x: 724, y: 393 },
    posJakob: { x: 503, y: 392 },
    zIndex: 101
  },
  jakna: {
    glyph: 'jakna',
    overlayGlyph: 'jaknaOverlay',
    overlayMajaGlyph: 'jaknaOverlayMaja',
    part: 'body-over',
    pos: { x: 417, y: 48 },
    posMaja: { x: 722, y: 396 },
    posJakob: { x: 502, y: 398 },
    zIndex: 101
  }
}

export let inputsData = {
  kapa: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'kapa',
    posMaja: { x: 912, y: 252 },
    posJakob: { x: 682, y: 242 }
  },
  sal: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'šal',
    posMaja: { x: 862, y: 372 },
    posJakob: { x: 642, y: 374 }
  },
  rokavice: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'rokavice',
    posMaja: { x: 924, y: 502 },
    posJakob: { x: 703, y: 508 }
  },
  sportniCopati: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'športni copati',
    posMaja: { x: 876, y: 670 },
    posJakob: { x: 656, y: 669 }
  },
  copati: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'copati',
    posMaja: { x: 908, y: 667 },
    posJakob: { x: 686, y: 666 }
  },
  cevlji: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'čevlji',
    posMaja: { x: 878, y: 667 },
    posJakob: { x: 666, y: 666 }
  },
  krilo: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'krilo',
    posMaja: { x: 908, y: 525 },
    posJakob: { x: 676, y: 496 }
  },
  trenirka: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'trenirka',
    posMaja: { x: 878, y: 565 },
    posJakob: { x: 665, y: 556 }
  },
  kavbojke: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'kavbojke',
    posMaja: { x: 878, y: 565 },
    posJakob: { x: 665, y: 556 }
  },
  hlace: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To so',
    solution: 'hlače',
    posMaja: { x: 878, y: 565 },
    posJakob: { x: 665, y: 556 }
  },
  jopica: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'jopica',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  },
  majica: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'majica',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  },
  pulover: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'pulover',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  },
  brezrokavnik: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'brezrokavnik',
    posMaja: { x: 878, y: 445 },
    posJakob: { x: 665, y: 436 }
  },
  bunda: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'bunda',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  },
  srajca: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'srajca',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  },
  jakna: {
    openDirection: 'right',
    arrowDirection: 'left',
    preText: 'To je',
    solution: 'jakna',
    posMaja: { x: 908, y: 445 },
    posJakob: { x: 695, y: 436 }
  }
}
