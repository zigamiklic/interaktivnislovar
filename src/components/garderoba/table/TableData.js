export let soundsData = new Map([
  ['GarderobaTableEdina', {
    brezrokavnik: '2.1.1/brezrokavnik.mp3',
    bunda: '2.1.1/bunda.mp3',
    deznik: '2.1.1/deznik.mp3',
    jakna: '2.1.1/jakna.mp3',
    jopica: '2.1.1/jopica.mp3',
    kapa: '2.1.1/kapa.mp3',
    krilo: '2.1.1/krilo.mp3',
    majica: '2.1.1/majica.mp3',
    pulover: '2.1.1/pulover.mp3',
    sal: '2.1.1/sal.mp3',
    srajca: '2.1.1/srajca.mp3',
    trenirka: '2.1.1/trenirka.mp3'
  }],

  ['GarderobaTableMnozina', {
    cevlji: '2.1.2/cevlji.mp3',
    copati: '2.1.2/copati.mp3',
    hlace: '2.1.2/hlace.mp3',
    kavbojke: '2.1.2/kavbojke.mp3',
    nogavice: '2.1.2/nogavice.mp3',
    rokavice: '2.1.2/rokavice.mp3',
    sportniCopati: '2.1.2/sportni-copati.mp3',
  }]
])

export let labelsData = new Map([
  // Ednina
  ['GarderobaTableEdina', {
    // Moški spol
    pulover: { matchId: 'b1', text: 'pulover' },
    brezrokavnik: { matchId: 'b1', text: 'brezrokavnik' },
    sal: { matchId: 'b1', text: 'šal' },
    deznik: { matchId: 'b1', text: 'dežnik' },

    // Ženski spol
    jopica: { matchId: 'b2', text: 'jopica' },
    jakna: { matchId: 'b2', text: 'jakna' },
    bunda: { matchId: 'b2', text: 'bunda' },
    majica: { matchId: 'b2', text: 'majica' },
    srajca: { matchId: 'b2', text: 'srajca' },
    trenirka: { matchId: 'b2', text: 'trenirka' },
    kapa: { matchId: 'b2', text: 'kapa' },

    // Srednji spol
    krilo: { matchId: 'b3', text: 'krilo' }
  }],

  ['GarderobaTableMnozina', {
    // Moški spol
    copati: { matchId: 'b1', text: 'copati' },
    cevlji: { matchId: 'b1', text: 'čevlji' },
    sportniCopati: { matchId: 'b1', text: 'športni copati' },

    // Ženski spol
    hlace: { matchId: 'b2', text: 'hlače' },
    kavbojke: { matchId: 'b2', text: 'kavbojke' },
    rokavice: { matchId: 'b2', text: 'rokavice' },
    nogavice: { matchId: 'b2', text: 'nogavice' }
  }]
])
