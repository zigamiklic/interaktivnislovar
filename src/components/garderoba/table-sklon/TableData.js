export let soundsData = new Map([
  ['GarderobaTableSklonMoski', {
    pulover: '2.2.1.4/pulover.mp3',
    brezrokavnik: '2.2.1.4/brezrokavnik.mp3',
    sal: '2.2.1.4/sal.mp3',

    pulover2: '2.2.1.4/pulover2.mp3',
    brezrokavnik2: '2.2.1.4/brezrokavnik2.mp3',
    sal2: '2.2.1.4/sal2.mp3',

    copati: '2.2.1.4/copati.mp3',
    cevlji: '2.2.1.4/cevlji.mp3',
    sportniCopati: '2.2.1.4/sportni-copati.mp3',

    copati2: '2.2.1.4/copati2.mp3',
    cevlji2: '2.2.1.4/cevlji2.mp3',
    sportniCopati2: '2.2.1.4/sportni-copati2.mp3',
  }],

  ['GarderobaTableSklonZenski', {
    jopica: '2.2.2.4/jopica.mp3',
    jakna: '2.2.2.4/jakna.mp3',
    bunda: '2.2.2.4/bunda.mp3',
    majica: '2.2.2.4/majica.mp3',
    srajca: '2.2.2.4/srajca.mp3',
    trenirka: '2.2.2.4/trenirka.mp3',
    kapa: '2.2.2.4/kapa.mp3',

    jopica2: '2.2.2.4/jopica2.mp3',
    jakna2: '2.2.2.4/jakna2.mp3',
    bunda2: '2.2.2.4/bunda2.mp3',
    majica2: '2.2.2.4/majica2.mp3',
    srajca2: '2.2.2.4/srajca2.mp3',
    trenirka2: '2.2.2.4/trenirka2.mp3',
    kapa2: '2.2.2.4/kapa2.mp3',

    hlace: '2.2.2.4/hlace.mp3',
    kavbojke: '2.2.2.4/kavbojke.mp3',
    rokavice: '2.2.2.4/rokavice.mp3',
    nogavice: '2.2.2.4/nogavice.mp3',

    hlace2: '2.2.2.4/hlace2.mp3',
    kavbojke2: '2.2.2.4/kavbojke2.mp3',
    rokavice2: '2.2.2.4/rokavice2.mp3',
    nogavice2: '2.2.2.4/nogavice2.mp3'
  }]
])

export const randomItemsData = {
  GarderobaTableSklonMoski: {
    b1: ['pulover', 'brezrokavnik', 'sal'],
    b2: ['copati', 'cevlji', 'sportniCopati']
  },
  GarderobaTableSklonZenski: {
    b1: ['jopica', 'jakna', 'bunda', 'majica', 'srajca', 'trenirka', 'kapa'],
    b2: ['hlace', 'kavbojke', 'rokavice', 'nogavice']
  }
}

export let labelsData = new Map([
  // Ednina
  ['GarderobaTableSklonMoski', {
    // Moški spol - left
    pulover: { matchId: ['b1-left', 'b1-right'], text: 'pulover' },
    brezrokavnik: { matchId: ['b1-left', 'b1-right'], text: 'brezrokavnik' },
    sal: { matchId: ['b1-left', 'b1-right'], text: 'šal' },

    // Moški spol - right
    pulover2: { matchId: ['b1-right', 'b1-left'], text: 'pulover' },
    brezrokavnik2: { matchId: ['b1-right', 'b1-left'], text: 'brezrokavnik' },
    sal2: { matchId: ['b1-right', 'b1-left'], text: 'šal' },

    // Moški spol - left
    copati: { matchId: 'b2-left', text: 'copati' },
    cevlji: { matchId: 'b2-left', text: 'čevlji' },
    sportniCopati: { matchId: 'b2-left', text: 'športni copati' },

    // Moški spol - right
    copati2: { matchId: 'b2-right', text: 'copate' },
    cevlji2: { matchId: 'b2-right', text: 'čevlje' },
    sportniCopati2: { matchId: 'b2-right', text: 'športne copate' }
  }],

  ['GarderobaTableSklonZenski', {
    // Ženski spol - left
    jopica: { matchId: 'b1-left', text: 'jopica' },
    jakna: { matchId: 'b1-left', text: 'jakna' },
    bunda: { matchId: 'b1-left', text: 'bunda' },
    majica: { matchId: 'b1-left', text: 'majica' },
    srajca: { matchId: 'b1-left', text: 'srajca' },
    trenirka: { matchId: 'b1-left', text: 'trenirka' },
    kapa: { matchId: 'b1-left', text: 'kapa' },

    // Ženski spol - right
    jopica2: { matchId: 'b1-right', text: 'jopico' },
    jakna2: { matchId: 'b1-right', text: 'jakno' },
    bunda2: { matchId: 'b1-right', text: 'bundo' },
    majica2: { matchId: 'b1-right', text: 'majico' },
    srajca2: { matchId: 'b1-right', text: 'srajco' },
    trenirka2: { matchId: 'b1-right', text: 'trenirko' },
    kapa2: { matchId: 'b1-right', text: 'kapo' },

    // Ženski spol - left
    hlace: { matchId: ['b2-left', 'b2-right'], text: 'hlače' },
    kavbojke: { matchId: ['b2-left', 'b2-right'], text: 'kavbojke' },
    rokavice: { matchId: ['b2-left', 'b2-right'], text: 'rokavice' },
    nogavice: { matchId: ['b2-left', 'b2-right'], text: 'nogavice' },

    // Ženski spol - right
    hlace2: { matchId: ['b2-right', 'b2-left'], text: 'hlače' },
    kavbojke2: { matchId: ['b2-right', 'b2-left'], text: 'kavbojke' },
    rokavice2: { matchId: ['b2-right', 'b2-left'], text: 'rokavice' },
    nogavice2: { matchId: ['b2-right', 'b2-left'], text: 'nogavice' }
  }]
])
