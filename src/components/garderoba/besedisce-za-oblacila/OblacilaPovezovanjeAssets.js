import bunda from '@/assets/garderoba/shared/bunda.svg'
import brezrokavnik from '@/assets/garderoba/shared/brezrokavnik.svg'
import deznik from '@/assets/garderoba/shared/deznik.svg'
import nogavice from '@/assets/garderoba/shared/nogavice.svg'
import vezalke from '@/assets/garderoba/shared/vezalke.svg'
import jopica from '@/assets/garderoba/shared/jopica.svg'
import kapa from '@/assets/garderoba/shared/kapa.svg'
import rokavice from '@/assets/garderoba/shared/rokavice.svg'
import sportniCopati from '@/assets/garderoba/shared/sportni-copati.svg'
import kavbojke from '@/assets/garderoba/shared/kavbojke.svg'
import krilo from '@/assets/garderoba/shared/krilo.svg'
import sal from '@/assets/garderoba/shared/sal.svg'
import hlace from '@/assets/garderoba/shared/hlace.svg'
import cevlji from '@/assets/garderoba/shared/cevlji.svg'
import majica from '@/assets/garderoba/shared/majica.svg'
import pulover from '@/assets/garderoba/shared/pulover.svg'
import srajca from '@/assets/garderoba/shared/srajca.svg'
import copati from '@/assets/garderoba/shared/copati.svg'
import trenirka from '@/assets/garderoba/shared/trenirka.svg'
import jakna from '@/assets/garderoba/shared/jakna.svg'

export default {
  bunda,
  brezrokavnik,
  deznik,
  nogavice,
  vezalke,
  jopica,
  kapa,
  rokavice,
  sportniCopati,
  kavbojke,
  krilo,
  sal,
  hlace,
  cevlji,
  majica,
  pulover,
  srajca,
  copati,
  trenirka,
  jakna
}
