import brezrokavnik from '@/assets/sounds/garderoba/1.2/brezrokavnik.mp3'
import bunda from '@/assets/sounds/garderoba/1.2/bunda.mp3'
import cevlji from '@/assets/sounds/garderoba/1.2/cevlji.mp3'
import copati from '@/assets/sounds/garderoba/1.2/copati.mp3'
import deznik from '@/assets/sounds/garderoba/1.2/deznik.mp3'
import hlace from '@/assets/sounds/garderoba/1.2/hlace.mp3'
import jakna from '@/assets/sounds/garderoba/1.2/jakna.mp3'
import jopica from '@/assets/sounds/garderoba/1.2/jopica.mp3'
import kapa from '@/assets/sounds/garderoba/1.2/kapa.mp3'
import kavbojke from '@/assets/sounds/garderoba/1.2/kavbojke.mp3'
import krilo from '@/assets/sounds/garderoba/1.2/krilo.mp3'
import majica from '@/assets/sounds/garderoba/1.2/majica.mp3'
import nogavice from '@/assets/sounds/garderoba/1.2/nogavice.mp3'
import pulover from '@/assets/sounds/garderoba/1.2/pulover.mp3'
import rokavice from '@/assets/sounds/garderoba/1.2/rokavice.mp3'
import sal from '@/assets/sounds/garderoba/1.2/sal.mp3'
import sportniCopati from '@/assets/sounds/garderoba/1.2/sportniCopati.mp3'
import srajca from '@/assets/sounds/garderoba/1.2/srajca.mp3'
import trenirka from '@/assets/sounds/garderoba/1.2/trenirka.mp3'
import vezalke from '@/assets/sounds/garderoba/1.2/vezalke.mp3'

export default {
  brezrokavnik,
  bunda,
  cevlji,
  copati,
  deznik,
  hlace,
  jakna,
  jopica,
  kapa,
  kavbojke,
  krilo,
  majica,
  nogavice,
  pulover,
  rokavice,
  sal,
  sportniCopati,
  srajca,
  trenirka,
  vezalke
}
