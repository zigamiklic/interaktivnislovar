import caj from '@/assets/sounds/1.1.3/caj.mp3'
import jogurt from '@/assets/sounds/1.1.3/jogurt.mp3'
import kakav from '@/assets/sounds/1.1.3/kakav.mp3'
import kosmici from '@/assets/sounds/1.1.3/kosmici.mp3'
import kruh from '@/assets/sounds/1.1.3/kruh.mp3'
import makovka from '@/assets/sounds/1.1.3/makovka.mp3'
import marmelada from '@/assets/sounds/1.1.3/marmelada.mp3'
import maslo from '@/assets/sounds/1.1.3/maslo.mp3'
import med from '@/assets/sounds/1.1.3/med.mp3'
import mleko from '@/assets/sounds/1.1.3/mleko.mp3'
import pasteta from '@/assets/sounds/1.1.3/pasteta.mp3'
import salama from '@/assets/sounds/1.1.3/salama.mp3'
import sir from '@/assets/sounds/1.1.3/sir.mp3'
import zemlja from '@/assets/sounds/1.1.3/zemlja.mp3'

export default {
  caj,
  jogurt,
  kakav,
  kosmici,
  kruh,
  makovka,
  marmelada,
  maslo,
  med,
  mleko,
  pasteta,
  salama,
  sir,
  zemlja
}
