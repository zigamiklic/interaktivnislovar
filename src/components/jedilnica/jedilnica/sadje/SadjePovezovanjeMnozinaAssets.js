import banane from '@/assets/jedilnica/jedilnica/sadje/banana.svg'
import hruske from '@/assets/jedilnica/jedilnica/sadje/hruska.svg'
import limone from '@/assets/jedilnica/jedilnica/sadje/limona.svg'
import jagode from '@/assets/jedilnica/jedilnica/sadje/jagoda.svg'
import slive from '@/assets/jedilnica/jedilnica/sadje/sliva.svg'
import pomarance from '@/assets/jedilnica/jedilnica/sadje/pomaranca.svg'
import marelice from '@/assets/jedilnica/jedilnica/sadje/marelica.svg'
import maline from '@/assets/jedilnica/jedilnica/sadje/malina.svg'
import borovnice from '@/assets/jedilnica/jedilnica/sadje/borovnica.svg'
import cesnje from '@/assets/jedilnica/jedilnica/sadje/cesnja.svg'
import breskve from '@/assets/jedilnica/jedilnica/sadje/breskev.svg'
import jabolka from '@/assets/jedilnica/jedilnica/sadje/jabolko.svg'

export default {
  banane,
  hruske,
  limone,
  pomarance,
  marelice,
  maline,
  borovnice,
  cesnje,
  slive,
  jagode,
  breskve,
  jabolka
}
