export let targetsData = new Map([
  ['SadjePovezovanjeEdnina', {
    banana: {matchId: 'banana', pos: {x: 370, y: 36}},
    hruska: {matchId: 'hruska', pos: {x: 404, y: 238}},
    limona: {matchId: 'limona', pos: {x: 176, y: 36}},
    sliva: { matchId: 'sliva', pos: { x: 800, y: 161 }, size: { width: 63, height: 80 }},
    jagoda: {matchId: 'jagoda', pos: {x: 816, y: 260}, size: { width: 80, height: 80 }},
    pomaranca: {matchId: 'pomaranca', pos: {x: 1140, y: 296}},
    marelica: { matchId: 'marelica', pos: { x: 1156, y: 124 }, size: { width: 80, height: 61 }},
    malina: { matchId: 'malina', pos: { x: 698, y: 53 }, size: { width: 68, height: 80 }},
    borovnica: {matchId: 'borovnica', pos: {x: 648, y: 160}},
    cesnja: { matchId: 'cesnja', pos: { x: 871, y: 40 }, size: { width: 43, height: 100 }},
    lubenica: {matchId: 'lubenica', pos: {x: 910, y: 316}},
    ananas: {matchId: 'ananas', pos: {x: 950, y: 28}},
    kivi: {matchId: 'kivi', pos: {x: 138, y: 176}},
    breskev: {matchId: 'breskev', pos: {x: 158, y: 350}},
    jabolko: {matchId: 'jabolko', pos: {x: 628, y: 294}}
  }],
  ['SadjePovezovanjeMnozina', {
    banane: { matchId: 'banane', pos: { x: 410, y: 16 } },
    hruske: { matchId: 'hruske', pos: { x: 404, y: 238 } },
    limone: { matchId: 'limone', pos: { x: 176, y: 36 } },
    slive: { matchId: 'slive', pos: { x: 800, y: 161 } },
    jagode: { matchId: 'jagode', pos: { x: 916, y: 260 } },
    pomarance: { matchId: 'pomarance', pos: { x: 1140, y: 296 } },
    marelice: { matchId: 'marelice', pos: { x: 1126, y: 124 } },
    maline: { matchId: 'maline', pos: { x: 758, y: 53 } },
    borovnice: { matchId: 'borovnice', pos: { x: 648, y: 160 } },
    cesnje: { matchId: 'cesnje', pos: { x: 971, y: 40 } },
    breskve: { matchId: 'breskve', pos: { x: 128, y: 290 } },
    jabolka: { matchId: 'jabolka', pos: { x: 678, y: 294 } }
  }]
])

export let labelsData = new Map([
  ['SadjePovezovanjeEdnina', {
    banana: {animateTo: 'banana', matchId: 'banana', text: 'To je banana.'},
    hruska: {animateTo: 'hruska', matchId: 'hruska', text: 'To je hruška.'},
    jagoda: {animateTo: 'jagoda', matchId: 'jagoda', text: 'To je jagoda.'},
    limona: {animateTo: 'limona', matchId: 'limona', text: 'To je limona.'},
    sliva: {animateTo: 'sliva', matchId: 'sliva', text: 'To je sliva.'},
    pomaranca: {animateTo: 'pomaranca', matchId: 'pomaranca', text: 'To je pomaranča.'},
    marelica: {animateTo: 'marelica', matchId: 'marelica', text: 'To je marelica.'},
    malina: {animateTo: 'malina', matchId: 'malina', text: 'To je malina.'},
    borovnica: {animateTo: 'borovnica', matchId: 'borovnica', text: 'To je borovnica.'},
    cesnja: {animateTo: 'cesnja', matchId: 'cesnja', text: 'To je češnja.'},
    breskev: {animateTo: 'breskev', matchId: 'breskev', text: 'To je breskev.'},
    ananas: {animateTo: 'ananas', matchId: 'ananas', text: 'To je ananas.'},
    lubenica: {animateTo: 'lubenica', matchId: 'lubenica', text: 'To je lubenica.'},
    kivi: {animateTo: 'kivi', matchId: 'kivi', text: 'To je kivi.'},
    jabolko: {animateTo: 'jabolko', matchId: 'jabolko', text: 'To je jabolko.'}
  }],
  ['SadjePovezovanjeMnozina', {
    banane: { animateTo: 'banane', matchId: 'banane', text: 'To so banane.' },
    hruske: { animateTo: 'hruske', matchId: 'hruske', text: 'To so hruške.' },
    jagode: { animateTo: 'jagode', matchId: 'jagode', text: 'To so jagode.' },
    limone: { animateTo: 'limone', matchId: 'limone', text: 'To so limone.' },
    slive: { animateTo: 'slive', matchId: 'slive', text: 'To so slive.' },
    pomarance: { animateTo: 'pomarance', matchId: 'pomarance', text: 'To so pomaranče.' },
    marelice: { animateTo: 'marelice', matchId: 'marelice', text: 'To so marelice.' },
    maline: { animateTo: 'maline', matchId: 'maline', text: 'To so maline.' },
    borovnice: { animateTo: 'borovnice', matchId: 'borovnice', text: 'To so borovnice.' },
    cesnje: { animateTo: 'cesnje', matchId: 'cesnje', text: 'To so češnje.' },
    breskve: { animateTo: 'breskve', matchId: 'breskve', text: 'To so breskve.' },
    jabolka: { animateTo: 'jabolka', matchId: 'jabolka', text: 'To so jabolka.' }
  }]
])
