import banana from '@/assets/shared/banana.svg'
import hruska from '@/assets/shared/hruska.svg'
import jagoda from '@/assets/shared/jagoda.svg'
import limona from '@/assets/shared/limona.svg'
import sliva from '@/assets/shared/sliva.svg'
import pomaranca from '@/assets/shared/pomaranca.svg'
import marelica from '@/assets/shared/marelica.svg'
import malina from '@/assets/shared/malina.svg'
import borovnica from '@/assets/shared/borovnica.svg'
import cesnja from '@/assets/shared/cesnja.svg'
import breskev from '@/assets/shared/breskev.svg'
import ananas from '@/assets/jedilnica/jedilnica/sadje/ananas.svg'
import lubenica from '@/assets/jedilnica/jedilnica/sadje/lubenica.svg'
import kivi from '@/assets/jedilnica/jedilnica/sadje/kivi.svg'
import jabolko from '@/assets/shared/jabolko.svg'

export default {
  banana,
  hruska,
  limona,
  pomaranca,
  marelica,
  malina,
  borovnica,
  cesnja,
  sliva,
  jagoda,
  ananas,
  lubenica,
  kivi,
  breskev,
  jabolko
}
