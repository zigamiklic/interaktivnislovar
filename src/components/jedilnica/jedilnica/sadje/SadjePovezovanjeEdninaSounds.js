import banana from '@/assets/sounds/2.6/banana.mp3'
import hruska from '@/assets/sounds/2.6/hruska.mp3'
import jagoda from '@/assets/sounds/2.6/jagoda.mp3'
import limona from '@/assets/sounds/2.6/limona.mp3'
import sliva from '@/assets/sounds/2.6/sliva.mp3'
import pomaranca from '@/assets/sounds/2.6/pomaranca.mp3'
import marelica from '@/assets/sounds/2.6/marelica.mp3'
import malina from '@/assets/sounds/2.6/malina.mp3'
import borovnica from '@/assets/sounds/2.6/borovnica.mp3'
import cesnja from '@/assets/sounds/2.6/cesnja.mp3'
import breskev from '@/assets/sounds/2.6/breskev.mp3'
import ananas from '@/assets/sounds/1.1.1/ananas.mp3'
import lubenica from '@/assets/sounds/1.1.1/lubenica.mp3'
import kivi from '@/assets/sounds/1.1.1/kivi.mp3'
import jabolko from '@/assets/sounds/2.6/jabolko.mp3'

export default {
  banana,
  hruska,
  limona,
  pomaranca,
  marelica,
  malina,
  borovnica,
  cesnja,
  sliva,
  jagoda,
  ananas,
  lubenica,
  kivi,
  breskev,
  jabolko
}
