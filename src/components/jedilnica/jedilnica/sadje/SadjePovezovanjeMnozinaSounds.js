import banane from '@/assets/sounds/2.6/banane.mp3'
import hruske from '@/assets/sounds/2.6/hruske.mp3'
import jagode from '@/assets/sounds/2.6/jagode.mp3'
import limone from '@/assets/sounds/2.6/limone.mp3'
import slive from '@/assets/sounds/2.6/slive.mp3'
import pomarance from '@/assets/sounds/2.6/pomarance.mp3'
import marelice from '@/assets/sounds/2.6/marelice.mp3'
import maline from '@/assets/sounds/1.1.1/maline.mp3'
import borovnice from '@/assets/sounds/1.1.1/borovnice.mp3'
import cesnje from '@/assets/sounds/1.1.1/cesnje.mp3'
import breskve from '@/assets/sounds/2.6/breskve.mp3'
import jabolka from '@/assets/sounds/2.6/jabolka.mp3'

export default {
  banane,
  hruske,
  limone,
  pomarance,
  marelice,
  maline,
  borovnice,
  cesnje,
  slive,
  jagode,
  breskve,
  jabolka
}
