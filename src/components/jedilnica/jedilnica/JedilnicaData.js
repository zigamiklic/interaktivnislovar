import voda from '@/assets/jedilnica/kosare/malica/voda.svg'
import sendvic from '@/assets/shared/sendvic.svg'

export let lines = {
  jakob: 'To je jedilnica.',
  lacen: 'Lačen sem.',
  zejen: 'Žejen sem.',
  lacna: 'Lačna sem.',
  zejna: 'Žejna sem.',
  jem: 'Jem.',
  pijem: 'Pijem.'
}

export let glyphs = {
  pijem: voda,
  zejen: voda,
  zejna: voda,

  lacen: sendvic,
  lacna: sendvic,
  jem: sendvic
}
