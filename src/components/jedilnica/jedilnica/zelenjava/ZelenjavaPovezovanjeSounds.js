import bucka from '@/assets/sounds/1.1.2/bucka.mp3'
import cebula from '@/assets/sounds/1.1.2/cebula.mp3'
import cesen from '@/assets/sounds/1.1.2/cesen.mp3'
import cvetaca from '@/assets/sounds/1.1.2/cvetaca.mp3'
import fizol from '@/assets/sounds/1.1.2/fizol.mp3'
import grah from '@/assets/sounds/1.1.2/grah.mp3'
import korencek from '@/assets/sounds/1.1.2/korencek.mp3'
import koruza from '@/assets/sounds/1.1.2/koruza.mp3'
import krompir from '@/assets/sounds/1.1.2/krompir.mp3'
import kumara from '@/assets/sounds/1.1.2/kumara.mp3'
import paprika from '@/assets/sounds/1.1.2/paprika.mp3'
import paradiznik from '@/assets/sounds/1.1.2/paradiznik.mp3'
import por from '@/assets/sounds/1.1.2/por.mp3'
import spinaca from '@/assets/sounds/1.1.2/spinaca.mp3'

export default {
  bucka,
  cvetaca,
  grah,
  korencek,
  koruza,
  fizol,
  cesen,
  cebula,
  krompir,
  kumara,
  paprika,
  paradiznik,
  por,
  spinaca
}
