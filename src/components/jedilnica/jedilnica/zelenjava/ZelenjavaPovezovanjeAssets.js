import bucka from '@/assets/shared/bucke.svg'
import cebula from '@/assets/jedilnica/jedilnica/zelenjava/cebula.svg'
import cesen from '@/assets/jedilnica/jedilnica/zelenjava/cesen.svg'
import cvetaca from '@/assets/jedilnica/jedilnica/zelenjava/cvetaca.svg'
import fizol from '@/assets/jedilnica/jedilnica/zelenjava/fizol.svg'
import grah from '@/assets/jedilnica/jedilnica/zelenjava/grah.svg'
import korencek from '@/assets/jedilnica/jedilnica/zelenjava/korencek.svg'
import koruza from '@/assets/jedilnica/jedilnica/zelenjava/koruza.svg'
import krompir from '@/assets/jedilnica/jedilnica/zelenjava/krompir.svg'
import kumara from '@/assets/shared/kumare.svg'
import paprika from '@/assets/shared/paprike.svg'
import paradiznik from '@/assets/jedilnica/jedilnica/zelenjava/paradiznik.svg'
import por from '@/assets/jedilnica/jedilnica/zelenjava/por.svg'
import spinaca from '@/assets/jedilnica/jedilnica/zelenjava/spinaca.svg'

export default {
  bucka,
  cvetaca,
  grah,
  korencek,
  koruza,
  fizol,
  cesen,
  cebula,
  krompir,
  kumara,
  paprika,
  paradiznik,
  por,
  spinaca
}
