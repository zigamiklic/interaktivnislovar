import jakob from '@/assets/sounds/1.1/jedilnica/jakob.mp3'
import zejen from '@/assets/sounds/1.1/jedilnica/zejen.mp3'
import lacen from '@/assets/sounds/1.1/jedilnica/lacen.mp3'
import zejna from '@/assets/sounds/1.1/jedilnica/zejna.mp3'
import lacna from '@/assets/sounds/1.1/jedilnica/lacna.mp3'
import jem from '@/assets/sounds/1.1/jedilnica/jem.mp3'
import pijem from '@/assets/sounds/1.1/jedilnica/pijem.mp3'

export default {
  jakob,
  zejen,
  lacen,
  zejna,
  lacna,
  jem,
  pijem
}
