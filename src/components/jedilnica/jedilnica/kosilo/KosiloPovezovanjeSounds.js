import cokolada from '@/assets/sounds/1.1.4/cokolada.mp3'
import juha from '@/assets/sounds/1.1.4/juha.mp3'
import krompir from '@/assets/sounds/1.1.4/krompir.mp3'
import makaroni from '@/assets/sounds/1.1.4/makaroni.mp3'
import meso from '@/assets/sounds/1.1.4/meso.mp3'
import palacinke from '@/assets/sounds/1.1.4/palacinke.mp3'
import pica from '@/assets/sounds/1.1.4/pica.mp3'
import riba from '@/assets/sounds/1.1.4/riba.mp3'
import riz from '@/assets/sounds/1.1.4/riz.mp3'
import sladoled from '@/assets/sounds/1.1.4/sladoled.mp3'
import sok from '@/assets/sounds/1.1.4/sok.mp3'
import solata from '@/assets/sounds/1.1.4/solata.mp3'
import spageti from '@/assets/sounds/1.1.4/spageti.mp3'
import torta from '@/assets/sounds/1.1.4/torta.mp3'

export default {
  cokolada,
  juha,
  krompir,
  makaroni,
  meso,
  palacinke,
  pica,
  riba,
  riz,
  sladoled,
  sok,
  solata,
  spageti,
  torta
}
