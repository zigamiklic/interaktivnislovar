export let targetsData = new Map([
  ['KosiloPovezovanje', {
    juha1: {glyph: 'juha', matchId: 'juha', pos: {x: 190, y: 142}},
    meso: {glyph: 'meso', matchId: 'meso', pos: {x: 288, y: 153}},
    krompir: {glyph: 'krompir', matchId: 'krompir', pos: {x: 403, y: 154}},
    solata1: {glyph: 'solata', matchId: 'solata', pos: {x: 514, y: 142}},
    sladoled: {glyph: 'sladoled', matchId: 'sladoled', pos: {x: 626, y: 130}},

    juha2: {glyph: 'juha', matchId: 'juha', pos: {x: 170, y: 250}},
    spageti: {glyph: 'spageti', matchId: 'spageti', pos: {x: 300, y: 250}},
    solata2: {glyph: 'solata', matchId: 'solata', pos: {x: 452, y: 250}},
    palacinke: {glyph: 'palacinke', matchId: 'palacinke', pos: {x: 570, y: 248}},

    juha3: {glyph: 'juha', matchId: 'juha', pos: {x: 175, y: 353}},
    riba: {glyph: 'riba', matchId: 'riba', pos: {x: 284, y: 356}},
    riz: {glyph: 'riz', matchId: 'riz', pos: {x: 444, y: 362}},
    cokolada: {glyph: 'cokolada', matchId: 'cokolada', pos: {x: 580, y: 345}},

    pica: {glyph: 'pica', matchId: 'pica', pos: {x: 255, y: 448}},
    solata3: {glyph: 'solata', matchId: 'solata', pos: {x: 392, y: 450}},
    sok: {glyph: 'sok', matchId: 'sok', pos: {x: 526, y: 442}},

    juha4: {glyph: 'juha', matchId: 'juha', pos: {x: 180, y: 554}},
    makaroni: {glyph: 'makaroni', matchId: 'makaroni', pos: {x: 292, y: 557}},
    solata4: {glyph: 'solata', matchId: 'solata', pos: {x: 450, y: 554}},
    torta: {glyph: 'torta', matchId: 'torta', pos: {x: 570, y: 560}}
  }]
])

export let labelsData = new Map([
  ['KosiloPovezovanje', {
    cokolada: {animateTo: 'cokolada', matchId: 'cokolada', text: 'To je čokolada.'},
    juha: {animateTo: 'juha1', matchId: 'juha', text: 'To je juha.'},
    krompir: {animateTo: 'krompir', matchId: 'krompir', text: 'To je krompir.'},
    makaroni: {animateTo: 'makaroni', matchId: 'makaroni', text: 'To so makaroni.'},
    meso: {animateTo: 'meso', matchId: 'meso', text: 'To je meso.'},
    palacinke: {animateTo: 'palacinke', matchId: 'palacinke', text: 'To so palačinke.'},
    pica: {animateTo: 'pica', matchId: 'pica', text: 'To je pica.'},
    riba: {animateTo: 'riba', matchId: 'riba', text: 'To je riba.'},
    riz: {animateTo: 'riz', matchId: 'riz', text: 'To je riž.'},
    sladoled: {animateTo: 'sladoled', matchId: 'sladoled', text: 'To je sladoled.'},
    sok: {animateTo: 'sok', matchId: 'sok', text: 'To je sok.'},
    solata: {animateTo: 'solata1', matchId: 'solata', text: 'To je zelena solata.'},
    spageti: {animateTo: 'spageti', matchId: 'spageti', text: 'To so špageti.'},
    torta: {animateTo: 'torta', matchId: 'torta', text: 'To je torta.'}
  }]
])
