import cokolada from '@/assets/jedilnica/jedilnica/kosilo/cokolada.svg'
import juha from '@/assets/jedilnica/jedilnica/kosilo/juha.svg'
import krompir from '@/assets/jedilnica/jedilnica/kosilo/krompir.svg'
import makaroni from '@/assets/jedilnica/jedilnica/kosilo/makaroni.svg'
import meso from '@/assets/jedilnica/jedilnica/kosilo/meso.svg'
import palacinke from '@/assets/jedilnica/jedilnica/kosilo/palacinke.svg'
import pica from '@/assets/jedilnica/jedilnica/kosilo/pica.svg'
import riba from '@/assets/jedilnica/jedilnica/kosilo/ribe.svg'
import riz from '@/assets/jedilnica/jedilnica/kosilo/riz.svg'
import sladoled from '@/assets/jedilnica/jedilnica/kosilo/sladoled.svg'
import sok from '@/assets/jedilnica/jedilnica/kosilo/sok.svg'
import solata from '@/assets/jedilnica/jedilnica/kosilo/solata.svg'
import spageti from '@/assets/jedilnica/jedilnica/kosilo/spageti.svg'
import torta from '@/assets/jedilnica/jedilnica/kosilo/torta.svg'

export default {
  cokolada,
  juha,
  krompir,
  makaroni,
  meso,
  palacinke,
  pica,
  riba,
  riz,
  sladoled,
  sok,
  solata,
  spageti,
  torta
}
