import ananas from '@/assets/jedilnica/kosare/mesanaHrana/ananas.svg'
import banane from '@/assets/jedilnica/kosare/mesanaHrana/banane.svg'
import borovnice from '@/assets/jedilnica/kosare/mesanaHrana/borovnice.svg'
import breskve from '@/assets/jedilnica/kosare/mesanaHrana/breskve.svg'
import bucka from '@/assets/jedilnica/kosare/mesanaHrana/bucka.svg'
import caj from '@/assets/jedilnica/kosare/mesanaHrana/caj.svg'
import cebula from '@/assets/jedilnica/kosare/mesanaHrana/cebula.svg'
import cesen from '@/assets/jedilnica/kosare/mesanaHrana/cesen.svg'
import cesnje from '@/assets/jedilnica/kosare/mesanaHrana/cesnje.svg'
import cokolada from '@/assets/jedilnica/kosare/mesanaHrana/cokolada.svg'
import cvetaca from '@/assets/jedilnica/kosare/mesanaHrana/cvetaca.svg'
import fizol from '@/assets/jedilnica/kosare/mesanaHrana/fizol.svg'
import grah from '@/assets/jedilnica/kosare/mesanaHrana/grah.svg'
import hruske from '@/assets/jedilnica/kosare/mesanaHrana/hruske.svg'
import jabolka from '@/assets/jedilnica/kosare/mesanaHrana/jabolka.svg'
import jagode from '@/assets/jedilnica/kosare/mesanaHrana/jagode.svg'
import jogurt from '@/assets/jedilnica/kosare/mesanaHrana/jogurt.svg'
import juha from '@/assets/jedilnica/kosare/mesanaHrana/juha.svg'
import kakav from '@/assets/jedilnica/kosare/mesanaHrana/kakav.svg'
import kivi from '@/assets/jedilnica/kosare/mesanaHrana/kivi.svg'
import korenje from '@/assets/jedilnica/kosare/mesanaHrana/korenje.svg'
import koruza from '@/assets/jedilnica/kosare/mesanaHrana/koruza.svg'
import kosmici from '@/assets/jedilnica/kosare/mesanaHrana/kosmici.svg'
import kozarec from '@/assets/jedilnica/kosare/mesanaHrana/kozarec.svg'
import krompir from '@/assets/jedilnica/kosare/mesanaHrana/krompir.svg'
import kroznik from '@/assets/jedilnica/kosare/mesanaHrana/kroznik.svg'
import kruh from '@/assets/jedilnica/kosare/mesanaHrana/kruh.svg'
import kumara from '@/assets/jedilnica/kosare/mesanaHrana/kumara.svg'
import limone from '@/assets/jedilnica/kosare/mesanaHrana/limone.svg'
import lubenica from '@/assets/jedilnica/kosare/mesanaHrana/lubenica.svg'
import makaroni from '@/assets/jedilnica/kosare/mesanaHrana/makaroni.svg'
import makovka from '@/assets/jedilnica/kosare/mesanaHrana/makovka.svg'
import maline from '@/assets/jedilnica/kosare/mesanaHrana/maline.svg'
import marelice from '@/assets/jedilnica/kosare/mesanaHrana/marelice.svg'
import marmelada from '@/assets/jedilnica/kosare/mesanaHrana/marmelada.svg'
import maslo from '@/assets/jedilnica/kosare/mesanaHrana/maslo.svg'
import med from '@/assets/jedilnica/kosare/mesanaHrana/med.svg'
import meso from '@/assets/jedilnica/kosare/mesanaHrana/meso.svg'
import mleko from '@/assets/jedilnica/kosare/mesanaHrana/mleko.svg'
import noz from '@/assets/jedilnica/kosare/mesanaHrana/noz.svg'
import palacinke from '@/assets/jedilnica/kosare/mesanaHrana/palacinke.svg'
import paprika from '@/assets/jedilnica/kosare/mesanaHrana/paprika.svg'
import paradiznik from '@/assets/jedilnica/kosare/mesanaHrana/paradiznik.svg'
import pasteta from '@/assets/jedilnica/kosare/mesanaHrana/pasteta.svg'
import pica from '@/assets/jedilnica/kosare/mesanaHrana/pica.svg'
import pladenj from '@/assets/jedilnica/kosare/mesanaHrana/pladenj.svg'
import pomarance from '@/assets/jedilnica/kosare/mesanaHrana/pomarance.svg'
import por from '@/assets/jedilnica/kosare/mesanaHrana/por.svg'
import prticek from '@/assets/jedilnica/kosare/mesanaHrana/prticek.svg'
import riba from '@/assets/jedilnica/kosare/mesanaHrana/riba.svg'
import riz from '@/assets/jedilnica/kosare/mesanaHrana/riz.svg'
import salama from '@/assets/jedilnica/kosare/mesanaHrana/salama.svg'
import sir from '@/assets/jedilnica/kosare/mesanaHrana/sir.svg'
import skodelica from '@/assets/jedilnica/kosare/mesanaHrana/skodelica.svg'
import sladoled from '@/assets/jedilnica/kosare/mesanaHrana/sladoled.svg'
import slive from '@/assets/jedilnica/kosare/mesanaHrana/slive.svg'
import sok from '@/assets/jedilnica/kosare/mesanaHrana/sok.svg'
import solata from '@/assets/jedilnica/kosare/mesanaHrana/solata.svg'
import spageti from '@/assets/jedilnica/kosare/mesanaHrana/spageti.svg'
import spinaca from '@/assets/jedilnica/kosare/mesanaHrana/spinaca.svg'
import torta from '@/assets/jedilnica/kosare/mesanaHrana/torta.svg'
import vilica from '@/assets/jedilnica/kosare/mesanaHrana/vilica.svg'
import voda from '@/assets/jedilnica/kosare/mesanaHrana/voda.svg'
import zemlja from '@/assets/jedilnica/kosare/mesanaHrana/zemlja.svg'
import zlica from '@/assets/jedilnica/kosare/mesanaHrana/zlica.svg'

export default {
  ananas,
  banane,
  borovnice,
  breskve,
  bucka,
  caj,
  cebula,
  cesen,
  cesnje,
  cokolada,
  cvetaca,
  fizol,
  grah,
  hruske,
  jabolka,
  jagode,
  jogurt,
  juha,
  kakav,
  kivi,
  korenje,
  koruza,
  kosmici,
  kozarec,
  krompir,
  kroznik,
  kruh,
  kumara,
  limone,
  lubenica,
  makaroni,
  makovka,
  maline,
  marelice,
  marmelada,
  maslo,
  med,
  meso,
  mleko,
  noz,
  palacinke,
  paprika,
  paradiznik,
  pasteta,
  pica,
  pladenj,
  pomarance,
  por,
  prticek,
  riba,
  riz,
  salama,
  sir,
  skodelica,
  sladoled,
  slive,
  sok,
  solata,
  spageti,
  spinaca,
  torta,
  vilica,
  voda,
  zemlja,
  zlica
}
