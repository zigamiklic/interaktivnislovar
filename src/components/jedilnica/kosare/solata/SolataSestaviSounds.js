import ana1 from '@/assets/sounds/1.3.4.1/ana1.mp3'
import ana2 from '@/assets/sounds/1.3.4.1/ana2.mp3'
import ana3 from '@/assets/sounds/1.3.4.1/ana3.mp3'
import jakob1 from '@/assets/sounds/1.3.4.1/jakob1.mp3'
import jakob2 from '@/assets/sounds/1.3.4.1/jakob2.mp3'
import jakob3 from '@/assets/sounds/1.3.4.1/jakob3.mp3'
import jakob4 from '@/assets/sounds/1.3.4.1/jakob4.mp3'

export default {
  ana1,
  ana2,
  ana3,
  jakob1,
  jakob2,
  jakob3,
  jakob4
}
