import bucka from '@/assets/jedilnica/kosare/solata/bucka.svg'
import cebula from '@/assets/jedilnica/kosare/solata/cebula.svg'
import cesen from '@/assets/jedilnica/kosare/solata/cesen.svg'
import cvetaca from '@/assets/jedilnica/kosare/solata/cvetaca.svg'
import fizol from '@/assets/jedilnica/kosare/solata/fizol.svg'
import grah from '@/assets/jedilnica/kosare/solata/grah.svg'
import korenje from '@/assets/jedilnica/kosare/solata/korenje.svg'
import koruza from '@/assets/jedilnica/kosare/solata/koruza.svg'
import krompir from '@/assets/jedilnica/kosare/solata/krompir.svg'
import kroznik from '@/assets/jedilnica/kosare/solata/kroznik.svg'
import kumara from '@/assets/jedilnica/kosare/solata/kumara.svg'
import paprika from '@/assets/jedilnica/kosare/solata/paprika.svg'
import paradiznik from '@/assets/jedilnica/kosare/solata/paradiznik.svg'
import por from '@/assets/jedilnica/kosare/solata/por.svg'
import spinaca from '@/assets/jedilnica/kosare/solata/spinaca.svg'

export default {
  bucka,
  cebula,
  cesen,
  cvetaca,
  fizol,
  grah,
  korenje,
  koruza,
  krompir,
  kroznik,
  kumara,
  paprika,
  paradiznik,
  por,
  spinaca
}
