export let targetsData = new Map([
  ['SolataSestavi', {
    kroznik: {glyph: 'kroznik', matchId: 'kroznik', pos: {x: 270, y: 584}}
  }]
])

export let itemsData = new Map([
  ['SolataSestavi', {
    bucka: {glyph: 'bucka', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 34, y: 492}},
    cebula: {glyph: 'cebula', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 640, y: 525}},
    cesen: {glyph: 'cesen', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 1045, y: 555}},
    cvetaca: {glyph: 'cvetaca', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 685, y: 430}},
    fizol: {glyph: 'fizol', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 515, y: 504}},
    grah: {glyph: 'grah', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 119, y: 637}},
    korenje: {glyph: 'korenje', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 1138, y: 578}},
    koruza: {glyph: 'koruza', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 865, y: 535}},
    krompir: {glyph: 'krompir', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 878, y: 616}},
    kumara: {glyph: 'kumara', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 14, y: 587}},
    paprika: {glyph: 'paprika', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 490, y: 609}},
    paradiznik: {glyph: 'paradiznik', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 752, y: 565}},
    por: {glyph: 'por', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 574, y: 585}},
    spinaca: {glyph: 'spinaca', animateTo: 'kroznik', matchId: 'kroznik', pos: {x: 1140, y: 480}}
  }]
])

export let inputsData = new Map([
  ['SolataSestavi', {
    bucka: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'bučka', pos: {x: 188, y: 198} },
    cebula: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'čebula', pos: {x: 188, y: 198} },
    cesen: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'česen', pos: {x: 188, y: 198} },
    cvetaca: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'cvetača', pos: {x: 188, y: 198} },
    fizol: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'korenje', pos: {x: 188, y: 198} },
    grah: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'grah', pos: {x: 188, y: 198} },
    korenje: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'korenje', pos: { x: 188, y: 198 } },
    koruza: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'koruza', pos: {x: 188, y: 198} },
    krompir: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'krompir', pos: {x: 188, y: 198} },
    kumara: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'kumara', pos: {x: 188, y: 198} },
    paprika: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'paprika', pos: {x: 188, y: 198} },
    paradiznik: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'paradižnik', pos: {x: 188, y: 198} },
    por: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'por', pos: {x: 188, y: 198} },
    spinaca: { openDirection: 'right', arrowDirection: 'left', preText: 'To je', solution: 'špinaca', pos: {x: 188, y: 198} }
  }]])

export let linesData = new Map([
  ['SolataSestavi', {
    ana1: 'Kaj je v tvoji solati?',
    jakob1: 'V moji solati so',
    paprika: 'paprika,',
    paradiznik: 'in paradižnik.',
    cvetaca: 'cvetača',
    poskusi: 'Zdaj pa poskusi še ti.',
    ana2: 'Kaj je v tvoji solati?',
    doberTek: 'Dober tek.'
  }]
])
