export let targetsData = new Map([
  ['KosiloSestavi', {
    pladenj: { glyph: 'pladenj', matchId: 'pladenj', pos: { x: 180, y: 550 } }
  }]
])

export let itemsData = new Map([
  ['KosiloSestavi', {
    cokolada: { glyph: 'cokolada', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1130, y: 610 } },
    juha: { glyph: 'juha', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 585, y: 450 } },
    krompir: { glyph: 'krompir', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 707, y: 620 } },
    makaroni: { glyph: 'makaroni', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 983, y: 635 } },
    meso: { glyph: 'meso', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 31, y: 493 } },
    palacinke: { glyph: 'palacinke', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 90, y: 548 } },
    pica: { glyph: 'pica', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 561, y: 537 } },
    ribe: { glyph: 'ribe', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 752, y: 532 } },
    riz: { glyph: 'riz', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1090, y: 493 } },
    sladoled: { glyph: 'sladoled', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 730, y: 400 } },
    solata: { glyph: 'solata', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1007, y: 545 } },
    spageti: { glyph: 'spageti', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 858, y: 613 } },
    torta: { glyph: 'torta', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 15, y: 606 } },
    voda: { glyph: 'voda', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 836, y: 423 } },
    sok: { glyph: 'sok', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 152, y: 425 } },
    caj: { glyph: 'caj', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 576, y: 621 } }
  }]
])

const preText = 'Za kosilo jem'
const preTextDrink = 'Za kosilo pijem'

export let inputsData = new Map([
  ['KosiloSestavi', {
    cokolada: { preText, solution: 'čokolado' },
    juha: { preText, solution: 'juho' },
    krompir: { preText, solution: 'krompir' },
    makaroni: { preText, solution: 'makarone' },
    meso: { preText, solution: 'meso' },
    palacinke: { preText, solution: 'palačinke' },
    pica: { preText, solution: 'pico' },
    ribe: { preText, solution: 'ribo' },
    riz: { preText, solution: 'riž' },
    sladoled: { preText, solution: 'sladoled' },
    solata: { preText, solution: 'solato' },
    spageti: { preText, solution: 'špagete' },
    torta: { preText, solution: 'torto' },
    voda: { preText: preTextDrink, solution: 'vodo' },
    sok: { preText: preTextDrink, solution: 'sok' },
    caj: { preText: preTextDrink, solution: 'čaj' }
  }]])

export let linesData = new Map([
  ['KosiloSestavi', {
    ana1: 'Kaj ješ in piješ za kosilo?',
    jakob1: 'Za kosilo jem',
    juha: 'juho,',
    meso: 'meso',
    krompir: 'in krompir.',
    poskusi: 'Zdaj pa poskusi še ti.',
    ana2: 'Kaj ješ in piješ za kosilo?',
    doberTek: 'Dober tek.'
  }]
])
