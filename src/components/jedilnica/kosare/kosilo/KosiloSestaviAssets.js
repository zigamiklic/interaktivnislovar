import cokolada from '@/assets/jedilnica/kosare/kosilo/cokolada.svg'
import juha from '@/assets/jedilnica/kosare/kosilo/juha.svg'
import krompir from '@/assets/jedilnica/kosare/kosilo/krompir.svg'
import makaroni from '@/assets/jedilnica/kosare/kosilo/makaroni.svg'
import meso from '@/assets/jedilnica/kosare/kosilo/meso.svg'
import palacinke from '@/assets/jedilnica/kosare/kosilo/palacinke.svg'
import pica from '@/assets/jedilnica/kosare/kosilo/pica.svg'
import ribe from '@/assets/jedilnica/kosare/kosilo/ribe.svg'
import riz from '@/assets/jedilnica/kosare/kosilo/riz.svg'
import sladoled from '@/assets/jedilnica/kosare/kosilo/sladoled.svg'
import solata from '@/assets/jedilnica/kosare/kosilo/solata.svg'
import spageti from '@/assets/jedilnica/kosare/kosilo/spageti.svg'
import torta from '@/assets/jedilnica/kosare/kosilo/torta.svg'

import voda from '@/assets/jedilnica/kosare/malica/voda.svg'
import sok from '@/assets/jedilnica/kosare/malica/sok.svg'
import caj from '@/assets/jedilnica/jedilnica/malica/caj.svg'

import pladenj from '@/assets/jedilnica/kosare/kosilo/pladenj.svg'

export default {
  cokolada,
  juha,
  krompir,
  makaroni,
  meso,
  palacinke,
  pica,
  ribe,
  riz,
  sladoled,
  solata,
  spageti,
  torta,
  voda,
  sok,
  caj,
  pladenj
}
