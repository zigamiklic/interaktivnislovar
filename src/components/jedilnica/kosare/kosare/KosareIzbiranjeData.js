export let itemsData = new Map([
  ['KosareIzbiranje', [
    { name: 'ananas', label: 'ananas' },
    { name: 'banane', label: 'banane', plural: true },
    { name: 'borovnice', label: 'borovnice', plural: true },
    { name: 'breskve', label: 'breskve', plural: true },
    { name: 'hruske', label: 'hruške', plural: true },
    { name: 'jabolka', label: 'jabolka', plural: true },
    { name: 'jagode', label: 'jagode', plural: true },
    { name: 'kivi', label: 'kivi' },
    { name: 'limone', label: 'limone', plural: true },
    { name: 'lubenica', label: 'lubenica' },
    { name: 'maline', label: 'maline', plural: true },
    { name: 'marelice', label: 'marelice', plural: true },
    { name: 'pomarance', label: 'pomaranče', plural: true },
    { name: 'slive', label: 'slive', plural: true },
    { name: 'spinaca', label: 'špinača' },
    { name: 'por', label: 'por' },
    { name: 'paprika', label: 'paprika' },
    { name: 'paradiznik', label: 'paradižnik' },
    { name: 'korencek', label: 'korenček' },
    { name: 'krompir', label: 'krompir' },
    { name: 'kumara', label: 'kumara' },
    { name: 'cvetaca', label: 'cvetača' },
    { name: 'fizol', label: 'fižol' },
    { name: 'grah', label: 'grah' },
    { name: 'bucka', label: 'bučka' },
    { name: 'cebula', label: 'čebula' },
    { name: 'cesen', label: 'česen' },
    { name: 'cesnje', label: 'češnje', plural: true }
  ]]
])

export let platePositions = [
  { x: 232, y: 450 },
  { x: 656, y: 450 },
  { x: 1074, y: 450 }
]
