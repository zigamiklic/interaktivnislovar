import ananas from '@/assets/sounds/2.6/ananas.mp3'
import banane from '@/assets/sounds/2.6/banane.mp3'
import borovnice from '@/assets/sounds/2.6/borovnice.mp3'
import breskve from '@/assets/sounds/2.6/breskve.mp3'
import hruske from '@/assets/sounds/2.6/hruske.mp3'
import jabolka from '@/assets/sounds/2.6/jabolka.mp3'
import jagode from '@/assets/sounds/2.6/jagode.mp3'
import kivi from '@/assets/sounds/1.1.1/kivi.mp3'
import limone from '@/assets/sounds/2.6/limone.mp3'
import lubenica from '@/assets/sounds/1.1.1/lubenica.mp3'
import maline from '@/assets/sounds/2.6/maline.mp3'
import marelice from '@/assets/sounds/2.6/marelice.mp3'
import pomarance from '@/assets/sounds/2.6/pomarance.mp3'
import slive from '@/assets/sounds/2.6/slive.mp3'
import spinaca from '@/assets/sounds/1.1.2/spinaca.mp3'
import por from '@/assets/sounds/1.1.2/por.mp3'
import paprika from '@/assets/sounds/2.6/paprika.mp3'
import paradiznik from '@/assets/sounds/2.6/paradiznik.mp3'
import korencek from '@/assets/sounds/1.1.2/korencek.mp3'
import koruza from '@/assets/sounds/1.1.2/koruza.mp3'
import krompir from '@/assets/sounds/1.1.2/krompir.mp3'
import kumara from '@/assets/sounds/2.6/kumara.mp3'
import cvetaca from '@/assets/sounds/1.1.2/cvetaca.mp3'
import fizol from '@/assets/sounds/1.1.2/fizol.mp3'
import grah from '@/assets/sounds/1.1.2/grah.mp3'
import bucka from '@/assets/sounds/2.6/bucka.mp3'
import cebula from '@/assets/sounds/1.1.2/cebula.mp3'
import cesen from '@/assets/sounds/1.1.2/cesen.mp3'
import cesnje from '@/assets/sounds/2.6/cesnje.mp3'

export default {
  ananas,
  banane,
  borovnice,
  breskve,
  hruske,
  jabolka,
  jagode,
  kivi,
  limone,
  lubenica,
  maline,
  marelice,
  pomarance,
  slive,
  spinaca,
  por,
  paprika,
  paradiznik,
  korencek,
  koruza,
  krompir,
  kumara,
  cvetaca,
  fizol,
  grah,
  bucka,
  cebula,
  cesen,
  cesnje
}
