const itemsData = [
  { name: 'caj', label: 'To je čaj.' },
  { name: 'cokolada', label: 'To je čokolada.' },
  { name: 'jogurt', label: 'To je jogurt.' },
  { name: 'juha', label: 'To je juha.' },
  { name: 'kakav', label: 'To je kakav.' },
  { name: 'kosmici', label: 'To so kosmiči.' },
  { name: 'krompir', label: 'To je krompir.' },
  { name: 'kruh', label: 'To je kruh.' },
  { name: 'makaroni', label: 'To so makaroni.' },
  { name: 'makovka', label: 'To je makovka.' },
  { name: 'marmelada', label: 'To je marmelada.' },
  { name: 'maslo', label: 'To je maslo.' },
  { name: 'med', label: 'To je med.' },
  { name: 'meso', label: 'To je meso.' },
  { name: 'mleko', label: 'To je mleko.' },
  { name: 'palacinke', label: 'To so palačinke.' },
  { name: 'pasteta', label: 'To je pašteta.' },
  { name: 'pica', label: 'To je pica.' },
  { name: 'riba', label: 'To je riba.' },
  { name: 'riz', label: 'To je riž.' },
  { name: 'salama', label: 'To je salama.' },
  { name: 'sir', label: 'To je sir.' },
  { name: 'sladoled', label: 'To je sladoled.' },
  { name: 'sok', label: 'To je sok.' },
  { name: 'solata', label: 'To je solata.' },
  { name: 'spageti', label: 'To so špageti.' },
  { name: 'torta', label: 'To je torta.' },
  { name: 'voda', label: 'To je voda.' },
  { name: 'zemlja', label: 'To je žemlja.' }
]

export {
  itemsData
}
