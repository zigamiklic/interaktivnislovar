import caj from '@/assets/jedilnica/kosare/spominMalicaKosilo/caj.svg'
import cokolada from '@/assets/jedilnica/kosare/spominMalicaKosilo/cokolada.svg'
import jogurt from '@/assets/jedilnica/kosare/spominMalicaKosilo/jogurt.svg'
import juha from '@/assets/jedilnica/kosare/spominMalicaKosilo/juha.svg'
import kakav from '@/assets/jedilnica/kosare/spominMalicaKosilo/kakav.svg'
import kosmici from '@/assets/jedilnica/kosare/spominMalicaKosilo/kosmici.svg'
import krompir from '@/assets/jedilnica/kosare/spominMalicaKosilo/krompir.svg'
import kruh from '@/assets/jedilnica/kosare/spominMalicaKosilo/kruh.svg'
import makaroni from '@/assets/jedilnica/kosare/spominMalicaKosilo/makaroni.svg'
import makovka from '@/assets/jedilnica/kosare/spominMalicaKosilo/makovka.svg'
import marmelada from '@/assets/jedilnica/kosare/spominMalicaKosilo/marmelada.svg'
import maslo from '@/assets/jedilnica/kosare/spominMalicaKosilo/maslo.svg'
import med from '@/assets/jedilnica/kosare/spominMalicaKosilo/med.svg'
import meso from '@/assets/jedilnica/kosare/spominMalicaKosilo/meso.svg'
import mleko from '@/assets/jedilnica/kosare/spominMalicaKosilo/mleko.svg'
import palacinke from '@/assets/jedilnica/kosare/spominMalicaKosilo/palacinke.svg'
import pasteta from '@/assets/jedilnica/kosare/spominMalicaKosilo/pasteta.svg'
import pica from '@/assets/jedilnica/kosare/spominMalicaKosilo/pica.svg'
import riba from '@/assets/jedilnica/kosare/spominMalicaKosilo/riba.svg'
import riz from '@/assets/jedilnica/kosare/spominMalicaKosilo/riz.svg'
import salama from '@/assets/jedilnica/kosare/spominMalicaKosilo/salama.svg'
import sir from '@/assets/jedilnica/kosare/spominMalicaKosilo/sir.svg'
import sladoled from '@/assets/jedilnica/kosare/spominMalicaKosilo/sladoled.svg'
import sok from '@/assets/jedilnica/kosare/spominMalicaKosilo/sok.svg'
import solata from '@/assets/jedilnica/kosare/spominMalicaKosilo/solata.svg'
import spageti from '@/assets/jedilnica/kosare/spominMalicaKosilo/spageti.svg'
import torta from '@/assets/jedilnica/kosare/spominMalicaKosilo/torta.svg'
import voda from '@/assets/jedilnica/kosare/spominMalicaKosilo/voda.svg'
import zemlja from '@/assets/jedilnica/kosare/spominMalicaKosilo/zemlja.svg'

export default {
  caj,
  cokolada,
  jogurt,
  juha,
  kakav,
  kosmici,
  krompir,
  kruh,
  makaroni,
  makovka,
  marmelada,
  maslo,
  med,
  meso,
  mleko,
  palacinke,
  pasteta,
  pica,
  riba,
  riz,
  salama,
  sir,
  sladoled,
  sok,
  solata,
  spageti,
  torta,
  voda,
  zemlja
}
