let targetsData = []

for (let i = 0; i < 8; i++) {
  targetsData.push({ x: 581, y: 77 + (77 * i) })
}

export { targetsData }

export let itemsSmall = [
  { name: 'cesen', word: 'česen' },
  { name: 'fizol', word: 'fižol' },
  { name: 'grah', word: 'grah' },
  { name: 'paprika', word: 'paprika' },
  { name: 'spinaca', word: 'špinača' },
  { name: 'borovnice', word: 'borovnice' },
  { name: 'breskve', word: 'breskve' },
  { name: 'cesnje', word: 'češnje' },
  { name: 'jagode', word: 'jagode' },
  { name: 'kiviji', word: 'kiviji' },
  { name: 'limone', word: 'limone' },
  { name: 'lubenica', word: 'lubenica' },
  { name: 'maline', word: 'maline' },
  { name: 'pomarance', word: 'pomaranče' }
]

export let itemsLarge = [
  { name: 'ananas', word: 'ananas' },
  { name: 'banane', word: 'banane' },
  { name: 'cebula', word: 'čebula' },
  { name: 'bucka', word: 'bučka' },
  { name: 'cvetaca', word: 'cvetača' },
  { name: 'korenje', word: 'korenček' },
  { name: 'hruske', word: 'hruške' },
  { name: 'koruza', word: 'koruza' },
  { name: 'krompir', word: 'krompir' },
  { name: 'kumara', word: 'kumara' },
  { name: 'jabolka', word: 'jabolka' },
  { name: 'marelice', word: 'marelice' },
  { name: 'paradiznik', word: 'paradižnik' },
  { name: 'por', word: 'por' },
  { name: 'slive', word: 'slive' }
]

export let tableItemsData = [
  { size: 'lg', x: 980, y: 250 },
  { size: 'lg', x: 1225, y: 260 },
  { size: 'sm', x: 895, y: 339 },
  { size: 'sm', x: 1126, y: 339 },

  { size: 'sm', x: 935, y: 490 },
  { size: 'sm', x: 1083, y: 493 },
  { size: 'sm', x: 935, y: 570 },
  { size: 'lg', x: 1236, y: 545 }
]
