import bucka from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/bucka.svg'
import cebula from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/cebula.svg'
import cesen from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/cesen.svg'
import cvetaca from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/cvetaca.svg'
import fizol from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/fizol.svg'
import grah from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/grah.svg'
import korenje from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/korenje.svg'
import koruza from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/koruza.svg'
import krompir from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/krompir.svg'
import kumara from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/kumara.svg'
import paprika from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/paprika.svg'
import paradiznik from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/paradiznik.svg'
import por from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/por.svg'
import spinaca from '@/assets/jedilnica/kosare/pomesaneCrke/zelenjava/spinaca.svg'

import ananas from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/ananas.svg'
import banane from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/banane.svg'
import borovnice from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/borovnice.svg'
import breskve from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/breskve.svg'
import cesnje from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/cesnje.svg'
import hruske from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/hruske.svg'
import jabolka from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/jabolka.svg'
import jagode from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/jagode.svg'
import kiviji from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/kiviji.svg'
import limone from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/limone.svg'
import lubenica from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/lubenica.svg'
import maline from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/maline.svg'
import marelice from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/marelice.svg'
import pomarance from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/pomarance.svg'
import slive from '@/assets/jedilnica/kosare/pomesaneCrke/sadje/slive.svg'

import itemImg from '@/assets/jedilnica/kosare/pomesaneCrke/item-img.svg'
import item from '@/assets/jedilnica/kosare/pomesaneCrke/item.svg'
import correct from '@/assets/jedilnica/kosare/pomesaneCrke/correct.svg'
import wrong from '@/assets/jedilnica/kosare/pomesaneCrke/wrong.svg'

export default {
  bucka,
  cebula,
  cesen,
  cvetaca,
  fizol,
  grah,
  korenje,
  koruza,
  krompir,
  kumara,
  paprika,
  paradiznik,
  por,
  spinaca,

  ananas,
  banane,
  borovnice,
  breskve,
  cesnje,
  hruske,
  jabolka,
  jagode,
  kiviji,
  limone,
  lubenica,
  maline,
  marelice,
  pomarance,
  slive,

  itemImg,
  item,
  correct,
  wrong
}
