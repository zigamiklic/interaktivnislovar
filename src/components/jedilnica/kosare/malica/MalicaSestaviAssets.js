import caj from '@/assets/jedilnica/kosare/malica/caj.svg'
import jogurt from '@/assets/jedilnica/kosare/malica/jogurt.svg'
import kakav from '@/assets/jedilnica/kosare/malica/kakav.svg'
import kosmici from '@/assets/jedilnica/kosare/malica/kosmici.svg'
import kruh from '@/assets/jedilnica/kosare/malica/kruh.svg'
import makovka from '@/assets/jedilnica/kosare/malica/makovka.svg'
import marmelada from '@/assets/jedilnica/kosare/malica/marmelada.svg'
import maslo from '@/assets/jedilnica/kosare/malica/maslo.svg'
import med from '@/assets/jedilnica/kosare/malica/med.svg'
import mleko from '@/assets/jedilnica/kosare/malica/mleko.svg'
import pasteta from '@/assets/jedilnica/kosare/malica/pasteta.svg'
import sok from '@/assets/jedilnica/kosare/malica/sok.svg'
import salama from '@/assets/jedilnica/kosare/malica/salama.svg'
import sir from '@/assets/jedilnica/kosare/malica/sir.svg'
import voda from '@/assets/jedilnica/kosare/malica/voda.svg'
import zemlja from '@/assets/jedilnica/kosare/malica/zemlja.svg'

import pladenj from '@/assets/jedilnica/kosare/kosilo/pladenj.svg'

export default {
  caj,
  jogurt,
  kakav,
  kosmici,
  kruh,
  makovka,
  marmelada,
  maslo,
  med,
  mleko,
  pasteta,
  sok,
  salama,
  sir,
  voda,
  zemlja,
  pladenj
}
