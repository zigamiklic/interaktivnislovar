export let targetsData = new Map([
  ['MalicaSestavi', {
    pladenj: { glyph: 'pladenj', matchId: 'pladenj', pos: { x: 180, y: 550 } }
  }]
])

export let itemsData = new Map([
  ['MalicaSestavi', {
    caj: { glyph: 'caj', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 16, y: 545 } },
    jogurt: { glyph: 'jogurt', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 654, y: 414 } },
    kakav: { glyph: 'kakav', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 561, y: 528 } },
    kosmici: { glyph: 'kosmici', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 876, y: 618 } },
    kruh: { glyph: 'kruh', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 651, y: 560 } },
    makovka: { glyph: 'makovka', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1129, y: 624 } },
    marmelada: { glyph: 'marmelada', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1071, y: 514 } },
    maslo: { glyph: 'maslo', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 580, y: 635 } },
    med: { glyph: 'med', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 759, y: 426 } },
    mleko: { glyph: 'mleko', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 1170, y: 450 } },
    pasteta: { glyph: 'pasteta', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 141, y: 521 } },
    sok: { glyph: 'sok', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 31, y: 425 } },
    salama: { glyph: 'salama', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 840, y: 540 } },
    sir: { glyph: 'sir', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 53, y: 626 } },
    voda: { glyph: 'voda', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 996, y: 579 } },
    zemlja: { glyph: 'zemlja', animateTo: 'pladenj', matchId: 'pladenj', pos: { x: 766, y: 610 } }
  }]
])

const eat = 'Za malico jem'
const drink = 'Za malico pijem'

export let inputsData = new Map([
  ['MalicaSestavi', {
    caj: { preText: drink, solution: 'čaj' },
    jogurt: { preText: eat, solution: 'jogurt' },
    kakav: { preText: drink, solution: 'kakav' },
    kosmici: { preText: eat, solution: 'kosmiče' },
    kruh: { preText: eat, solution: 'kruh' },
    makovka: { preText: eat, solution: 'makovko' },
    marmelada: { preText: eat, solution: 'marmelado' },
    maslo: { preText: eat, solution: 'maslo' },
    med: { preText: eat, solution: 'med' },
    mleko: { preText: drink, solution: 'mleko' },
    pasteta: { preText: eat, solution: 'pašteto' },
    sok: { preText: drink, solution: 'sok' },
    salama: { preText: eat, solution: 'salamo' },
    sir: { preText: eat, solution: 'sir' },
    voda: { preText: drink, solution: 'vodo' },
    zemlja: { preText: eat, solution: 'žemljo' }
  }]])

export let linesData = new Map([
  ['MalicaSestavi', {
    ana1: 'Kaj ješ in piješ za malico?',
    jakob1: 'Za malico jem',
    kruh: 'kruh',
    maslo: 'in maslo.',
    mleko: 'Pijem mleko.',
    poskusi: 'Zdaj pa poskusi še ti.',
    ana2: 'Kaj ješ in piješ za malico?',
    doberTek: 'Dober tek.'
  }]
])
