const itemsData = [
  { name: 'ananas', label: 'To je ananas.' },
  { name: 'banana', label: 'To je banana.' },
  { name: 'borovnica', label: 'To je borovnica.' },
  { name: 'breskev', label: 'To je breskev.' },
  { name: 'bucka', label: 'To je bučka.' },
  { name: 'cebula', label: 'To je čebula.' },
  { name: 'cesen', label: 'To je česen.' },
  { name: 'cesnja', label: 'To je češnja.' },
  { name: 'cvetaca', label: 'To je cvetača.' },
  { name: 'fizol', label: 'To je fižol.' },
  { name: 'grah', label: 'To je grah.' },
  { name: 'hruska', label: 'To je hruška.' },
  { name: 'jabolko', label: 'To je jabolko.' },
  { name: 'kivi', label: 'To je kivi.' },
  { name: 'korenje', label: 'To je korenje.' },
  { name: 'koruza', label: 'To je koruza.' },
  { name: 'krompir', label: 'To je krompir.' },
  { name: 'kumara', label: 'To je kumara.' },
  { name: 'limona', label: 'To je limona.' },
  { name: 'lubenica', label: 'To je lubenica.' },
  { name: 'malina', label: 'To je malina.' },
  { name: 'marelica', label: 'To je marelica.' },
  { name: 'paprika', label: 'To je paprika.' },
  { name: 'paradiznik', label: 'To je paradižnik.' },
  { name: 'pomaranca', label: 'To je pomaranča.' },
  { name: 'por', label: 'To je por.' },
  { name: 'sliva', label: 'To je sliva.' },
  { name: 'spinaca', label: 'To je špinača.' }
]

export {
  itemsData
}
