import ananas from '@/assets/jedilnica/kosare/spominSadjeZelenjava/ananas.svg'
import banana from '@/assets/jedilnica/kosare/spominSadjeZelenjava/banana.svg'
import borovnica from '@/assets/jedilnica/kosare/spominSadjeZelenjava/borovnica.svg'
import breskev from '@/assets/jedilnica/kosare/spominSadjeZelenjava/breskev.svg'
import bucka from '@/assets/jedilnica/kosare/spominSadjeZelenjava/bucka.svg'
import cebula from '@/assets/jedilnica/kosare/spominSadjeZelenjava/cebula.svg'
import cesen from '@/assets/jedilnica/kosare/spominSadjeZelenjava/cesen.svg'
import cesnja from '@/assets/jedilnica/kosare/spominSadjeZelenjava/cesnja.svg'
import cvetaca from '@/assets/jedilnica/kosare/spominSadjeZelenjava/cvetaca.svg'
import fizol from '@/assets/jedilnica/kosare/spominSadjeZelenjava/fizol.svg'
import grah from '@/assets/jedilnica/kosare/spominSadjeZelenjava/grah.svg'
import hruska from '@/assets/jedilnica/kosare/spominSadjeZelenjava/hruska.svg'
import jabolko from '@/assets/jedilnica/kosare/spominSadjeZelenjava/jabolko.svg'
import jagoda from '@/assets/jedilnica/kosare/spominSadjeZelenjava/jagoda.svg'
import kivi from '@/assets/jedilnica/kosare/spominSadjeZelenjava/kivi.svg'
import korenje from '@/assets/jedilnica/kosare/spominSadjeZelenjava/korenje.svg'
import koruza from '@/assets/jedilnica/kosare/spominSadjeZelenjava/koruza.svg'
import krompir from '@/assets/jedilnica/kosare/spominSadjeZelenjava/krompir.svg'
import kumara from '@/assets/jedilnica/kosare/spominSadjeZelenjava/kumara.svg'
import limona from '@/assets/jedilnica/kosare/spominSadjeZelenjava/limona.svg'
import lubenica from '@/assets/jedilnica/kosare/spominSadjeZelenjava/lubenica.svg'
import malina from '@/assets/jedilnica/kosare/spominSadjeZelenjava/malina.svg'
import marelica from '@/assets/jedilnica/kosare/spominSadjeZelenjava/marelica.svg'
import paprika from '@/assets/jedilnica/kosare/spominSadjeZelenjava/paprika.svg'
import paradiznik from '@/assets/jedilnica/kosare/spominSadjeZelenjava/paradiznik.svg'
import pomaranca from '@/assets/jedilnica/kosare/spominSadjeZelenjava/pomaranca.svg'
import por from '@/assets/jedilnica/kosare/spominSadjeZelenjava/por.svg'
import sliva from '@/assets/jedilnica/kosare/spominSadjeZelenjava/sliva.svg'
import spinaca from '@/assets/jedilnica/kosare/spominSadjeZelenjava/spinaca.svg'

export default {
  ananas,
  banana,
  borovnica,
  breskev,
  bucka,
  cebula,
  cesen,
  cesnja,
  cvetaca,
  fizol,
  grah,
  hruska,
  jabolko,
  jagoda,
  kivi,
  korenje,
  koruza,
  krompir,
  kumara,
  limona,
  lubenica,
  malina,
  marelica,
  paprika,
  paradiznik,
  pomaranca,
  por,
  sliva,
  spinaca
}
