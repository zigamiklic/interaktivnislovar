export default [
  { name: 'jajce', label: 'To je jajce.' },
  { name: 'kvas', label: 'To je kvas.' },
  { name: 'limona', label: 'To je limona.' },
  { name: 'maslo', label: 'To je maslo.' },
  { name: 'mleko', label: 'To je mleko.' },
  { name: 'moka', label: 'To je moka.' },
  { name: 'oreh', label: 'To so orehi.' },
  { name: 'sladkor', label: 'To je sladkor.' }
]
