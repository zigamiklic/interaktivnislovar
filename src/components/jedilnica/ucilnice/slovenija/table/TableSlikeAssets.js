import jajce from '@/assets/jedilnica/ucilnice/slovenija/table/jajce.svg'
import kvas from '@/assets/jedilnica/ucilnice/slovenija/table/kvas.svg'
import limona from '@/assets/jedilnica/ucilnice/slovenija/table/limona.svg'
import maslo from '@/assets/jedilnica/ucilnice/slovenija/table/maslo.svg'
import mleko from '@/assets/jedilnica/ucilnice/slovenija/table/mleko.svg'
import moka from '@/assets/jedilnica/ucilnice/slovenija/table/moka.svg'
import oreh from '@/assets/jedilnica/ucilnice/slovenija/table/oreh.svg'
import sladkor from '@/assets/jedilnica/ucilnice/slovenija/table/sladkor.svg'

export default {
  jajce,
  kvas,
  limona,
  maslo,
  mleko,
  moka,
  oreh,
  sladkor
}
