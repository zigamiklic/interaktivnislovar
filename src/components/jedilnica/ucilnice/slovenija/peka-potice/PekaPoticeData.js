export let targetsData = {
  tableTesto: {
    name: 'tableTesto',
    pos: { x: 599, y: 335 },
    items: ['maslo', 'jajca', 'moka', 'sladkor', 'mleko', 'kvas'],
    itemLocations: [
      { x: 690, y: 641 },
      { x: 889, y: 525 },
      { x: 672, y: 515 },
      { x: 923, y: 634 },
      { x: 776, y: 542 },
      { x: 810, y: 602 }
    ]
  },
  tableNadev: {
    name: 'tableNadev',
    pos: { x: 964, y: 351 },
    items: ['maslo', 'jajca', 'moka', 'sladkor', 'orehi', 'limona'],
    itemLocations: [
      { x: 1058, y: 495 },
      { x: 1085, y: 615 },
      { x: 1272, y: 526 },
      { x: 1192, y: 603 },
      { x: 1290, y: 611 },
      { x: 1167, y: 523 }
    ]
  }
}

export let itemsData = [
  {
    name: 'maslo',
    multiple: true,
    isDuplicateHidden: true,
    pos: { x: 10, y: 649 }
  },
  {
    name: 'jajca',
    multiple: true,
    isDuplicateHidden: true,
    pos: { x: 90, y: 542 }
  },
  {
    name: 'moka',
    multiple: true,
    isDuplicateHidden: true,
    pos: { x: 504, y: 595 }
  },
  {
    name: 'sladkor',
    multiple: true,
    isDuplicateHidden: true,
    pos: { x: 380, y: 485 }
  },
  {
    name: 'mleko',
    pos: { x: 484, y: 396 }
  },
  {
    name: 'kvas',
    pos: { x: 24, y: 510 }
  },
  {
    name: 'orehi',
    pos: { x: 195, y: 541 }
  },
  {
    name: 'limona',
    pos: { x: 170, y: 636 }
  },

  {
    name: 'borovnice',
    pos: { x: 491, y: 516 },
    width: 90,
    height: 61
  },
  {
    name: 'sladoled',
    pos: { x: 396, y: 541 },
    width: 68,
    height: 110
  },
  {
    name: 'korenje',
    pos: { x: 263, y: 533 }
  },
  {
    name: 'cebula',
    pos: { x: 293, y: 628 },
    width: 68,
    height: 71
  },
  {
    name: 'pasteta',
    pos: { x: 399, y: 656 },
    width: 58,
    height: 48
  },
  {
    name: 'sir',
    pos: { x: 13, y: 570 }
  },
  {
    name: 'jogurt',
    pos: { x: 7, y: 415 },
    width: 51,
    height: 79
  }
]

const preText = 'To je'
const preTextPlural = 'To so'

export let inputsData = {
  maslo: { preText, solution: 'maslo' },
  moka: { preText, solution: 'moka' },
  kvas: { preText, solution: 'kvas' },
  mleko: { preText, solution: 'mleko' },
  sladkor: { preText, solution: 'sladkor' },
  jajca: { preText: preTextPlural, solution: 'jajca' },
  limona: { preText, solution: 'limona' },
  orehi: { preText: preTextPlural, solution: 'orehi' }
}

export let linesData = {
  intro: 'Kaj je v potici?',
  testo: 'To je testo.',
  nadev: 'To je nadev.'
}
