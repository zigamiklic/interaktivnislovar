import jajca from '@/assets/jedilnica/ucilnice/slovenija/uvod/jajca.svg'
import kvas from '@/assets/jedilnica/ucilnice/slovenija/uvod/kvas.svg'
import limona from '@/assets/jedilnica/ucilnice/slovenija/uvod/limona.svg'
import maslo from '@/assets/jedilnica/ucilnice/slovenija/uvod/maslo.svg'
import mleko from '@/assets/jedilnica/ucilnice/slovenija/uvod/mleko.svg'
import moka from '@/assets/jedilnica/ucilnice/slovenija/uvod/moka.svg'
import orehi from '@/assets/jedilnica/ucilnice/slovenija/uvod/orehi.svg'
import potica from '@/assets/jedilnica/ucilnice/slovenija/uvod/potica.svg'
import sladkor from '@/assets/jedilnica/ucilnice/slovenija/uvod/sladkor.svg'

import borovnice from '@/assets/shared/borovnice.svg'
import sladoled from '@/assets/shared/sladoled.svg'
import korenje from '@/assets/shared/korenje.svg'
import cebula from '@/assets/shared/cebula.svg'
import pasteta from '@/assets/shared/pasteta.svg'
import sir from '@/assets/shared/sir.svg'
import jogurt from '@/assets/shared/jogurt.svg'

import tableNadev from '@/assets/jedilnica/ucilnice/slovenija/peka-potice/table-nadev.svg'
import tableTesto from '@/assets/jedilnica/ucilnice/slovenija/peka-potice/table-testo.svg'

import soundUvod from '@/assets/sounds/3.2/uvod.mp3'
import soundTesto from '@/assets/sounds/3.2/testo.mp3'
import soundNadev from '@/assets/sounds/3.2/nadev.mp3'

export const sounds = {
  uvod: soundUvod,
  testo: soundTesto,
  nadev: soundNadev
}

export const sprites = {
  jajca,
  kvas,
  limona,
  maslo,
  mleko,
  moka,
  orehi,
  potica,
  sladkor,

  borovnice,
  sladoled,
  korenje,
  cebula,
  pasteta,
  sir,
  jogurt,

  tableNadev,
  tableTesto
}
