import jajca from '@/assets/jedilnica/ucilnice/slovenija/uvod/jajca.svg'
import kvas from '@/assets/jedilnica/ucilnice/slovenija/uvod/kvas.svg'
import limona from '@/assets/jedilnica/ucilnice/slovenija/uvod/limona.svg'
import maslo from '@/assets/jedilnica/ucilnice/slovenija/uvod/maslo.svg'
import mleko from '@/assets/jedilnica/ucilnice/slovenija/uvod/mleko.svg'
import moka from '@/assets/jedilnica/ucilnice/slovenija/uvod/moka.svg'
import orehi from '@/assets/jedilnica/ucilnice/slovenija/uvod/orehi.svg'
import potica from '@/assets/jedilnica/ucilnice/slovenija/uvod/potica.svg'
import sladkor from '@/assets/jedilnica/ucilnice/slovenija/uvod/sladkor.svg'

import ana from '@/assets/sounds/3.0/ana.mp3'
import jakob from '@/assets/sounds/3.0/jakob.mp3'
import soundMoka from '@/assets/sounds/3.0/moka.mp3'
import soundKvas from '@/assets/sounds/3.0/kvas.mp3'
import soundMleko from '@/assets/sounds/3.0/mleko.mp3'
import soundSladkor from '@/assets/sounds/3.0/sladkor.mp3'
import soundMaslo from '@/assets/sounds/3.0/maslo.mp3'
import soundJajca from '@/assets/sounds/3.0/jajca.mp3'
import soundOrehi from '@/assets/sounds/3.0/orehi.mp3'
import soundLimona from '@/assets/sounds/3.0/limona.mp3'

export const sprites = {
  jajca,
  kvas,
  limona,
  maslo,
  mleko,
  moka,
  orehi,
  potica,
  sladkor
}

export const sounds = {
  ana,
  jakob,
  moka: soundMoka,
  kvas: soundKvas,
  mleko: soundMleko,
  sladkor: soundSladkor,
  maslo: soundMaslo,
  jajca: soundJajca,
  orehi: soundOrehi,
  limona: soundLimona
}
