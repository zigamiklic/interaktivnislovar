export default [
  {
    name: 'potica',
    x: 593,
    y: 510
  },

  // Testo
  {
    name: 'moka',
    category: 'testo',
    x: 500,
    y: 577
  },
  {
    name: 'mleko',
    category: 'testo',
    x: 183,
    y: 570
  },
  {
    name: 'maslo',
    category: 'testo',
    x: 333,
    y: 590
  },
  {
    name: 'jajca',
    category: 'testo',
    x: 373,
    y: 510
  },
  {
    name: 'kvas',
    category: 'testo',
    x: 183,
    y: 504
  },
  {
    name: 'sladkor',
    category: 'testo',
    x: 522,
    y: 525
  },

  // Nadev
  {
    name: 'sladkor',
    category: 'nadev',
    x: 1096,
    y: 641
  },
  {
    name: 'moka',
    category: 'nadev',
    x: 1083,
    y: 514
  },
  {
    name: 'limona',
    category: 'nadev',
    x: 998,
    y: 563
  },
  {
    name: 'jajca',
    category: 'nadev',
    x: 830,
    y: 639
  },
  {
    name: 'maslo',
    category: 'nadev',
    x: 848,
    y: 533
  },
  {
    name: 'orehi',
    category: 'nadev',
    x: 893,
    y: 593
  }
]
