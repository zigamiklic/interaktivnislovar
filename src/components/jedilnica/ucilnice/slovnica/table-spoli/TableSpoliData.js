export let soundsData = new Map([
  ['TableJedilnicaSpoli-1', {
    ananas: '/1.1.1/ananas.mp3',
    paradiznik: '/1.1.2/paradiznik.mp3',
    korencek: '/1.1.2/korencek.mp3',
    riz: '/1.1.4/riz.mp3',
    kruh: '/1.1.3/kruh.mp3',
    krompir: '/1.1.2/krompir.mp3',
    sok: '/1.1.4/sok.mp3',
    jogurt: '/1.1.3/jogurt.mp3',
    caj: '/1.1.3/caj.mp3',
    sir: '/1.1.3/sir.mp3',

    banana: '/2.6/banana.mp3',
    hruska: '/2.6/hruska.mp3',
    paprika: '/2.6/paprika.mp3',
    jagoda: '/2.6/jagoda.mp3',
    limona: '/2.6/limona.mp3',
    bucka: '/2.6/bucka.mp3',
    sliva: '/2.6/sliva.mp3',
    cesnja: '/2.6/cesnja.mp3',
    pomaranca: '/2.6/pomaranca.mp3',

    // Srednji spol
    jabolko: '/2.6/jabolko.mp3',
    mleko: '/1.1.3/mleko.mp3'
  }],

  ['TableJedilnicaSpoli-2', {
    // Moški spol
    kakav: '/1.1.3/kakav.mp3',
    med: '/1.1.3/med.mp3',
    sladoled: '/1.1.4/sladoled.mp3',

    // Ženski spol
    marelica: '/2.6/marelica.mp3',
    kumara: '/2.6/kumara.mp3',
    // solata: '',
    pasteta: '/1.1.3/pasteta.mp3',
    zemlja: '/1.1.3/zemlja.mp3',
    makovka: '/1.1.3/makovka.mp3',
    voda: '/1.1.3/voda.mp3',
    salama: '/1.1.3/salama.mp3',
    marmelada: '/1.1.3/marmelada.mp3',
    juha: '/1.1.4/juha.mp3',
    riba: '/1.1.4/riba.mp3',
    pica: '/1.1.4/pica.mp3',
    torta: '/1.1.4/torta.mp3',
    cokolada: '/1.1.4/cokolada.mp3',
    // palacinka: '',
    malina: '/2.6/malina.mp3',

    // Srednji spol
    maslo: '/1.1.3/maslo.mp3',
    meso: '/1.1.4/meso.mp3'
  }],

  ['TableJedilnicaSpoli-3', {
    // Moški spol
    makaroni: '/1.1.4/makaroni.mp3',
    korencki: '/2.6/korencki.mp3',
    kosmici: '/1.1.3/kosmici.mp3',

    // Ženski spol
    borovnice: '/2.6/borovnice.mp3',
    kumare: '/2.6/kumare.mp3',
    torte: '/2.6/torte.mp3',
    zemlje: '/2.6/zemlje.mp3',
    marelice: '/2.6/marelice.mp3',
    makovke: '/2.6/makovke.mp3',
    ribe: '/2.6/ribe.mp3',
    maline: '/2.6/maline.mp3',
    bucke: '/2.6/bucke.mp3'
  }],

  ['TableJedilnicaSpoli-4', {
    // Moški spol
    spageti: '/1.1.4/spageti.mp3',
    ananasi: '/2.6/ananasi.mp3',
    paradizniki: '/2.6/paradizniki.mp3',

    // Ženski spol
    paprike: '/2.6/paprike.mp3',
    jagode: '/2.6/jagode.mp3',
    limone: '/2.6/limone.mp3',
    hruske: '/2.6/hruske.mp3',
    makaroni: '/1.1.4/makaroni.mp3',
    palacinke: '/1.1.4/palacinke.mp3',
    cesnje: '/2.6/cesnje.mp3',
    pomarance: '/2.6/pomarance.mp3',
    banane: '/2.6/banane.mp3',
    slive: '/2.6/slive.mp3',

    // Srednji spol
    jabolka: '/2.6/jabolka.mp3'
  }]
])

export let labelsData = new Map([
  // Ednina
  ['TableJedilnicaSpoli-1', {
    // Moški spol
    ananas: { matchId: 'b1', text: 'ananas' },
    paradiznik: { matchId: 'b1', text: 'paradižnik' },
    korencek: { matchId: 'b1', text: 'korenček' },
    riz: { matchId: 'b1', text: 'riž' },
    kruh: { matchId: 'b1', text: 'kruh' },
    krompir: { matchId: 'b1', text: 'krompir' },
    sok: { matchId: 'b1', text: 'sok' },
    jogurt: { matchId: 'b1', text: 'jogurt' },
    caj: { matchId: 'b1', text: 'čaj' },
    sir: { matchId: 'b1', text: 'sir' },

    // Ženski spol
    banana: { matchId: 'b2', text: 'banana' },
    hruska: { matchId: 'b2', text: 'hruška' },
    paprika: { matchId: 'b2', text: 'paprika' },
    jagoda: { matchId: 'b2', text: 'jagoda' },
    limona: { matchId: 'b2', text: 'limona' },
    bucka: { matchId: 'b2', text: 'bučka' },
    sliva: { matchId: 'b2', text: 'sliva' },
    cesnja: { matchId: 'b2', text: 'češnja' },
    pomaranca: { matchId: 'b2', text: 'pomaranča' },

    // Srednji spol
    jabolko: { matchId: 'b3', text: 'jabolko' },
    mleko: { matchId: 'b3', text: 'mleko' }
  }],

  ['TableJedilnicaSpoli-2', {
    // Moški spol
    kakav: { matchId: 'b1', text: 'kakav' },
    med: { matchId: 'b1', text: 'med' },
    sladoled: { matchId: 'b1', text: 'sladoled' },

    // Ženski spol
    marelica: { matchId: 'b2', text: 'marelica' },
    kumara: { matchId: 'b2', text: 'kumara' },
    solata: { matchId: 'b2', text: 'solata' },
    pasteta: { matchId: 'b2', text: 'pašteta' },
    zemlja: { matchId: 'b2', text: 'žemlja' },
    makovka: { matchId: 'b2', text: 'makovka' },
    voda: { matchId: 'b2', text: 'voda' },
    salama: { matchId: 'b2', text: 'salama' },
    marmelada: { matchId: 'b2', text: 'marmelada' },
    juha: { matchId: 'b2', text: 'juha' },
    riba: { matchId: 'b2', text: 'riba' },
    pica: { matchId: 'b2', text: 'pica' },
    torta: { matchId: 'b2', text: 'torta' },
    cokolada: { matchId: 'b2', text: 'čokolada' },
    palacinka: { matchId: 'b2', text: 'palačinka' },
    malina: { matchId: 'b2', text: 'malina' },

    // Srednji spol
    maslo: { matchId: 'b3', text: 'maslo' },
    meso: { matchId: 'b3', text: 'meso' }
  }],

  // Množina
  ['TableJedilnicaSpoli-3', {
    // Moški spol
    makaroni: { matchId: 'b1', text: 'makaroni' },
    korencki: { matchId: 'b1', text: 'korenčki' },
    kosmici: { matchId: 'b1', text: 'kosmiči' },

    // Ženski spol
    borovnice: { matchId: 'b2', text: 'borovnice' },
    kumare: { matchId: 'b2', text: 'kumare' },
    torte: { matchId: 'b2', text: 'torte' },
    zemlje: { matchId: 'b2', text: 'žemlje' },
    marelice: { matchId: 'b2', text: 'marelice' },
    makovke: { matchId: 'b2', text: 'makovke' },
    ribe: { matchId: 'b2', text: 'ribe' },
    maline: { matchId: 'b2', text: 'maline' },
    bucke: { matchId: 'b2', text: 'bučke' }
  }],

  ['TableJedilnicaSpoli-4', {
    // Moški spol
    spageti: { matchId: 'b1', text: 'špageti' },
    ananasi: { matchId: 'b1', text: 'ananasi' },
    paradizniki: { matchId: 'b1', text: 'paradižniki' },

    // Ženski spol
    paprike: { matchId: 'b2', text: 'paprike' },
    jagode: { matchId: 'b2', text: 'jagode' },
    limone: { matchId: 'b2', text: 'limone' },
    hruske: { matchId: 'b2', text: 'hruške' },
    slive: { matchId: 'b2', text: 'slive' },
    palacinke: { matchId: 'b2', text: 'palačinke' },
    cesnje: { matchId: 'b2', text: 'češnje' },
    pomarance: { matchId: 'b2', text: 'pomaranče' },
    banane: { matchId: 'b2', text: 'banane' },

    // Srednji spol
    jabolka: { matchId: 'b3', text: 'jabolka' }
  }]
])
