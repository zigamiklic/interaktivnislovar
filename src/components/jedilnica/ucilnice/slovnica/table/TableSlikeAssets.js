import onJe from '@/assets/jedilnica/ucilnice/slovnica/table/jaz-jem.svg'
import onaPije from '@/assets/jedilnica/ucilnice/slovnica/table/jaz-pijem.svg'
import onadvaPijeta from '@/assets/jedilnica/ucilnice/slovnica/table/midva-pijeva.svg'
import oniJejo from '@/assets/jedilnica/ucilnice/slovnica/table/mi-jemo.svg'
import oniPijejo from '@/assets/jedilnica/ucilnice/slovnica/table/mi-pijemo.svg'
import onadvaJesta from '@/assets/jedilnica/ucilnice/slovnica/table/onadva-jesta.svg'
import onaJe from '@/assets/jedilnica/ucilnice/slovnica/table/ona-je.svg'
import oniKuhajo from '@/assets/jedilnica/ucilnice/slovnica/table/oni-kuhajo.svg'
import onKuha from '@/assets/jedilnica/ucilnice/slovnica/table/on-kuha.svg'
import onaKuha from '@/assets/jedilnica/ucilnice/slovnica/table/ti-kuhas.svg'
import onPije from '@/assets/jedilnica/ucilnice/slovnica/table/ti-pijes.svg'
import onadvaKuhata from '@/assets/jedilnica/ucilnice/slovnica/table/vidva-kuhata.svg'

export default {
  onJe,
  onaPije,
  onadvaPijeta,
  oniJejo,
  oniPijejo,
  onadvaJesta,
  onaJe,
  oniKuhajo,
  onKuha,
  onaKuha,
  onPije,
  onadvaKuhata
}
