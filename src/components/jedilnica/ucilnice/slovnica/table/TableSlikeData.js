export default [
  { name: 'onJe', label: 'On je.' },
  { name: 'onaPije', label: 'Ona pije.' },
  { name: 'onadvaPijeta', label: 'Onadva pijeta.' },
  { name: 'oniJejo', label: 'Oni jejo.' },
  { name: 'oniPijejo', label: 'Oni pijejo.' },
  { name: 'onadvaJesta', label: 'Onadva jesta.' },
  { name: 'onaJe', label: 'Ona je.' },
  { name: 'oniKuhajo', label: 'Oni kuhajo.' },
  { name: 'onKuha', label: 'On kuha.' },
  { name: 'onaKuha', label: 'Ona kuha.' },
  { name: 'onPije', label: 'On pije.' },
  { name: 'onadvaKuhata', label: 'Onadva kuhata.' }
]
