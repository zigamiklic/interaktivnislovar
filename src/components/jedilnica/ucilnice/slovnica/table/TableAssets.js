import jazJem from '@/assets/jedilnica/ucilnice/slovnica/table/jaz-jem.svg'
import jazPijem from '@/assets/jedilnica/ucilnice/slovnica/table/jaz-pijem.svg'
import midvaPijeva from '@/assets/jedilnica/ucilnice/slovnica/table/midva-pijeva.svg'
import miJemo from '@/assets/jedilnica/ucilnice/slovnica/table/mi-jemo.svg'
import miPijemo from '@/assets/jedilnica/ucilnice/slovnica/table/mi-pijemo.svg'
import onadvaJesta from '@/assets/jedilnica/ucilnice/slovnica/table/onadva-jesta.svg'
import onaJe from '@/assets/jedilnica/ucilnice/slovnica/table/ona-je.svg'
import oniKuhajo from '@/assets/jedilnica/ucilnice/slovnica/table/oni-kuhajo.svg'
import onKuha from '@/assets/jedilnica/ucilnice/slovnica/table/on-kuha.svg'
import tiKuhas from '@/assets/jedilnica/ucilnice/slovnica/table/ti-kuhas.svg'
import tiPijes from '@/assets/jedilnica/ucilnice/slovnica/table/ti-pijes.svg'
import vidvaKuhata from '@/assets/jedilnica/ucilnice/slovnica/table/vidva-kuhata.svg'
import viJeste from '@/assets/jedilnica/ucilnice/slovnica/table/vi-jeste.svg'

export default {
  jazJem,
  jazPijem,
  midvaPijeva,
  miJemo,
  miPijemo,
  onadvaJesta,
  onaJe,
  oniKuhajo,
  onKuha,
  tiKuhas,
  tiPijes,
  vidvaKuhata,
  viJeste
}
