let m1 = 16 // margin between items
let h1 = 48 // height of item
let cl1 = 270 // left column x
let cr1 = 540 // right column x
let ct1 = 102 // columns top/y

export default {
  jazJem: {
    label: 'jem',
    person: 'jaz',
    position: { x: cl1, y: ct1 }
  },
  miPijemo: {
    label: 'pijemo',
    person: 'mi',
    position: { x: cl1, y: ct1 + (h1 + m1) }
  },
  onKuha: {
    label: 'kuha',
    person: 'on',
    position: { x: cl1, y: ct1 + (h1 + m1) * 2 }
  },
  vidvaKuhata: {
    label: 'kuhata',
    person: 'vidva',
    position: { x: cl1, y: ct1 + (h1 + m1) * 3 }
  },
  tiPijes: {
    label: 'piješ',
    person: 'ti',
    position: { x: cl1, y: ct1 + (h1 + m1) * 4 }
  },
  onadvaJesta: {
    label: 'jesta',
    person: 'onadva',
    position: { x: cl1, y: ct1 + (h1 + m1) * 5 }
  },
  oniKuhajo: {
    label: 'kuhajo',
    person: 'oni',
    position: { x: cl1, y: ct1 + (h1 + m1) * 6 }
  },
  onaJe: {
    label: 'je',
    person: 'ona',
    position: { x: cr1, y: ct1 }
  },
  midvaPijeva: {
    label: 'pijeva',
    person: 'midva',
    position: { x: cr1, y: ct1 + (h1 + m1) }
  },
  viJeste: {
    label: 'jeste',
    person: 'vi',
    position: { x: cr1, y: ct1 + (h1 + m1) * 2 }
  },
  jazPijem: {
    label: 'pijem',
    person: 'jaz',
    position: { x: cr1, y: ct1 + (h1 + m1) * 3 }
  },
  tiKuhas: {
    label: 'kuhaš',
    person: 'ti',
    position: { x: cr1, y: ct1 + (h1 + m1) * 4 }
  },
  miJemo: {
    label: 'jemo',
    person: 'mi',
    position: { x: cr1, y: ct1 + (h1 + m1) * 5 }
  }
}
