let m1 = 16
let h1 = 48
let cl1 = 340
let cr1 = 770
let ct1 = 120

export default {
  jaz: { glyph: 'jazJem', solution: 'jem', text: 'jaz', pos: { x: cl1, y: ct1 } },
  mi: { glyph: 'miPijemo', solution: 'pijemo', text: 'mi', pos: { x: cl1, y: ct1 + (h1 + m1) } },
  on: { glyph: 'onKuha', solution: 'kuha', text: 'on', pos: { x: cl1, y: ct1 + (h1 + m1) * 2 } },
  vidva: { glyph: 'vidvaKuhata', solution: 'kuhata', text: 'vidva', pos: { x: cl1, y: ct1 + (h1 + m1) * 3 } },
  ti: { glyph: 'tiPijes', solution: 'piješ', text: 'ti', pos: { x: cl1, y: ct1 + (h1 + m1) * 4 } },
  onadva: { glyph: 'onadvaJesta', solution: 'jesta', text: 'onadva', pos: { x: cl1, y: ct1 + (h1 + m1) * 5 } },
  oni: { glyph: 'oniKuhajo', solution: 'kuhajo', text: 'oni', pos: { x: cl1, y: ct1 + (h1 + m1) * 6 } },

  ona: { glyph: 'onaJe', solution: 'je', text: 'ona', pos: { x: cr1, y: ct1 } },
  midva: { glyph: 'midvaPijeva', solution: 'pijeva', text: 'midva', pos: { x: cr1, y: ct1 + (h1 + m1) } },
  vi: { glyph: 'viJeste', solution: 'jeste', text: 'vi', pos: { x: cr1, y: ct1 + (h1 + m1) * 2 } },
  jaz2: { glyph: 'jazPijem', solution: 'pijem', text: 'jaz', pos: { x: cr1, y: ct1 + (h1 + m1) * 3 } },
  ti2: { glyph: 'tiKuhas', solution: 'kuhaš', text: 'ti', pos: { x: cr1, y: ct1 + (h1 + m1) * 4 } },
  mi2: { glyph: 'miJemo', solution: 'jemo', text: 'mi', pos: { x: cr1, y: ct1 + (h1 + m1) * 5 } }
}
