import ananas from '@/assets/sounds/2.6/ananas.mp3'
import paradiznik from '@/assets/sounds/2.6/paradiznik.mp3'
import caj from '@/assets/sounds/1.1.3/caj.mp3'
import kruh from '@/assets/sounds/1.1.3/kruh.mp3'
import med from '@/assets/sounds/1.1.3/med.mp3'
import sir from '@/assets/sounds/1.1.3/sir.mp3'
import jogurt from '@/assets/sounds/1.1.3/jogurt.mp3'
import krompir from '@/assets/sounds/2.6/krompir.mp3'
import riz from '@/assets/sounds/1.1.4/riz.mp3'
import sladoled from '@/assets/sounds/1.1.4/sladoled.mp3'
import makaroni from '@/assets/sounds/1.1.4/makaroni.mp3'
import spageti from '@/assets/sounds/1.1.4/spageti.mp3'
import kosmici from '@/assets/sounds/1.1.3/kosmici.mp3'
import banana from '@/assets/sounds/2.6/banana.mp3'
import pomaranca from '@/assets/sounds/2.6/pomaranca.mp3'
import marelica from '@/assets/sounds/2.6/marelica.mp3'
import paprika from '@/assets/sounds/2.6/paprika.mp3'
import kumara from '@/assets/sounds/2.6/kumara.mp3'
import salama from '@/assets/sounds/1.1.3/salama.mp3'
import marmelada from '@/assets/sounds/1.1.3/marmelada.mp3'
import pasteta from '@/assets/sounds/1.1.3/pasteta.mp3'
import solata from '@/assets/sounds/1.1.4/solata.mp3'
import juha from '@/assets/sounds/1.1.4/juha.mp3'
import riba from '@/assets/sounds/1.1.4/riba.mp3'
import pica from '@/assets/sounds/1.1.4/pica.mp3'
import torta from '@/assets/sounds/1.1.4/torta.mp3'
import mleko from '@/assets/sounds/1.1.4/mleko.mp3'
import cokolada from '@/assets/sounds/1.1.4/cokolada.mp3'
import borovnice from '@/assets/sounds/2.6/borovnice.mp3'
import cesnje from '@/assets/sounds/2.6/cesnje.mp3'
import maline from '@/assets/sounds/2.6/maline.mp3'
import palacinke from '@/assets/sounds/1.1.4/palacinke.mp3'
import jabolko from '@/assets/sounds/2.6/jabolko.mp3'
import maslo from '@/assets/sounds/1.1.4/maslo.mp3'
import meso from '@/assets/sounds/1.1.4/meso.mp3'
import sok from '@/assets/sounds/1.1.4/sok.mp3'
import jabolka from '@/assets/sounds/2.6/jabolka.mp3'
import hruska from '@/assets/sounds/2.6/hruska.mp3'
import voda from '@/assets/sounds/1.1.3/voda.mp3'

export default {
  banana,
  ananas,
  borovnice,
  paradiznik,
  krompir,
  paprika,
  maline,
  cesnje,
  jogurt,
  caj,
  kumara,
  mleko,
  riba,
  pasteta,
  maslo,
  kruh,
  med,
  sir,
  kosmici,
  salama,
  pomaranca,
  jabolko,
  jabolka,
  marmelada,
  marelica,
  riz,
  sladoled,
  makaroni,
  spageti,
  solata,
  juha,
  pica,
  torta,
  cokolada,
  palacinke,
  sok,
  meso,
  hruska,
  voda
}
