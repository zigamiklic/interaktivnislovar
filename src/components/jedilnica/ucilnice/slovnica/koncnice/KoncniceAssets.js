import ananas from '@/assets/shared/ananas.svg'
import jemAnanas from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-ananas.svg'
import paradiznik from '@/assets/shared/paradiznik.svg'
import jemParadiznik from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-paradiznik.svg'
import kruh from '@/assets/shared/kruh.svg'
import jemKruh from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-kruh.svg'
import caj from '@/assets/shared/caj.svg'
import pijemCaj from '@/assets/jedilnica/ucilnice/slovnica/koncnice/pijem-caj.svg'
import kuhamCaj from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-caj.svg'
import sir from '@/assets/shared/sir.svg'
import jemSir from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-sir.svg'
import med from '@/assets/shared/med.svg'
import jemMed from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-med.svg'
import jogurt from '@/assets/shared/jogurt.svg'
import jemJogurt from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-jogurt.svg'
import krompir from '@/assets/shared/krompir.svg'
import jemKrompir from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-krompir.svg'
import kuhamKrompir from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-krompir.svg'
import riz from '@/assets/shared/riz.svg'
import jemRiz from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-riz.svg'
import kuhamRiz from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-riz.svg'
import sladoled from '@/assets/shared/sladoled.svg'
import jemSladoled from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-sladoled.svg'

import makaroni from '@/assets/shared/makaroni.svg'
import jemMakarone from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-makarone.svg'
import kuhamMakarone from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-makarone.svg'
import spageti from '@/assets/shared/spageti.svg'
import jemSpagete from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-spagete.svg'
import kuhamSpagete from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-spagete.svg'

import banana from '@/assets/shared/banana.svg'
import jemBanano from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-banano.svg'
import hruska from '@/assets/shared/hruska.svg'
import jemHrusko from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-hrusko.svg'
import pomaranca from '@/assets/shared/pomaranca.svg'
import jemPomaranco from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-pomaranco.svg'
import marelica from '@/assets/shared/marelica.svg'
import jemMarelico from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-marelico.svg'
import paprika from '@/assets/shared/paprika.svg'
import jemPapriko from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-papriko.svg'
import kumara from '@/assets/shared/kumara.svg'
import jemKumaro from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-kumaro.svg'
import voda from '@/assets/shared/voda.svg'
import pijemVodo from '@/assets/jedilnica/ucilnice/slovnica/koncnice/pijem-vodo.svg'
import salama from '@/assets/shared/salama.svg'
import jemSalamo from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-salamo.svg'
import marmelada from '@/assets/shared/marmelada.svg'
import jemMarmelado from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-marmelado.svg'
import pasteta from '@/assets/shared/pasteta.svg'
import jemPasteto from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-pasteto.svg'
import solata from '@/assets/shared/solata.svg'
import jemSolato from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-solato.svg'
import juha from '@/assets/shared/juha.svg'
import jemJuho from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-juho.svg'
import kuhamJuho from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-juho.svg'
import riba from '@/assets/shared/riba.svg'
import jemRibo from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-ribo.svg'
import pica from '@/assets/shared/pica.svg'
import jemPico from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-pico.svg'
import torta from '@/assets/shared/torta.svg'
import jemTorto from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-torto.svg'
import cokolada from '@/assets/shared/cokolada.svg'
import jemCokolada from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-cokolada.svg'

import borovnice from '@/assets/shared/borovnice.svg'
import jemBorovnice from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-borovnice.svg'
import cesnje from '@/assets/shared/cesnje.svg'
import jemCesnje from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-cesnje.svg'
import maline from '@/assets/shared/maline.svg'
import jemMaline from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-maline.svg'
import palacinke from '@/assets/shared/palacinke.svg'
import jemPalacinke from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-palacinke.svg'

import jabolko from '@/assets/shared/jabolko.svg'
import jemJabolko from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-jabolko.svg'
import mleko from '@/assets/shared/mleko.svg'
import pijemMleko from '@/assets/jedilnica/ucilnice/slovnica/koncnice/pijem-mleko.svg'
import maslo from '@/assets/shared/maslo.svg'
import jemMaslo from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-maslo.svg'
import meso from '@/assets/shared/meso.svg'
import jemMeso from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-meso.svg'
import kuhamMeso from '@/assets/jedilnica/ucilnice/slovnica/koncnice/kuham-meso.svg'

import jabolka from '@/assets/shared/jabolka.svg'
import jemJabolka from '@/assets/jedilnica/ucilnice/slovnica/koncnice/jem-jabolka.svg'

export const assets = {
  ananas: {
    default: ananas,
    eat: jemAnanas
  },
  paradiznik: {
    default: paradiznik,
    eat: jemParadiznik
  },
  kruh: {
    default: kruh,
    eat: jemKruh
  },
  caj: {
    default: caj,
    drink: pijemCaj,
    cook: kuhamCaj
  },
  sir: {
    default: sir,
    eat: jemSir
  },
  med: {
    default: med,
    eat: jemMed
  },
  jogurt: {
    default: jogurt,
    eat: jemJogurt
  },
  krompir: {
    default: krompir,
    eat: jemKrompir,
    cook: kuhamKrompir
  },
  riz: {
    default: riz,
    eat: jemRiz,
    cook: kuhamRiz
  },
  sladoled: {
    default: sladoled,
    eat: jemSladoled
  },

  makaroni: {
    default: makaroni,
    eat: jemMakarone,
    cook: kuhamMakarone
  },
  spageti: {
    default: spageti,
    eat: jemSpagete,
    cook: kuhamSpagete
  },

  banana: {
    default: banana,
    eat: jemBanano
  },
  hruska: {
    default: hruska,
    eat: jemHrusko
  },
  pomaranca: {
    default: pomaranca,
    eat: jemPomaranco
  },
  marelica: {
    default: marelica,
    eat: jemMarelico
  },
  paprika: {
    default: paprika,
    eat: jemPapriko
  },
  kumara: {
    default: kumara,
    eat: jemKumaro
  },
  voda: {
    default: voda,
    drink: pijemVodo
  },
  salama: {
    default: salama,
    eat: jemSalamo
  },
  marmelada: {
    default: marmelada,
    eat: jemMarmelado
  },
  pasteta: {
    default: pasteta,
    eat: jemPasteto
  },
  solata: {
    default: solata,
    eat: jemSolato
  },
  juha: {
    default: juha,
    eat: jemJuho,
    cook: kuhamJuho
  },
  riba: {
    default: riba,
    eat: jemRibo
  },
  pica: {
    default: pica,
    eat: jemPico
  },
  torta: {
    default: torta,
    eat: jemTorto
  },
  cokolada: {
    default: cokolada,
    eat: jemCokolada
  },

  borovnice: {
    default: borovnice,
    eat: jemBorovnice
  },
  cesnje: {
    default: cesnje,
    eat: jemCesnje
  },
  maline: {
    default: maline,
    eat: jemMaline
  },
  palacinke: {
    default: palacinke,
    eat: jemPalacinke
  },

  jabolko: {
    default: jabolko,
    eat: jemJabolko
  },
  mleko: {
    default: mleko,
    drink: pijemMleko
  },
  maslo: {
    default: maslo,
    eat: jemMaslo
  },
  meso: {
    default: meso,
    eat: jemMeso,
    cook: kuhamMeso
  },

  jabolka: {
    default: jabolka,
    eat: jemJabolka
  }
}
