const types = {
  drink: {
    label: 'pije',
    question: 'Kaj pije?'
  },
  eat: {
    label: 'je',
    question: 'Kaj je?'
  },
  cook: {
    label: 'kuha',
    question: 'Kaj kuha?'
  },
  is: {
    label: 'to je',
    question: 'Kaj je to?'
  },
  are: {
    label: 'to so',
    question: 'Kaj so to?'
  }
}

const manSingular = [
  {
    name: 'ananas',
    solution: 'ananas',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'paradiznik',
    solution: 'paradižnik',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'kruh',
    solution: 'kruh',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'caj',
    solution: 'čaj',
    type: 'drink',
    quantity: 'singular'
  },
  {
    name: 'caj',
    solution: 'čaj',
    type: 'cook',
    quantity: 'singular'
  },
  {
    name: 'sir',
    solution: 'sir',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'med',
    solution: 'med',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'jogurt',
    solution: 'jogurt',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'krompir',
    solution: 'krompir',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'krompir',
    solution: 'krompir',
    type: 'cook',
    quantity: 'singular'
  },
  {
    name: 'riz',
    solution: 'riž',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'riz',
    solution: 'riž',
    type: 'cook',
    quantity: 'singular'
  },
  {
    name: 'sladoled',
    solution: 'sladoled',
    type: 'eat',
    quantity: 'singular'
  }
]

const manPlural = [
  {
    name: 'makaroni',
    solution: 'makarone',
    text: 'makaron<span>i</span>',
    type: 'eat',
    quantity: 'plural'
  },
  {
    name: 'makaroni',
    solution: 'makarone',
    text: 'makaron<span>i</span>',
    type: 'cook',
    quantity: 'plural'
  },
  {
    name: 'spageti',
    solution: 'špagete',
    text: 'špaget<span>i</span>',
    type: 'eat',
    quantity: 'plural'
  },
  {
    name: 'spageti',
    solution: 'špagete',
    text: 'špaget<span>i</span>',
    type: 'cook',
    quantity: 'plural'
  }
]

const womanSingular = [
  {
    name: 'cokolada',
    solution: 'čokolado',
    text: 'čokolada',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'torta',
    solution: 'torto',
    text: 'torta',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'juha',
    solution: 'juho',
    text: 'juha',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'solata',
    solution: 'zeleno solato',
    text: 'zelena solata',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'pica',
    solution: 'pico',
    text: 'pica',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'pomaranca',
    solution: 'pomarančo',
    text: 'pomaranča',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'banana',
    solution: 'banano',
    text: 'banana',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'hruska',
    solution: 'hruško',
    text: 'hruška',
    type: 'eat',
    quantity: 'singular'
  },
  {
    name: 'voda',
    solution: 'vodo',
    text: 'voda',
    type: 'drink',
    quantity: 'singular'
  }
]

const womanPlural = [
  {
    name: 'borovnice',
    solution: 'borovnice',
    type: 'eat',
    quantity: 'plural'
  }
]

const neutralSingular = [
  {
    name: 'meso',
    solution: 'meso',
    type: 'eat',
    quantity: 'singular'
  }
]

const words = [
  ...manSingular,
  ...manPlural,
  ...womanSingular,
  ...womanPlural,
  ...neutralSingular
]

export default {
  types,
  words
}
