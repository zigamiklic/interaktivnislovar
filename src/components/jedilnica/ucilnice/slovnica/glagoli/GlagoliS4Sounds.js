import me from '@/assets/sounds/2.2/s4/me.mp3'
import you from '@/assets/sounds/2.2/s4/you.mp3'
import they from '@/assets/sounds/2.2/s4/they.mp3'

export default {
  me,
  you,
  they
}
