import meEat from '@/assets/sounds/2.2/s1/meEat.mp3'
import heEat from '@/assets/sounds/2.2/s1/heEat.mp3'
import sheEat from '@/assets/sounds/2.2/s1/sheEat.mp3'
import youEat from '@/assets/sounds/2.2/s1/youEat.mp3'

import meDrink from '@/assets/sounds/2.2/s1/meDrink.mp3'
import heDrink from '@/assets/sounds/2.2/s1/heDrink.mp3'
import sheDrink from '@/assets/sounds/2.2/s1/sheDrink.mp3'
import youDrink from '@/assets/sounds/2.2/s1/youDrink.mp3'

export default {
  eat: {
    me: meEat,
    you: youEat,
    he: heEat,
    she: sheEat
  },
  drink: {
    me: meDrink,
    you: youDrink,
    he: heDrink,
    she: sheDrink
  }
}
