import me from '@/assets/sounds/2.2/s2/me.mp3'
import you from '@/assets/sounds/2.2/s2/you.mp3'
import he from '@/assets/sounds/2.2/s2/he.mp3'
import she from '@/assets/sounds/2.2/s2/she.mp3'

export default {
  me,
  you,
  he,
  she
}
