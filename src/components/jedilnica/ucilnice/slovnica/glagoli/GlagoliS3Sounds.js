import meEat from '@/assets/sounds/2.2/s3/meEat.mp3'
import theyEat from '@/assets/sounds/2.2/s3/theyEat.mp3'
import youEat from '@/assets/sounds/2.2/s3/youEat.mp3'

import meDrink from '@/assets/sounds/2.2/s3/meDrink.mp3'
import theyDrink from '@/assets/sounds/2.2/s3/theyDrink.mp3'
import youDrink from '@/assets/sounds/2.2/s3/youDrink.mp3'

export default {
  eat: {
    me: meEat,
    you: youEat,
    they: theyEat
  },
  drink: {
    me: meDrink,
    you: youDrink,
    they: theyDrink
  }
}
