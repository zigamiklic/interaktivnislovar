export let linesData = new Map([
  ['GlagoliJedilnica-1', {
    eat: {
      me: 'Jaz je<span style="color: red;">m</span>.',
      you: 'Ti je<span style="color: red;">š</span>.',
      he: 'On je.',
      she: 'Ona je.'
    },
    drink: {
      me: 'Jaz pije<span style="color: red;">m</span>.',
      you: 'Ti pije<span style="color: red;">š</span>.',
      he: 'On pije.',
      she: 'Ona pije.'
    }
  }],
  ['GlagoliJedilnica-2', {
    cook: {
      me: 'Jaz kuha<span style="color: red;">m</span>.',
      you: 'Ti kuha<span style="color: red;">š</span>.',
      he: 'On kuha.',
      she: 'Ona kuha.'
    }
  }],
  ['GlagoliJedilnica-3', {
    eat: {
      me: 'Midva je<span style="color: red;">va</span>.',
      you: 'Vidva je<span style="color: red;">sta</span>.',
      they: 'Onadva je<span style="color: red;">sta</span>.'
    },
    drink: {
      me: 'Midva pije<span style="color: red;">va</span>.',
      you: 'Vidva pije<span style="color: red;">ta</span>.',
      they: 'Onadva pije<span style="color: red;">ta</span>.'
    }
  }],
  ['GlagoliJedilnica-4', {
    cook: {
      me: 'Midva kuha<span style="color: red;">va</span>.',
      you: 'Vidva kuha<span style="color: red;">ta</span>.',
      they: 'Onadva kuha<span style="color: red;">ta</span>.'
    }
  }],
  ['GlagoliJedilnica-5', {
    eat: {
      me: 'Mi je<span style="color: red;">mo</span>.',
      you: 'Vi je<span style="color: red;">ste</span>.',
      they: 'Oni je<span style="color: red;">jo</span>.'
    },
    drink: {
      me: 'Mi pije<span style="color: red;">mo</span>.',
      you: 'Vi pije<span style="color: red;">te</span>.',
      they: 'Oni pije<span style="color: red;">jo</span>.'
    }
  }],
  ['GlagoliJedilnica-6', {
    cook: {
      me: 'Mi kuha<span style="color: red;">mo</span>.',
      you: 'Vi kuha<span style="color: red;">te</span>.',
      they: 'Oni kuha<span style="color: red;">jo</span>.'
    }
  }]
])
