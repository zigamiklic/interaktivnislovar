import me from '@/assets/sounds/2.2/s6/me.mp3'
import you from '@/assets/sounds/2.2/s6/you.mp3'
import they from '@/assets/sounds/2.2/s6/they.mp3'

export default {
  me,
  you,
  they
}
