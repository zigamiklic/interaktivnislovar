import meEat from '@/assets/sounds/2.2/s5/meEat.mp3'
import theyEat from '@/assets/sounds/2.2/s5/theyEat.mp3'
import youEat from '@/assets/sounds/2.2/s5/youEat.mp3'

import meDrink from '@/assets/sounds/2.2/s5/meDrink.mp3'
import theyDrink from '@/assets/sounds/2.2/s5/theyDrink.mp3'
import youDrink from '@/assets/sounds/2.2/s5/youDrink.mp3'

export default {
  eat: {
    me: meEat,
    you: youEat,
    they: theyEat
  },
  drink: {
    me: meDrink,
    you: youDrink,
    they: theyDrink
  }
}
