import kroznik from '@/assets/sounds/1.2.1/kroznik.mp3'
import kozarec from '@/assets/sounds/1.2.1/kozarec.mp3'
import skodelica from '@/assets/sounds/1.2.1/skodelica.mp3'
import zlica from '@/assets/sounds/1.2.1/zlica.mp3'
import noz from '@/assets/sounds/1.2.1/noz.mp3'
import vilice from '@/assets/sounds/1.2.1/vilice.mp3'
import pladenj from '@/assets/sounds/1.2.1/pladenj.mp3'
import prticek from '@/assets/sounds/1.2.1/prticek.mp3'

export default {
  kroznik,
  kozarec,
  skodelica,
  zlica,
  noz,
  vilice,
  pladenj,
  prticek
}
