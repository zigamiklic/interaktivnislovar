import ana1 from '@/assets/sounds/1.2/ana1.mp3'
import ana2 from '@/assets/sounds/1.2/ana2.mp3'
import ana3 from '@/assets/sounds/1.2/ana3.mp3'
import ana4 from '@/assets/sounds/1.2/ana4.mp3'
import kuhar from '@/assets/sounds/1.2/kuhar.mp3'

export default {
  ana1,
  ana2,
  ana3,
  ana4,
  kuhar
}
