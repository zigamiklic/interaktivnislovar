import Vue from 'vue'
import Vuex from 'vuex'
import SceneRoutes from './modules/sceneRoutes'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

// Set all values from true to false if locking will be readed in the future

// const progress1 = JSON.parse(localStorage.getItem('slovarProgress1'))
// const progress2 = JSON.parse(localStorage.getItem('slovarProgress2'))
// const progress3 = JSON.parse(localStorage.getItem('slovarProgress3'))

const progress1 = {}
const progress2 = {}
const progress3 = {}

const gamesCompleted1 = Object.assign({
  MalicaPovezovanje: true,
  KosiloPovezovanje: true,
  SadjePovezovanje: true,
  ZelenjavaPovezovanje: true,
  JedilniPripomockiPovezovanje: true,
  JedilniPripomockiVpisovanje: true
}, progress1)

const gamesCompleted2 = Object.assign({
  KosareIzbiranje: true,
  SadnaKupaSestavi: true,
  SolataSestavi: true,
  PomesaneCrke: true,
  MalicaSestavi: true,
  KosiloSestavi: true,
  MesanaHrana: true,
  SpominMalicaKosilo: true,
  SpominSadjeZelenjava: true
}, progress2)

const gamesCompleted3 = Object.assign({
  'TableJedilnicaSpoli-1': true,
  'TableJedilnicaSpoli-2': true,
  'TableJedilnicaSpoli-3': true,
  'TableJedilnicaSpoli-4': true,
  TableJedilnicaPovezovanje: true,
  TableJedilnicaVpisovanje: true,
  TableJedilnicaSlike: true,
  KoncniceJedilnica: true
}, progress3)

function checkComplete (games) {
  return Object.values(games).find(item => item === false) === undefined
}

export default new Vuex.Store({
  state: {
    screenPosition: {x: 0, y: 0},
    screenScale: 1,
    screenSize: {width: 1334, height: 750},

    navBackText: '',
    navForwardText: '',
    showNavBack: false,
    showNavForward: false,
    pulseNavBack: false,
    pulseNavForward: false,

    viewId: 0,
    gamesCompleted1,
    gamesCompleted2,
    gamesCompleted3
  },
  getters: {
    ucilnica1Unlocked: state => checkComplete(state.gamesCompleted1),
    ucilnica2Unlocked: state => checkComplete(state.gamesCompleted2),
    ucilnica3Unlocked: state => checkComplete(state.gamesCompleted3)
  },
  mutations: {
    updateScreenPosition (state, position) {
      state.screenPosition = position
    },
    updateScreenScale (state, scale) {
      state.screenScale = scale
    },
    updateScreenSize (state, size) {
      state.screenSize = size
    },

    setNavBackText (state, name) {
      state.navBackText = name
    },
    setNavForwardText (state, name) {
      state.navForwardText = name
    },
    setShowNavBack (state, show) {
      state.showNavBack = show
    },
    setShowNavForward (state, show) {
      state.showNavForward = show
    },
    setPulseNavBack (state, pulse) {
      state.pulseNavBack = pulse
    },
    setPulseNavForward (state, pulse) {
      state.pulseNavForward = pulse
    },

    setViewId (state) {
      state.viewId += 1
    },
    setMalicaCompleted (state) {
      state.gamesCompleted1['MalicaPovezovanje'] = true
    },
    setKosiloCompleted (state) {
      state.gamesCompleted1['KosiloPovezovanje'] = true
    },
    setSadjeCompleted (state) {
      state.gamesCompleted1['SadjePovezovanje'] = true
    },
    setZelenjavaCompleted (state) {
      state.gamesCompleted1['ZelenjavaPovezovanje'] = true
    },
    setPripomockiCompleted (state) {
      state.gamesCompleted1['JedilniPripomockiPovezovanje'] = true
    },
    setPripomockiVpisovanjeCompleted (state) {
      state.gamesCompleted1['JedilniPripomockiVpisovanje'] = true
    }
  },
  actions: {
    updateScreenPosition ({commit}, position) {
      commit('updateScreenPosition', position)
    },
    updateScreenScale ({commit}, scale) {
      commit('updateScreenScale', scale)
    },
    updateScreenSize ({commit}, size) {
      commit('updateScreenSize', size)
    },

    setNavBackText ({commit}, name) {
      commit('setNavBackText', name)
    },
    setNavForwardText ({commit}, name) {
      commit('setNavForwardText', name)
    },
    setShowNavBack ({commit}, show) {
      commit('setShowNavBack', show)
    },
    setShowNavForward ({commit}, show) {
      commit('setShowNavForward', show)
    },
    setPulseNavForward ({commit}, pulse) {
      commit('setPulseNavForward', pulse)
    },
    setPulseNavBack ({commit}, pulse) {
      commit('setPulseNavBack', pulse)
    },

    reloadView ({ commit }) {
      commit('setViewId')
    },
    gameCompleted ({ state, commit }, game) {
      switch (game) {
        case 'malica': commit('setMalicaCompleted'); break
        case 'kosilo': commit('setKosiloCompleted'); break
        case 'sadje': commit('setSadjeCompleted'); break
        case 'zelenjava': commit('setZelenjavaCompleted'); break
        case 'pripomocki': commit('setPripomockiCompleted'); break
        case 'pripomockiVpisovanje': commit('setPripomockiVpisovanjeCompleted'); break
      }

      localStorage.setItem('slovarProgress1', JSON.stringify(state.gamesCompleted1))
      localStorage.setItem('slovarProgress2', JSON.stringify(state.gamesCompleted2))
      localStorage.setItem('slovarProgress3', JSON.stringify(state.gamesCompleted3))
    }
  },
  modules: {
    sceneRoutes: SceneRoutes
  },
  strict: debug
})
