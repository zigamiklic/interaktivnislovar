const state = {
  // Determines which end dialog pops up
  // 0 - no dialog; 1 - next game screen dialog; 2 - next game dialog; 3 - end of section dialog
  endDialogs: new Map([
    ['KajJeTo-1', 1],
    ['KajJeTo-2', 2],
    ['Potrebscine-2', 1],
    ['Potrebscine-3', 2],
    ['KajJeToVpisovanje', 3],
    ['BesedeKajDelajo', 2],
    ['BesedeKajDelajoVpisovanje', 3],

    ['TablePovezovanje-2', 2],
    ['TablePovezovanje', 2],
    ['TableVpisovanje', 2],
    ['TableSpoli-1', 1],
    ['TableSpoli-2', 2],

    ['SlovenciUvod', 0],
    ['KdoJeTo', 2],
    ['KajDelajoVpisovanje', 3],

    ['KosiloPovezovanje', 2],
    ['MalicaPovezovanje', 2],
    ['SadjePovezovanjeEdnina', 2],
    ['SadjePovezovanjeMnozina', 2],
    ['ZelenjavaPovezovanje', 2],

    ['JedilniPripomockiPovezovanje', 2],
    ['JedilniPripomockiVpisovanje', 2],

    ['KosareIzbiranje', 2],
    ['SadnaKupaSestavi', 2],
    ['SolataSestavi', 2],
    ['PomesaneCrke', 2],
    ['MalicaSestavi', 2],
    ['KosiloSestavi', 2],
    ['MesanaHrana', 2],
    ['SpominMalicaKosilo', 2],
    ['SpominSadjeZelenjava', 2],

    ['TableJedilnicaSpoli-1', 1],
    ['TableJedilnicaSpoli-2', 2],
    ['TableJedilnicaSpoli-3', 2],
    ['TableJedilnicaSpoli-4', 2],

    ['TableJedilnicaSlike', 2],
    ['TableJedilnicaPovezovanje', 2],
    ['TableJedilnicaVpisovanje', 2],

    ['KoncniceJedilnica', 3],

    ['TableSlikeSlovenija', 2],
    ['PekaPotice', 3]
  ]),

  forwardRoutes: new Map([
    ['NaslovnaStran', 'UvodnaAnimacija'],
    ['UvodnaAnimacija', 'Avla'],

    ['Avla', {
      ucilnice: 'Ucilnice',
      jedilnica: 'JedilnicaUcilnice',
      garderoba: 'Garderoba'
    }],

    ['Ucilnice', {
      besede: 'BesedeUvod',
      slovnica: 'TableSpoli-1',
      slovenci: 'SlovenciUvod',
      forward: 'BesedeUvod'
    }],

    ['BesedeUvod', 'KajJeTo-1'],
    ['KajJeTo-1', 'KajJeTo-2'],
    ['KajJeTo-2', 'Potrebscine-2'],
    ['Potrebscine-2', 'Potrebscine-3'],
    ['Potrebscine-3', 'KajJeToVpisovanje'],
    ['KajJeToVpisovanje', 'Potrebscine-2-Vpisovanje'],
    ['Potrebscine-2-Vpisovanje', 'Potrebscine-3-Vpisovanje'],
    ['Potrebscine-3-Vpisovanje', 'BesedeKajDelajo'],
    ['BesedeKajDelajo', 'BesedeKajDelajoVpisovanje'],
    ['BesedeKajDelajoVpisovanje', 'Ucilnice'],

    ['TableSpoli-1', 'TableSpoli-2'],
    ['TableSpoli-2', 'Glagoli-1'],
    ['Glagoli-1', 'Glagoli-2'],
    ['Glagoli-2', 'Glagoli-3'],
    ['Glagoli-3', 'TablePovezovanje'],
    ['TablePovezovanje', 'TablePovezovanje-2'],
    ['TablePovezovanje-2', 'TableVpisovanje'],
    ['TableVpisovanje', 'Ucilnice'],

    ['SlovenciUvod', 'KdoJeTo'],
    ['KdoJeTo', 'KajDelajoVpisovanje'],
    ['KajDelajoVpisovanje', 'Avla'],

    ['Jedilnica', {
      sadje: 'SadjePovezovanjeEdnina',
      zelenjava: 'ZelenjavaPovezovanje',
      malica: 'MalicaPovezovanje',
      kosilo: 'KosiloPovezovanje',
      kuhinja: 'Kuhinja'
    }],

    ['KosiloPovezovanje', 'SadjePovezovanjeEdnina'],
    ['MalicaPovezovanje', 'KosiloPovezovanje'],
    ['SadjePovezovanjeEdnina', 'SadjePovezovanjeMnozina'],
    ['SadjePovezovanjeMnozina', 'ZelenjavaPovezovanje'],
    ['ZelenjavaPovezovanje', 'Kuhinja'],

    ['Kuhinja', 'Pripomocki'],

    ['JedilniPripomockiPovezovanje', 'JedilniPripomockiVpisovanje'],
    ['JedilniPripomockiVpisovanje', 'KosareIzbiranje'],

    ['KosareIzbiranje', 'SadnaKupaSestavi'],
    ['SadnaKupaSestavi', 'SolataSestavi'],
    ['SolataSestavi', 'PomesaneCrke'],
    ['PomesaneCrke', 'MalicaSestavi'],
    ['MalicaSestavi', 'KosiloSestavi'],
    ['KosiloSestavi', 'MesanaHrana'],
    ['MesanaHrana', 'SpominMalicaKosilo'],
    ['SpominMalicaKosilo', 'SpominSadjeZelenjava'],
    ['SpominSadjeZelenjava', 'JedilnicaUcilnice'],

    ['JedilnicaUcilnice', {
      forward: 'Jedilnica'
    }],

    ['TableJedilnicaSpoli-1', 'TableJedilnicaSpoli-2'],
    ['TableJedilnicaSpoli-2', 'TableJedilnicaSpoli-3'],
    ['TableJedilnicaSpoli-3', 'TableJedilnicaSpoli-4'],
    ['TableJedilnicaSpoli-4', 'GlagoliJedilnica-1'],

    ['GlagoliJedilnica-1', 'GlagoliJedilnica-2'],
    ['GlagoliJedilnica-2', 'GlagoliJedilnica-3'],
    ['GlagoliJedilnica-3', 'GlagoliJedilnica-4'],
    ['GlagoliJedilnica-4', 'GlagoliJedilnica-5'],
    ['GlagoliJedilnica-5', 'GlagoliJedilnica-6'],
    ['GlagoliJedilnica-6', 'TableJedilnicaSlike'],

    ['TableJedilnicaSlike', 'TableJedilnicaPovezovanje'],
    ['TableJedilnicaPovezovanje', 'TableJedilnicaVpisovanje'],
    ['TableJedilnicaVpisovanje', 'KoncniceJedilnica'],
    ['KoncniceJedilnica', 'JedilnicaUcilnice'],

    ['SlovenijaUvod', 'TableSlikeSlovenija'],
    ['TableSlikeSlovenija', 'PekaPotice'],
    ['PekaPotice', 'JedilnicaUcilnice'],

    ['Garderoba', {
      besede: 'GarderobaStevilke',
      slovnica: 'GarderobaTableEdina',
      slovenci: 'GarderobaPesmica',
      forward: 'GarderobaStevilke'
    }],

    ['GarderobaStevilke', 'GarderobaStevilkeVpisovanje'],
    ['GarderobaStevilkeVpisovanje', 'GarderobaOblacilaPovezovanje'],
    ['GarderobaOblacilaPovezovanje', 'GarderobaBarve'],
    ['GarderobaBarve', 'GarderobaOblacilaVpisovanje'],
    ['GarderobaOblacilaVpisovanje', 'GarderobaKakoSoObleceni'],
    ['GarderobaKakoSoObleceni', 'GarderobaObleci'],
    ['GarderobaObleci', 'Garderoba'],


    ['GarderobaTableEdina', 'GarderobaTableMnozina'],
    ['GarderobaTableMnozina', 'GarderobaTableSklonMoski'],
    ['GarderobaTableSklonMoski', 'GarderobaTableSklonZenski'],
    ['GarderobaTableSklonZenski', 'GarderobKoncnice'],
    ['GarderobKoncnice', 'Garderoba'],

    ['GarderobaPesmica', 'GarderobaTableSlike'],
    ['GarderobaTableSlike', 'GarderobaPesmicaVpisovanje'],
    ['GarderobaPesmicaVpisovanje', 'Avla']
  ]),

  backwardRoutes: new Map([
    ['Ucilnice', {
      backward: 'Avla'
    }],

    ['BesedeUvod', 'Ucilnice'],
    ['KajJeTo-1', 'BesedeUvod'],
    ['KajJeTo-2', 'KajJeTo-1'],
    ['Potrebscine-2', 'KajJeTo-2'],
    ['Potrebscine-3', 'Potrebscine-2'],
    ['KajJeToVpisovanje', 'Potrebscine-3'],
    ['Potrebscine-2-Vpisovanje', 'KajJeToVpisovanje'],
    ['Potrebscine-3-Vpisovanje', 'Potrebscine-2-Vpisovanje'],
    ['BesedeKajDelajo', 'Potrebscine-3-Vpisovanje'],
    ['BesedeKajDelajoVpisovanje', 'BesedeKajDelajo'],

    ['TableSpoli-1', 'Ucilnice'],
    ['TableSpoli-2', 'TableSpoli-1'],
    ['Glagoli-1', 'TableSpoli-2'],
    ['Glagoli-2', 'Glagoli-1'],
    ['Glagoli-3', 'Glagoli-2'],
    ['TablePovezovanje', 'Glagoli-3'],
    ['TablePovezovanje-2', 'TablePovezovanje'],
    ['TableVpisovanje', 'TablePovezovanje-2'],

    ['SlovenciUvod', 'Ucilnice'],
    ['KdoJeTo', 'SlovenciUvod'],
    ['KajDelajoVpisovanje', 'KdoJeTo'],

    ['Jedilnica', 'Avla'],

    ['KosiloPovezovanje', 'Jedilnica'],
    ['MalicaPovezovanje', 'Jedilnica'],
    ['SadjePovezovanjeEdnina', 'Jedilnica'],
    ['SadjePovezovanjeMnozina', 'Jedilnica'],
    ['ZelenjavaPovezovanje', 'Jedilnica'],

    ['Kuhinja', 'Jedilnica'],

    ['JedilniPripomockiPovezovanje', 'Kuhinja'],
    ['JedilniPripomockiVpisovanje', 'JedilniPripomockiPovezovanje'],

    ['KosareIzbiranje', 'JedilnicaUcilnice'],
    ['SadnaKupaSestavi', 'KosareIzbiranje'],
    ['SolataSestavi', 'SadnaKupaSestavi'],
    ['PomesaneCrke', 'SolataSestavi'],
    ['MalicaSestavi', 'PomesaneCrke'],
    ['KosiloSestavi', 'MalicaSestavi'],
    ['MesanaHrana', 'KosiloSestavi'],
    ['SpominMalicaKosilo', 'MesanaHrana'],
    ['SpominSadjeZelenjava', 'SpominMalicaKosilo'],

    ['JedilnicaUcilnice', {
      backward: 'Avla'
    }],

    ['TableJedilnicaSpoli-1', 'JedilnicaUcilnice'],
    ['TableJedilnicaSpoli-2', 'TableJedilnicaSpoli-1'],
    ['TableJedilnicaSpoli-3', 'TableJedilnicaSpoli-2'],
    ['TableJedilnicaSpoli-4', 'TableJedilnicaSpoli-3'],

    ['GlagoliJedilnica-1', 'TableJedilnicaSpoli-4'],
    ['GlagoliJedilnica-2', 'GlagoliJedilnica-1'],
    ['GlagoliJedilnica-3', 'GlagoliJedilnica-2'],
    ['GlagoliJedilnica-4', 'GlagoliJedilnica-3'],
    ['GlagoliJedilnica-5', 'GlagoliJedilnica-4'],
    ['GlagoliJedilnica-6', 'GlagoliJedilnica-5'],

    ['TableJedilnicaSlike', 'GlagoliJedilnica-6'],
    ['TableJedilnicaPovezovanje', 'TableJedilnicaSlike'],
    ['TableJedilnicaVpisovanje', 'TableJedilnicaPovezovanje'],
    ['KoncniceJedilnica', 'TableJedilnicaVpisovanje'],
    ['SlovenijaUvod', 'JedilnicaUcilnice'],
    ['TableSlikeSlovenija', 'SlovenijaUvod'],
    ['PekaPotice', 'TableSlikeSlovenija'],

    ['Garderoba', 'Avla'],

    ['GarderobaStevilke', 'Garderoba'],
    ['GarderobaStevilkeVpisovanje', 'GarderobaStevilke'],
    ['GarderobaOblacilaPovezovanje', 'GarderobaStevilkeVpisovanje'],
    ['GarderobaBarve', 'GarderobaOblacilaPovezovanje'],
    ['GarderobaOblacilaVpisovanje', 'GarderobaBarve'],
    ['GarderobaKakoSoObleceni', 'GarderobaOblacilaVpisovanje'],
    ['GarderobaObleci', 'GarderobaKakoSoObleceni'],

    ['GarderobaTableEdina', 'Garderoba'],
    ['GarderobaTableMnozina', 'GarderobaTableEdina'],
    ['GarderobaTableSklonMoski', 'GarderobaTableMnozina'],
    ['GarderobaTableSklonZenski', 'GarderobaTableSklonMoski'],
    ['GarderobKoncnice', 'GarderobaTableSklonZenski'],

    ['GarderobaPesmica', 'Garderoba'],
    ['GarderobaTableSlike', 'GarderobaPesmica'],
    ['GarderobaPesmicaVpisovanje', 'GarderobaTableSlike']
  ])
}

const mutations = {}

const actions = {}

const getters = {}

export default {
  state,
  getters,
  actions,
  mutations
}
