import Vue from 'vue'
import Router from 'vue-router'

const NotFoundContainer = () => import('@/components/NotFoundContainer')

const IntroTitleContainer = () => import('@/components/intro/IntroTitleContainer')
const IntroAnimationContainer = () => import('@/components/intro/IntroAnimationContainer')

const AvlaContainer = () => import('@/components/avla/AvlaContainer')
const UcilniceContainer = () => import('@/components/ucilnice/UcilniceContainer')

const GameContainer = () => import('@/components/core/GameContainer')

const BesedeIntroContainer = () => import('@/components/ucilnice/besede/intro/BesedeIntroContainer')

const KajJeToContainer = () => import('@/components/ucilnice/besede/kajJeTo/KajJeToContainer')
const KajJeToVpisovanjeContainer = () => import('@/components/ucilnice/besede/kajJeTo/KajJeToVpisovanjeContainer')
const PotrebscineS2Container = () => import('@/components/ucilnice/besede/potrebscine/PotrebscineS2Container')
const PotrebscineS2VpisovanjeContainer = () => import('@/components/ucilnice/besede/potrebscine/PotrebscineS2VpisovanjeContainer')
const PotrebscineS3VpisovanjeContainer = () => import('@/components/ucilnice/besede/potrebscine/PotrebscineS3VpisovanjeContainer')
const PotrebscineS3Container = () => import('@/components/ucilnice/besede/potrebscine/PotrebscineS3Container')
const BesedeKajDelajoVpisovanjeContainer = () => import('@/components/ucilnice/besede/kajDelajo/BesedeKajDelajoVpisovanjeContainer')
const BesedeKajDelajoContainer = () => import('@/components/ucilnice/besede/kajDelajo/BesedeKajDelajoContainer')

const GlagoliS1Container = () => import('@/components/ucilnice/slovnica/glagoli/GlagoliS1Container')
const GlagoliS2Container = () => import('@/components/ucilnice/slovnica/glagoli/GlagoliS2Container')
const GlagoliS3Container = () => import('@/components/ucilnice/slovnica/glagoli/GlagoliS3Container')
const TableSpoliContainer = () => import('@/components/ucilnice/slovnica/table-spoli/TableSpoliContainer')
const TablePovezovanjeContainer = () => import('@/components/ucilnice/slovnica/table/TablePovezovanjeContainer')
const TablePovezovanje2Container = () => import('@/components/ucilnice/slovnica/table/TablePovezovanje2Container')
const TableVpisovanjeContainer = () => import('@/components/ucilnice/slovnica/table/TableVpisovanjeContainer')

const SlovenciIntroContainer = () => import('@/components/ucilnice/slovenci/SlovenciIntroContainer')
const KdoJeToContainer = () => import('@/components/ucilnice/slovenci/KdoJeToContainer')
const KajDelajoVpisovanjeContainer = () => import('@/components/ucilnice/slovenci/KajDelajoVpisovanjeContainer')

const JedilnicaContainer = () => import('@/components/jedilnica/jedilnica/JedilnicaContainer')
const KosiloPovezovanjeContainer = () => import('@/components/jedilnica/jedilnica/kosilo/KosiloPovezovanjeContainer')
const MalicaPovezovanjeContainer = () => import('@/components/jedilnica/jedilnica/malica/MalicaPovezovanjeContainer')
const SadjePovezovanjeContainer = () => import('@/components/jedilnica/jedilnica/sadje/SadjePovezovanjeContainer')
const ZelenjavaPovezovanjeContainer = () => import('@/components/jedilnica/jedilnica/zelenjava/ZelenjavaPovezovanjeContainer')

const KuhinjaContainer = () => import('@/components/jedilnica/kuhinja/KuhinjaContainer')
const JedilniPripomockiPovezovanjeContainer = () => import('@/components/jedilnica/kuhinja/pripomocki/JedilniPripomockiPovezovanjeContainer')
const JedilniPripomockiVpisovanjeContainer = () => import('@/components/jedilnica/kuhinja/pripomocki/JedilniPripomockiVpisovanjeContainer')

const KosareIzbiranjeContainer = () => import('@/components/jedilnica/kosare/kosare/KosareIzbiranjeContainer')
const SadnaKupaSestaviContainer = () => import('@/components/jedilnica/kosare/sadnaKupa/SadnaKupaSestaviContainer')
const SolataSestaviContainer = () => import('@/components/jedilnica/kosare/solata/SolataSestaviContainer')
const PomesaneCrkeContainer = () => import('@/components/jedilnica/kosare/pomesaneCrke/PomesaneCrkeContainer')
const MalicaSestaviContainer = () => import('@/components/jedilnica/kosare/malica/MalicaSestaviContainer')
const KosiloSestaviContainer = () => import('@/components/jedilnica/kosare/kosilo/KosiloSestaviContainer')
const MesanaHranaContainer = () => import('@/components/jedilnica/kosare/mesanaHrana/MesanaHranaContainer')
const SpominMalicaKosiloContainer = () => import('@/components/jedilnica/kosare/spominMalicaKosilo/SpominMalicaKosiloContainer')
const SpominSadjeZelenjavaContainer = () => import('@/components/jedilnica/kosare/spominSadjeZelenjava/SpominSadjeZelenjavaContainer')

const JedilnicaUcilniceContainer = () => import('@/components/jedilnica/ucilnice/JedilnicaUcilniceContainer')
const TableSpoliJedilnicaContainer = () => import('@/components/jedilnica/ucilnice/slovnica/table-spoli/TableSpoliContainer')

const GlagoliJedilnicaS1Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS1Container')
const GlagoliJedilnicaS2Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS2Container')
const GlagoliJedilnicaS3Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS3Container')
const GlagoliJedilnicaS4Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS4Container')
const GlagoliJedilnicaS5Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS5Container')
const GlagoliJedilnicaS6Container = () => import('@/components/jedilnica/ucilnice/slovnica/glagoli/GlagoliS6Container')

const TablePovezovanjeJedilnicaContainer = () => import('@/components/jedilnica/ucilnice/slovnica/table/TablePovezovanjeContainer')
const TableVpisovanjeJedilnicaContainer = () => import('@/components/jedilnica/ucilnice/slovnica/table/TableVpisovanjeContainer')
const TableSlikeJedilnicaContainer = () => import('@/components/jedilnica/ucilnice/slovnica/table/TableSlikeContainer')
const KoncniceJedilnicaContainer = () => import('@/components/jedilnica/ucilnice/slovnica/koncnice/KoncniceContainer')

const TableSlikeSlovenijaContainer = () => import('@/components/jedilnica/ucilnice/slovenija/table/TableSlikeContainer')
const SlovenijaUvodContainer = () => import('@/components/jedilnica/ucilnice/slovenija/uvod/SlovenijaUvodContainer')
const PekaPoticeContainer = () => import('@/components/jedilnica/ucilnice/slovenija/peka-potice/PekaPoticeContainer')

const GarderobaContainer = () => import('@/components/garderoba/GarderobaContainer');
const GarderobaStevilkeContainer = () => import('@/components/garderoba/stevilke/StevilkeContainer')
const GarderobaStevilkeVpisovanjeContainer = () => import('@/components/garderoba/stevilke/StevilkeVpisovanjeContainer')
const GarderobaOblacilaPovezovanjeContainer = () => import('@/components/garderoba/besedisce-za-oblacila/OblacilaPovezovanjeContainer')
const GarderobaOblacilaVpisovanjeContainer = () => import('@/components/garderoba/besedisce-za-oblacila/OblacilaVpisovanjeContainer')
const GarderobaBarveContainer = () => import('@/components/garderoba/barve/BarveContainer')
const GarderobaKakoSoObleceniContainer = () => import('@/components/garderoba/kako-so-obleceni/KakoSoObleceniContainer')
const GarderobaObleciContainer = () => import('@/components/garderoba/obleci/ObleciContainer')
const GarderobaTableContainer = () => import('@/components/garderoba/table/TableContainer')
const GarderobaTableSklonContainer = () => import('@/components/garderoba/table-sklon/TableContainer')
const GarderobaTableSlikeContainer = () => import('@/components/garderoba/table-slike/TableSlikeContainer')
const GarderobaKoncniceContainer = () => import('@/components/garderoba/koncnice/KoncniceContainer')
const GarderobaPesmicaContainer = () => import('@/components/garderoba/pesmica/PesmicaContainer')
const GarderobaPesmicaVpisovanjeContainer = () => import('@/components/garderoba/pesmica-vpisovanje/PesmicaVpisovanjeContainer')

import store from '@/store'

Vue.use(Router)

const mainTitle = 'Interaktivni slovar'

function isLocked (id) {
  return false
  // return !store.getters[`ucilnica${id}Unlocked`]
}

const router = new Router({
  routes: [
    { path: '*', component: NotFoundContainer },
    {
      path: '/',
      name: 'NaslovnaStran',
      component: IntroTitleContainer,
      meta: {
        title: mainTitle
      }
    },
    {
      path: '/uvod',
      name: 'UvodnaAnimacija',
      component: IntroAnimationContainer,
      meta: {
        title: mainTitle
      }
    },
    {
      path: '/avla',
      name: 'Avla',
      component: AvlaContainer,
      meta: {
        title: `Avla`
      }
    },
    {
      path: '/ucilnice',
      name: 'Ucilnice',
      component: UcilniceContainer,
      meta: {
        title: `Učilnice`
      }
    },
    {
      path: '/ucilnice/besede',
      redirect: { name: 'BesedeUvod' },
      name: 'Besede',
      component: GameContainer,
      children: [
        {
          path: 'uvod',
          name: 'BesedeUvod',
          component: BesedeIntroContainer,
          meta: {
            title: `Besede uvod`
          }
        },
        {
          path: 'kaj-je-to-1',
          name: 'KajJeTo-1',
          component: KajJeToContainer,
          meta: {
            title: `Kaj je to?`
          }
        },
        {
          path: 'kaj-je-to-2',
          name: 'KajJeTo-2',
          component: KajJeToContainer,
          meta: {
            title: `Kaj je to?`
          }
        },
        {
          path: 'potrebscine-2',
          name: 'Potrebscine-2',
          component: PotrebscineS2Container,
          meta: {
            title: `Potrebščine`
          }
        },
        {
          path: 'potrebscine-2-vpisovanje',
          name: 'Potrebscine-2-Vpisovanje',
          component: PotrebscineS2VpisovanjeContainer,
          meta: {
            title: `Potrebščine vpisovanje`
          }
        },
        {
          path: 'potrebscine-3-vpisovanje',
          name: 'Potrebscine-3-Vpisovanje',
          component: PotrebscineS3VpisovanjeContainer,
          meta: {
            title: `Potrebščine vpisovanje`
          }
        },
        {
          path: 'potrebscine-3',
          name: 'Potrebscine-3',
          component: PotrebscineS3Container,
          meta: {
            title: `Potrebščine`
          }
        },
        {
          path: 'kaj-je-to-vpisovanje',
          name: 'KajJeToVpisovanje',
          component: KajJeToVpisovanjeContainer,
          meta: {
            title: `Kaj je to?`
          }
        },
        {
          path: 'kaj-delajo',
          name: 'BesedeKajDelajo',
          component: BesedeKajDelajoContainer,
          meta: {
            title: `Kaj delajo?`
          }
        },
        {
          path: 'kaj-delajo-vpisovanje',
          name: 'BesedeKajDelajoVpisovanje',
          component: BesedeKajDelajoVpisovanjeContainer,
          meta: {
            title: `Kaj delajo?`
          }
        }
      ]
    },
    {
      path: '/ucilnice/slovnica',
      redirect: { name: 'TableSpoli-1' },
      name: 'Slovnica',
      component: GameContainer,
      children: [
        {
          path: 'table-spoli-1',
          name: 'TableSpoli-1',
          component: TableSpoliContainer,
          meta: {
            title: `Spoli`
          }
        },
        {
          path: 'table-spoli-2',
          name: 'TableSpoli-2',
          component: TableSpoliContainer,
          meta: {
            title: `Spoli`
          }
        },
        {
          path: 'glagoli-1',
          name: 'Glagoli-1',
          component: GlagoliS1Container,
          meta: {
            title: `Glagoli`
          }
        },
        {
          path: 'glagoli-2',
          name: 'Glagoli-2',
          component: GlagoliS2Container,
          meta: {
            title: `Glagoli`
          }
        },
        {
          path: 'glagoli-3',
          name: 'Glagoli-3',
          component: GlagoliS3Container,
          meta: {
            title: `Glagoli`
          }
        },
        {
          path: 'table-povezovanje',
          name: 'TablePovezovanje',
          component: TablePovezovanjeContainer,
          meta: {
            title: `Tabele`
          }
        },
        {
          path: 'table-povezovanje-2',
          name: 'TablePovezovanje-2',
          component: TablePovezovanje2Container,
          meta: {
            title: `Tabele`
          }
        },
        {
          path: 'table-vpisovanje',
          name: 'TableVpisovanje',
          component: TableVpisovanjeContainer,
          meta: {
            title: `Tabele`
          }
        }
      ]
    },
    {
      path: '/ucilnice/slovenci',
      redirect: { name: 'KdoJeTo' },
      name: 'slovenci',
      component: GameContainer,
      children: [
        {
          path: 'uvod',
          name: 'SlovenciUvod',
          component: SlovenciIntroContainer,
          meta: {
            title: `Kdo je to?`
          }
        },
        {
          path: 'kdo-je-to',
          name: 'KdoJeTo',
          component: KdoJeToContainer,
          meta: {
            title: `Kdo je to?`
          }
        },
        {
          path: 'kaj-delajo-vpisovanje',
          name: 'KajDelajoVpisovanje',
          component: KajDelajoVpisovanjeContainer,
          meta: {
            title: `Kaj delajo?`
          }
        }
      ]
    },
    {
      path: '/jedilnica',
      name: 'Jedilnica',
      component: JedilnicaContainer,
      meta: {
        title: `Jedilnica`
      }
    },
    {
      path: '/jedilnica/kosilo',
      redirect: { name: 'KosiloPovezovanje' },
      name: 'kosilo',
      component: GameContainer,
      children: [
        {
          path: 'kosilo-povezovanje',
          name: 'KosiloPovezovanje',
          component: KosiloPovezovanjeContainer,
          meta: {
            title: `Kosilo`
          }
        }
      ]
    },
    {
      path: '/jedilnica/malica',
      redirect: { name: 'MalicaPovezovanje' },
      name: 'malica',
      component: GameContainer,
      children: [
        {
          path: 'malica-povezovanje',
          name: 'MalicaPovezovanje',
          component: MalicaPovezovanjeContainer,
          meta: {
            title: `Malica`
          }
        }
      ]
    },
    {
      path: '/jedilnica/sadje',
      redirect: { name: 'SadjePovezovanjeEdnina' },
      name: 'sadje',
      component: GameContainer,
      children: [
        {
          path: 'sadje-povezovanje',
          name: 'SadjePovezovanjeEdnina',
          component: SadjePovezovanjeContainer,
          meta: {
            title: `Sadje edinina`
          }
        },
        {
          path: 'sadje-povezovanje-mnozina',
          name: 'SadjePovezovanjeMnozina',
          component: SadjePovezovanjeContainer,
          meta: {
            title: `Sadje množina`
          }
        }
      ]
    },
    {
      path: '/jedilnica/zelenjava',
      redirect: { name: 'ZelenjavaPovezovanje' },
      name: 'zelenjava',
      component: GameContainer,
      children: [
        {
          path: 'zelenjava-povezovanje',
          name: 'ZelenjavaPovezovanje',
          component: ZelenjavaPovezovanjeContainer,
          meta: {
            title: `Zelenjava`
          }
        }
      ]
    },

    {
      path: '/kuhinja',
      name: 'Kuhinja',
      component: KuhinjaContainer,
      meta: {
        title: `Kuhinja`
      }
    },
    {
      path: '/kuhinja/pripomocki',
      redirect: { name: 'JedilniPripomockiPovezovanje' },
      name: 'Pripomocki',
      component: GameContainer,
      children: [
        {
          path: 'jedilni-pripomocki-povezovanje',
          name: 'JedilniPripomockiPovezovanje',
          component: JedilniPripomockiPovezovanjeContainer,
          meta: {
            title: `Jedilni pripomočki`
          }
        },
        {
          path: 'jedilni-pripomocki-vpisovanje',
          name: 'JedilniPripomockiVpisovanje',
          component: JedilniPripomockiVpisovanjeContainer,
          meta: {
            title: `Jedilni pripomočki`
          }
        }
      ]
    },

    {
      path: '/kosare',
      redirect: { name: 'KosareIzbiranje' },
      name: 'Kosare',
      component: GameContainer,
      children: [
        {
          path: 'kosare-izbiranje',
          name: 'KosareIzbiranje',
          component: KosareIzbiranjeContainer,
          meta: {
            title: `Košare`,
            locked: isLocked(1)
          }
        },
        {
          path: 'sadna-kupa-sestavi',
          name: 'SadnaKupaSestavi',
          component: SadnaKupaSestaviContainer,
          meta: {
            title: `Sadna kupa`,
            locked: isLocked(1)
          }
        },
        {
          path: 'solata-sestavi',
          name: 'SolataSestavi',
          component: SolataSestaviContainer,
          meta: {
            title: `Solata`,
            locked: isLocked(1)
          }
        },
        {
          path: 'pomesane-crke',
          name: 'PomesaneCrke',
          component: PomesaneCrkeContainer,
          meta: {
            title: `Pomešane črke`,
            locked: isLocked(1)
          }
        },
        {
          path: 'kosilo-sestavi',
          name: 'KosiloSestavi',
          component: KosiloSestaviContainer,
          meta: {
            title: `Kosilo`,
            locked: isLocked(1)
          }
        },
        {
          path: 'malica-sestavi',
          name: 'MalicaSestavi',
          component: MalicaSestaviContainer,
          meta: {
            title: `Malica`,
            locked: isLocked(1)
          }
        },
        {
          path: 'mesana-hrana',
          name: 'MesanaHrana',
          component: MesanaHranaContainer,
          meta: {
            title: `Mešana hrana`,
            locked: isLocked(1)
          }
        },
        {
          path: 'spomin-malica-kosilo',
          name: 'SpominMalicaKosilo',
          component: SpominMalicaKosiloContainer,
          meta: {
            title: `Spomin (malica in kosilo)`,
            locked: isLocked(1)
          }
        },
        {
          path: 'spomin-sadje-zelenjava',
          name: 'SpominSadjeZelenjava',
          component: SpominSadjeZelenjavaContainer,
          meta: {
            title: `Spomin (sadje in zelenjava)`,
            locked: isLocked(1)
          }
        }
      ]
    },

    {
      path: '/jedilnica-ucilnice',
      name: 'JedilnicaUcilnice',
      component: JedilnicaUcilniceContainer,
      meta: {
        title: `Jedilnica učilnice`
      }
    },

    {
      path: '/jedilnica-ucilnice/slovnica',
      redirect: { name: 'TableJedilnicaSpoli-1' },
      name: 'JedilnicaSlovnica',
      component: GameContainer,
      children: [
        {
          path: 'table-spoli-1',
          name: 'TableJedilnicaSpoli-1',
          component: TableSpoliJedilnicaContainer,
          meta: {
            title: `Spoli ednina 1`
          }
        },
        {
          path: 'table-spoli-2',
          name: 'TableJedilnicaSpoli-2',
          component: TableSpoliJedilnicaContainer,
          meta: {
            title: `Spoli ednina 2`
          }
        },
        {
          path: 'table-spoli-3',
          name: 'TableJedilnicaSpoli-3',
          component: TableSpoliJedilnicaContainer,
          meta: {
            title: `Spoli množina 1`
          }
        },
        {
          path: 'table-spoli-4',
          name: 'TableJedilnicaSpoli-4',
          component: TableSpoliJedilnicaContainer,
          meta: {
            title: `Spoli množina 2`
          }
        },
        {
          path: 'glagoli-1',
          name: 'GlagoliJedilnica-1',
          component: GlagoliJedilnicaS1Container,
          meta: {
            title: `Glagoli - Kotiček za ednino`
          }
        },
        {
          path: 'glagoli-2',
          name: 'GlagoliJedilnica-2',
          component: GlagoliJedilnicaS2Container,
          meta: {
            title: `Glagoli - Ednina`
          }
        },
        {
          path: 'glagoli-3',
          name: 'GlagoliJedilnica-3',
          component: GlagoliJedilnicaS3Container,
          meta: {
            title: `Glagoli - Kotiček za dvojino`
          }
        },
        {
          path: 'glagoli-4',
          name: 'GlagoliJedilnica-4',
          component: GlagoliJedilnicaS4Container,
          meta: {
            title: `Glagoli - Dvojina`
          }
        },
        {
          path: 'glagoli-5',
          name: 'GlagoliJedilnica-5',
          component: GlagoliJedilnicaS5Container,
          meta: {
            title: `Glagoli - Kotiček za množino`
          }
        },
        {
          path: 'glagoli-6',
          name: 'GlagoliJedilnica-6',
          component: GlagoliJedilnicaS6Container,
          meta: {
            title: `Glagoli - Množina`
          }
        },
        {
          path: 'table-povezovanje',
          name: 'TableJedilnicaPovezovanje',
          component: TablePovezovanjeJedilnicaContainer,
          meta: {
            title: `Table povezovanje`
          }
        },
        {
          path: 'table-vpisovanje',
          name: 'TableJedilnicaVpisovanje',
          component: TableVpisovanjeJedilnicaContainer,
          meta: {
            title: `Table vpisovanje`
          }
        },
        {
          path: 'tabla-slike',
          name: 'TableJedilnicaSlike',
          component: TableSlikeJedilnicaContainer,
          meta: {
            title: `Tabela s slikami in glagoli`
          }
        },
        {
          path: 'koncnice',
          name: 'KoncniceJedilnica',
          component: KoncniceJedilnicaContainer,
          meta: {
            title: `Menjava končnic`
          }
        }
      ]
    },

    {
      path: '/jedilnica-ucilnice/slovenija',
      redirect: { name: 'SlovenijaUvod' },
      name: 'JedilnicaSlovenija',
      component: GameContainer,
      children: [
        {
          path: 'uvod',
          name: 'SlovenijaUvod',
          component: SlovenijaUvodContainer,
          meta: {
            title: `Uvod`
          }
        },
        {
          path: 'tabla-slike',
          name: 'TableSlikeSlovenija',
          component: TableSlikeSlovenijaContainer,
          meta: {
            title: `Tabela s slikami`
          }
        },
        {
          path: 'peka-potice',
          name: 'PekaPotice',
          component: PekaPoticeContainer,
          meta: {
            title: 'Peka potice'
          }
        }
      ]
    },

    {
      path: '/garderoba',
      name: 'Garderoba',
      component: GarderobaContainer,
      meta: {
        title: `Garderoba`
      }
    },

    {
      path: '/garderoba/stevilke',
      name: 'GarderobaStevilke',
      component: GarderobaStevilkeContainer,
      meta: {
        title: `Številke`
      }
    },
    {
      path: '/garderoba/stevilke-vpisovanje',
      name: 'GarderobaStevilkeVpisovanje',
      component: GarderobaStevilkeVpisovanjeContainer,
      meta: {
        title: `Številke`
      }
    },
    {
      path: '/garderoba/oblacila-povezovanje',
      name: 'GarderobaOblacilaPovezovanje',
      component: GarderobaOblacilaPovezovanjeContainer,
      meta: {
        title: `Oblačila povezovanje`
      }
    },
    {
      path: '/garderoba/barve',
      name: 'GarderobaBarve',
      component: GarderobaBarveContainer,
      meta: {
        title: `Barve`
      }
    },
    {
      path: '/garderoba/oblacila-vpisovanje',
      name: 'GarderobaOblacilaVpisovanje',
      component: GarderobaOblacilaVpisovanjeContainer,
      meta: {
        title: `Oblačila vpisovanje`
      }
    },
    {
      path: '/garderoba/kako-so-obleceni',
      name: 'GarderobaKakoSoObleceni',
      component: GarderobaKakoSoObleceniContainer,
      meta: {
        title: `Kako so oblečeni otroci?`
      }
    },
    {
      path: '/garderoba/obleci',
      name: 'GarderobaObleci',
      component: GarderobaObleciContainer,
      meta: {
        title: `Obleci Majo in Jakoba!`
      }
    },
    {
      path: '/garderoba/table-ednina',
      name: 'GarderobaTableEdina',
      component: GarderobaTableContainer,
      meta: {
        title: `Table za razporeditev samostalnika po spolu`
      }
    },
    {
      path: '/garderoba/table-mnozina',
      name: 'GarderobaTableMnozina',
      component: GarderobaTableContainer,
      meta: {
        title: `Table za razporeditev samostalnika po spolu`
      }
    },
    {
      path: '/garderoba/table-sklon-moski',
      name: 'GarderobaTableSklonMoski',
      component: GarderobaTableSklonContainer,
      meta: {
        title: `Table za razporeditev samostalnika po spolu - 4.sklon`
      }
    },
    {
      path: '/garderoba/table-sklon-zenski',
      name: 'GarderobaTableSklonZenski',
      component: GarderobaTableSklonContainer,
      meta: {
        title: `Table za razporeditev samostalnika po spolu - 4.sklon`
      }
    },
    {
      path: '/garderoba/koncnice',
      name: 'GarderobKoncnice',
      component: GarderobaKoncniceContainer,
      meta: {
        title: `Končnice`
      }
    },
    {
      path: '/garderoba/table-slike',
      name: 'GarderobaTableSlike',
      component: GarderobaTableSlikeContainer,
      meta: {
        title: `Table slike`
      }
    },
    {
      path: '/garderoba/pesmica',
      name: 'GarderobaPesmica',
      component: GarderobaPesmicaContainer,
      meta: {
        title: `Pesmica`
      }
    },
    {
      path: '/garderoba/pesmica-vpisovanje',
      name: 'GarderobaPesmicaVpisovanje',
      component: GarderobaPesmicaVpisovanjeContainer,
      meta: {
        title: `Pesmica vpisovanje`
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.meta.locked) {
    next('/avla')
  } else {
    next()
  }
})

router.afterEach((to, from) => {
  document.title = `${mainTitle} - ${to.meta.title}`
})

export default router
