let buildFolder = '/slikovni-slovar/'

process.argv.forEach(function (val, index, array) {
  if (val.includes('folder')) {
    buildFolder = val.split('=')[1].trim()
  }
})


// vue.config.js
module.exports = {
  chainWebpack: config => {
    config.plugins.delete('prefetch')

    const svgRule = config.module.rule('svg')

    svgRule.uses.clear()

    svgRule
      .use('svg-sprite-loader')
        .loader('svg-sprite-loader')
        .tap(options => {
          const newOptions = {
            symbolId: '[name][hash]'
          }

          return { ...options, ...newOptions }
        })
        .end()
  },
  baseUrl: process.env.NODE_ENV === 'production'
    ? buildFolder
    : '/'
}
